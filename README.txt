
                             InMed

  InMed é um programa para análise de incertezas de medição.

  -------------
  Como utilizar
  -------------

  Ao iniciar o programa entre com os dados solicitados nas caixas de texto.
  Título, Autor e Descrição são obrigatórios. Ao terminar aperte o botão Próximo (Alt+P).

  Em seguida deve-se entrar com modelo matemático da análise a ser realizada.
  Este modelo é uma equação da forma y = a*x + b^3, por exemplo.
  Esta equação poderá conter os seguintes valores:

    + - * /
    x^y ou pow(x,y) -> para potência de x elevado a y
    abs(x) -> valor absoluto de x
    sqrt(x) -> raiz quadrada de x
    sin(x) -> seno de x
    cos(x) -> cosseno de x
    tan(x) -> tangente de x
    asin(x) -> arco seno de x
    acos(x) -> arco cosseno de x
    atan(x) -> arco tangente de x
    sinh(x) -> seno hiperbólico de x
    cosh(x) -> cosseno hiperbólico de x
    tanh(x) -> tangente hiperbólica de x
    asinh(x) -> arco seno hiperbólico de x
    acosh(x) -> arco cosseno hiperbólico de x
    atanh(x) -> arco tangente hiperbólica de x
    exp(x) -> função exponencial de x
    log(x) -> logaritmo natural de x
    Pi -> π

  Mais funções podem ser encontradas em www.ginac.de/tutorial/#Numeric-functions

  Logo abaixo as grandezas que representam o modelo matemático serão apresentadas.
  Entre com dados solicitados para cada grandeza. A descrição é opcional, todos os
outros campos devem conter valores válidos.
  Valores numéricos válidos incluem os sinais + ou -, assim como e ou E usados em notação
científica e o ponto decimal.

  Ao terminar aperte o botão Próximo (Alt+P) para continuar ou o botão Anterior (Alt+N)
para retornar à página anterior.

  -----------
  Observações
  -----------

  Apesar do programa apresentar os valores arredondados em duas casa decimais, internamente nenhum
arredondamento é realizado durante os cálculos, sendo utilizado o tipo padrão em C++ double.

  Para grandezas do tipo B com distribuição normal, o valor do fator de abrangência (k) será 2
se este não for especificado.

  Para grandezas do tipo B, o número de graus de liberdade será considerado infinito se este
não for especificado.

  O programa usa como separador decimal o ponto (exemplo: 2.1) e não a vírgula.

  -----------
  Bibliotecas
  -----------

  Este programa é estrito em C++ e utiliza a biblioteca GiNaC (www.ginac.de) para a computação
simbólica e Qt (www.qt.io) para a interfáce gráfica. Para a compilação são utilizados
o GCC (gcc.gnu.org) e o CMake (cmake.org).





