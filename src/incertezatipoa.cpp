// InMed - incertezatipoa.cpp

#include "incertezatipoa.hpp"

#include <cmath>
#include <iterator>
#include <numeric>

IncertezaTipoA::IncertezaTipoA(std::vector<double> desvio,
                               double desvioPadraoAgrupado,
                               int numeroObservacoesAgrupadas)
{
    m_variancia = std::inner_product(begin(desvio), end(desvio), begin(desvio), 0.0) / (desvio.size() - 1);

    m_desvioPadrao = std::sqrt(m_variancia);

    if (numeroObservacoesAgrupadas == 0)
    {
        m_varianciaDaMedia = m_variancia / desvio.size();

        m_incertezaPadrao = std::sqrt(m_varianciaDaMedia);
    }
    else // desvio-padrao agrupado
    {
        m_incertezaPadrao = desvioPadraoAgrupado / std::sqrt(desvio.size());

        m_varianciaDaMedia = (m_desvioPadrao * m_desvioPadrao);
    }
}

double IncertezaTipoA::variancia() const
{
    return m_variancia;
}

double IncertezaTipoA::desvioPadrao() const
{
    return m_desvioPadrao;
}

double IncertezaTipoA::varianciaDaMedia() const
{
    return m_varianciaDaMedia;
}

double IncertezaTipoA::incertezaPadraoA() const
{
    return m_incertezaPadrao;
}
