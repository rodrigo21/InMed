#include "janelaprincipal.hpp"
#include "ui_janelaprincipal.h"

#include "derivadasparciais.hpp"
#include "dialogomostrartexto.hpp"
#include "dialogosobre.hpp"
#include "inmedconfig.hpp"
#include "paginamodelo.hpp"
#include "paginatitulo.hpp"

#include <QAction>
#include <QApplication>
#include <QMessageBox>
#include <QStackedWidget>
#include <QString>
#include <QWidget>

class QStringList;

bool JanelaPrincipal::apenasDerivadas = false;

JanelaPrincipal::JanelaPrincipal(QWidget* parent) :
    QMainWindow(parent),
    m_paginaTitulo(nullptr),
    m_paginaModelo(nullptr),
    m_paginaBalanco(nullptr),
    m_dadosMedicao(parent),
    m_resultadoMedicao(parent),
    m_dialogDerivadasParciais(nullptr),
    m_ui(new Ui::JanelaPrincipal)
{
    m_ui->setupUi(this);

    m_ui->stackedWidgetPaginas->setCurrentIndex(0);

    setWindowTitle(m_ui->stackedWidgetPaginas->currentWidget()->windowTitle());

    definirConexoes();
}

JanelaPrincipal::~JanelaPrincipal()
{
    delete m_ui;
}

void JanelaPrincipal::mostrarAjuda()
{
    auto dialogo = new DialogoMostrarTexto(":README", tr("Ajuda"), this);
    dialogo->resize(600, 800);
    dialogo->exec();
}

void JanelaPrincipal::mostrarLicenca()
{
    auto dialogo = new DialogoMostrarTexto(":LICENSE", tr("Licença"), this);
    dialogo->resize(500, 540);
    dialogo->exec();
}

void JanelaPrincipal::sobreInMed()
{
    auto dialogo = new DialogoSobre("Sobre", QString::fromStdString(InMed_VERSION_SHORT), this);
    dialogo->exec();
}

void JanelaPrincipal::anteriorPagina()
{
    int anterior = m_ui->stackedWidgetPaginas->currentIndex() - 1;
    m_ui->stackedWidgetPaginas->setCurrentIndex(anterior);
}

void JanelaPrincipal::proximaPagina()
{
    int proxima = m_ui->stackedWidgetPaginas->currentIndex() + 1;

    if (proxima == 1)
    {
        apenasDerivadas = false;

        coletarDados(proxima);

        m_ui->stackedWidgetPaginas->setCurrentIndex(proxima);
    }
    else if (proxima == 2)
    {
        apenasDerivadas = false;

        if (PaginaModelo::derivadasValidas && coletarDados(proxima))
        {
            m_ui->stackedWidgetPaginas->setCurrentIndex(proxima);
        }
    }
}

void JanelaPrincipal::visualizarDerivadasParciais()
{
    if (PaginaModelo::derivadasValidas)
    {
        apenasDerivadas = true;

        coletarDados(3);

        m_dialogDerivadasParciais = new DerivadasParciais(m_dadosMedicao.grandezaResultado(),
                                                          m_dadosMedicao.modeloMatematico(), this);
        m_dialogDerivadasParciais->exec();
    }
}

void JanelaPrincipal::definirConexoes()
{
    // Mudar titulo dajanela
    connect(m_ui->stackedWidgetPaginas, &QStackedWidget::currentChanged, this,
            [=] { setWindowTitle(m_ui->stackedWidgetPaginas->currentWidget()->windowTitle()); });

    // action connects
    // menu arquivo
    connect(m_ui->actionFechar, &QAction::triggered, this, &JanelaPrincipal::close);
    connect(m_ui->actionSair, &QAction::triggered, this, &JanelaPrincipal::close);

    // menu ajuda
    connect(m_ui->actionAjudaInMed, &QAction::triggered, this, &JanelaPrincipal::mostrarAjuda);
    connect(m_ui->actionLicenca, &QAction::triggered, this, &JanelaPrincipal::mostrarLicenca);
    connect(m_ui->actionSobreInMed, &QAction::triggered, this, &JanelaPrincipal::sobreInMed);
    connect(m_ui->actionSobreQt, &QAction::triggered, this, &QApplication::aboutQt);

    // mudar páginas
    // página 0
    connect(m_ui->pageTitulo, &PaginaTitulo::botaoProximoApertado, this,
            &JanelaPrincipal::proximaPagina);

    // página 1
    connect(m_ui->pageModelo, &PaginaModelo::botaoAnteriorApertado, this,
            &JanelaPrincipal::anteriorPagina);
    connect(m_ui->pageModelo, &PaginaModelo::botaoProximoApertado, this,
            &JanelaPrincipal::proximaPagina);

    // pagina 2
    connect(m_ui->pageBalanco, &PaginaBalanco::botaoAnteriorApertado, this,
            &JanelaPrincipal::anteriorPagina);

    // derivadas parciais
    connect(m_ui->pageModelo, &PaginaModelo::botaoDerivadasApertado, this,
            &JanelaPrincipal::visualizarDerivadasParciais);

    connect(&m_dadosMedicao, &DadosMedicao::invalidosDadosExperimentais, this,
            [this](const QStringList& grandezasInvalidas) {
                if (grandezasInvalidas.size() == 1)
                {
                    QMessageBox::critical(
                        this, tr("Erro"),
                        tr("A grandeza <b><font "
                           "color=red>%1</font></b> não possui dados experimentais válidos")
                            .arg(grandezasInvalidas.join("")));
                }
                else
                {
                    QMessageBox::critical(
                        this, tr("Erro"),
                        tr("As grandezas <b><font "
                           "color=red>%1</font></b> não possuem dados experimentais válidos")
                            .arg(grandezasInvalidas.join(", ")));
                }
            });

    connect(&m_dadosMedicao, &DadosMedicao::erroValoresDPEA, this,
            [this](const QStringList& grandezasInvalidas) {
                if (grandezasInvalidas.size() == 1)
                {
                    QMessageBox::critical(
                        this, tr("Erro"),
                        tr("O campo 'Desvio-padrão Experimental Agrupado' ou<br>'Número de "
                           "Observações Repetidas e Independentes'<br>da grandeza <b><font "
                           "color=red>%1</font></b> não possui um valor válido")
                            .arg(grandezasInvalidas.join("")));
                }
                else
                {
                    QMessageBox::critical(
                        this, tr("Erro"),
                        tr("O campo 'Desvio-padrão Experimental Agrupado' ou<br>'Número de "
                           "Observações Repetidas e Independentes'<br>das grandezas <b><font "
                           "color=red>%1</font></b> não possui um valor válido")
                            .arg(grandezasInvalidas.join(", ")));
                }
            });

    connect(&m_dadosMedicao, &DadosMedicao::erroValoresDistribuicao, this,
            [this](const QStringList& grandezasInvalidas) {
                if (grandezasInvalidas.size() == 1)
                {
                    QMessageBox::critical(
                        this, tr("Erro"),
                        tr("Valores inválidos para os dados da distribuição da grandeza <b><font "
                           "color=red>%1</font></b>")
                            .arg(grandezasInvalidas.join("")));
                }
                else
                {
                    QMessageBox::critical(
                        this, tr("Erro"),
                        tr("Valores inválidos para os dados da distribuição das grandezas <b><font "
                           "color=red>%1</font></b>")
                            .arg(grandezasInvalidas.join(", ")));
                }
            });
}

bool JanelaPrincipal::coletarDados(int proximaPagina)
{
    // mudando da página PaginaTitulo para a PaginaModelo
    if (proximaPagina == 1)
    {
        m_paginaTitulo = qobject_cast<PaginaTitulo*>(m_ui->stackedWidgetPaginas->widget(0));

        m_dadosMedicao.setTitulo(m_paginaTitulo->titulo());
        m_dadosMedicao.setAutor(m_paginaTitulo->dadosAutor());
        m_dadosMedicao.setData(m_paginaTitulo->dadosData());
        m_dadosMedicao.setReferencia(m_paginaTitulo->dadosReferencia());
        m_dadosMedicao.setVersao(m_paginaTitulo->dadosVersao());
        m_dadosMedicao.setDescricaoMedicao(m_paginaTitulo->descricao());
    }
    else if (proximaPagina == 2)
    {
        m_paginaModelo  = qobject_cast<PaginaModelo*>(m_ui->stackedWidgetPaginas->widget(1));
        m_paginaBalanco = qobject_cast<PaginaBalanco*>(m_ui->stackedWidgetPaginas->widget(2));

        // dados do tipo resultado

        m_dadosMedicao.setNomeGrandezaResultado(m_paginaModelo->nomeGrandezaResultado());
        m_dadosMedicao.setDefinicaoGrandezaResultado(m_paginaModelo->definicaoGrandezaResultado());
        m_dadosMedicao.setUnidadeGrandezaResultado(m_paginaModelo->unidadeGrandezaResultado());

        // dados do tipo A ou B

        // comuns a A e B
        m_dadosMedicao.setNomeGrandezasAouB(m_paginaModelo->nomeGrandezasAouB());
        m_dadosMedicao.setDefinicaoGrandezasAouB(m_paginaModelo->definicaoGrandezasAouB());
        m_dadosMedicao.setUnidadeGrandezasAouB(m_paginaModelo->unidadeGrandezasAouB());
        m_dadosMedicao.setTipoGrandezaAouB(m_paginaModelo->tipoGrandezaAouB());

        // tipo A
        m_dadosMedicao.setDadosExperimentaisAouB(m_paginaModelo->dadosExperimentaisAouB());
        m_dadosMedicao.setAvaliacaoIncertezaAouB(m_paginaModelo->avaliacaoIncertezaAouB());
        m_dadosMedicao.setDPEAgrupadoAouB(m_paginaModelo->DPEAgrupadoAouB());
        m_dadosMedicao.setNumObsRIAouB(m_paginaModelo->numObsRIAouB());

        // tipo B
        m_dadosMedicao.setDistribuicao(m_paginaModelo->distribuicao());
        m_dadosMedicao.setValorEstimado(m_paginaModelo->valorEstimado());
        m_dadosMedicao.setIncertezaExpandidaT(m_paginaModelo->incertezaExpandidaT());
        m_dadosMedicao.setGrausLiberdade(m_paginaModelo->grausLiberdade());
        m_dadosMedicao.setNivelConfianca(m_paginaModelo->nivelConfianca());
        m_dadosMedicao.setURTriMeiaLarguraLimites(m_paginaModelo->URTriMeiaLarguraLimites());
        m_dadosMedicao.setIncertezaExpandidaNormal(m_paginaModelo->incertezaExpandidaNormal());
        m_dadosMedicao.setFatorAbrangencia(m_paginaModelo->fatorAbrangencia());
        m_dadosMedicao.setIncertezaPadrao(m_paginaModelo->incertezaPadrao());
        m_dadosMedicao.setTraMeiaLarguraLimites1(m_paginaModelo->traMeiaLarguraLimites1());
        m_dadosMedicao.setTraMeiaLarguraLimites2(m_paginaModelo->traMeiaLarguraLimites2());

        m_dadosMedicao.setNumeroGrandezas(m_paginaModelo->numeroGrandezas());

        m_dadosMedicao.setModeloMatematico(m_paginaModelo->modeloMatematico());

        auto modeloMatematico  = m_dadosMedicao.modeloMatematico();
        auto grandezaResultado = m_dadosMedicao.grandezaResultado();
        auto grandezas         = m_dadosMedicao.grandezas();

        if (m_dadosMedicao.grandezasCorretas())
        {
            m_resultadoMedicao.setDadosMedicao(modeloMatematico, grandezaResultado, grandezas);

            m_paginaBalanco->setDadosBalanco(grandezaResultado, m_resultadoMedicao.contribuicoes(),
                                             m_resultadoMedicao.valorResultado());
        }
    }
    // derivadas parciais
    else if (proximaPagina == 3)
    {
        m_paginaModelo = qobject_cast<PaginaModelo*>(m_ui->stackedWidgetPaginas->widget(1));

        // dados do tipo resultado
        m_dadosMedicao.setNomeGrandezaResultado(m_paginaModelo->nomeGrandezaResultado());
        m_dadosMedicao.setDefinicaoGrandezaResultado(m_paginaModelo->definicaoGrandezaResultado());
        m_dadosMedicao.setUnidadeGrandezaResultado(m_paginaModelo->unidadeGrandezaResultado());

        m_dadosMedicao.setModeloMatematico(m_paginaModelo->modeloMatematico());
    }

    return m_dadosMedicao.grandezasCorretas();
}
