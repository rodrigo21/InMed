#include "derivadasparciais.hpp"
#include "ui_derivadasparciais.h"

#include <ginac/ginac.h>

#include <QFont>
#include <QFontDatabase>
#include <QString>

#include <sstream>
#include <string>

DerivadasParciais::DerivadasParciais(GrandezaResultado grandezaResultado,
                                     ModeloMatematico modeloMatematico,
                                     QWidget* parent) :
    QDialog(parent), m_ui(new Ui::DerivadasParciais)
{
    m_ui->setupUi(this);

    std::stringstream derivada;

    for (const auto& simbolo : modeloMatematico.simbolos())
    {
        derivada.clear();
        derivada.str(std::string());

        derivada << "∂" << grandezaResultado.nomeGrandeza() << "/∂" << simbolo.get_name() << " = "
                 << modeloMatematico.modelo().diff(simbolo) << "\n";

        m_ui->textEditDerivadas->append(QString::fromStdString(derivada.str()));
    }

    QFont monoFonte {QFontDatabase::systemFont(QFontDatabase::FixedFont)};
    monoFonte.setPointSize(14);

    m_ui->textEditDerivadas->setFont(monoFonte);
}

DerivadasParciais::~DerivadasParciais()
{
    delete m_ui;
}
