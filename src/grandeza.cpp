#include "grandeza.hpp"

#include <string>
#include <utility>

Grandeza::Grandeza(const GiNaC::symbol& simboloGrandeza, std::string definicao,
                   std::string unidade) :
    m_simboloGrandeza(simboloGrandeza),
    m_definicao(std::move(definicao)),
    m_unidade(std::move(unidade)),
    m_grausDeLiberdade(0),
    m_valorEstimado(0.0),
    m_incertezaPadrao(0.0),
    m_possuiCorrelacao(
        false),
    m_distribuicao(Distribuicao::Invalida),
    m_correcao(0.0)
{
    m_nomeGrandeza = m_simboloGrandeza.get_name();
}

GiNaC::symbol Grandeza::simboloGrandeza() const
{
    return m_simboloGrandeza;
}

std::string Grandeza::nomeGrandeza() const
{
    return m_nomeGrandeza;
}

std::string Grandeza::definicao() const
{
    return m_definicao;
}

std::string Grandeza::unidade() const
{
    return m_unidade;
}

int Grandeza::grausDeLiberdade() const
{
    return m_grausDeLiberdade;
}

double Grandeza::valorEstimado() const
{
    return m_valorEstimado;
}

Distribuicao Grandeza::distribuicao() const
{
    return m_distribuicao;
}

double Grandeza::correcao() const
{
    return m_correcao;
}
