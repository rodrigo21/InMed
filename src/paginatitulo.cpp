#include "paginatitulo.hpp"
#include "ui_paginatitulo.h"

#include <QDate>
#include <QDateEdit>
#include <QLineEdit>
#include <QPlainTextEdit>
#include <QPushButton>

PaginaTitulo::PaginaTitulo(QWidget* parent) :
    QWidget(parent), m_ui(new Ui::PaginaTitulo)
{
    m_ui->setupUi(this);

    m_ui->dateEditData->setDate(QDate::currentDate());

    // conecta o sinal para habilitar o botão Próximo
    connect(m_ui->plainTextEditTitulo, &QPlainTextEdit::textChanged, this,
            &PaginaTitulo::habilitarBotaoProximo);
    connect(m_ui->lineEditAutor, &QLineEdit::textChanged, this,
            &PaginaTitulo::habilitarBotaoProximo);
    connect(m_ui->plainTextEditDescricao, &QPlainTextEdit::textChanged, this,
            &PaginaTitulo::habilitarBotaoProximo);

    // conecta o sinal clicked do QPushButton ao sinal botaoProximoApertado
    connect(m_ui->pushButtonProximo, &QPushButton::clicked, this,
            &PaginaTitulo::botaoProximoApertado);
}

PaginaTitulo::~PaginaTitulo()
{
    delete m_ui;
}

QString PaginaTitulo::titulo() const
{
    return m_ui->plainTextEditTitulo->toPlainText();
}

QString PaginaTitulo::dadosAutor() const
{
    return m_ui->lineEditAutor->text();
}

QString PaginaTitulo::dadosData() const
{
    return m_ui->dateEditData->date().toString("dd/MM/yyyy");
}

QString PaginaTitulo::dadosReferencia() const
{
    return m_ui->lineEditReferencia->text();
}

QString PaginaTitulo::dadosVersao() const
{
    return m_ui->lineEditVersao->text();
}

QString PaginaTitulo::descricao() const
{
    return m_ui->plainTextEditDescricao->toPlainText();
}

void PaginaTitulo::habilitarBotaoProximo()
{
    if (titulo().isEmpty() || dadosAutor().isEmpty() || descricao().isEmpty())
    {
        m_ui->pushButtonProximo->setEnabled(false);
    }
    else
    {
        m_ui->pushButtonProximo->setEnabled(true);
    }
}
