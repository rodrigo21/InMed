#include "contribuicaoincerteza.hpp"

#include "grandeza.hpp"

#include <ginac/ginac.h>

#include <iostream>
#include <memory>
#include <utility>

ContribuicaoIncerteza::ContribuicaoIncerteza(std::shared_ptr<Grandeza> grandeza,
                                             const ModeloMatematico& modelo,
                                             GiNaC::exmap mapaValorEstimado) :
    m_grandeza(grandeza),
    m_modelo(modelo),
    m_mapaValorEstimado(std::move(
        mapaValorEstimado))
{
    try
    {
        m_coeficienteSensibilidade = m_modelo.modelo().diff(m_grandeza->simboloGrandeza());

        m_valorCoeficienteSensibilidade = GiNaC::ex_to<GiNaC::numeric>(
                                              m_coeficienteSensibilidade.subs(m_mapaValorEstimado).evalf())
                                              .to_double();
    }
    catch (...)
    {
        std::cout << "Erro: Valores inválidos para a grandezas do modelo." << std::endl;

        m_coeficienteSensibilidade = 0;

        m_valorCoeficienteSensibilidade = 0;
    }

    m_contribuicaoIncerteza = m_valorCoeficienteSensibilidade * grandeza->incertezaPadrao();

    m_grausDeLiberdade = grandeza->grausDeLiberdade();
}

GiNaC::ex ContribuicaoIncerteza::coeficienteSensibilidade() const
{
    return m_coeficienteSensibilidade;
}

double ContribuicaoIncerteza::valorCoeficienteSensibilidade() const
{
    return m_valorCoeficienteSensibilidade;
}

double ContribuicaoIncerteza::contribuicaoIncerteza() const
{
    return m_contribuicaoIncerteza;
}

int ContribuicaoIncerteza::grausDeLiberdade() const
{
    return m_grausDeLiberdade;
}

std::shared_ptr<Grandeza> ContribuicaoIncerteza::grandeza() const
{
    return m_grandeza;
}
