#include "paginamodelo.hpp"
#include "ui_paginamodelo.h"

#include "janelaprincipal.hpp"
#include "tipoaoub.hpp"
#include "tiporesultado.hpp"

#include <ginac/ginac.h>

#include <QFont>
#include <QFontDatabase>
#include <QFrame>
#include <QLabel>
#include <QLineEdit>
#include <QListWidget>
#include <QListWidgetItem>
#include <QMessageBox>
#include <QPushButton>
#include <QRegularExpression>

bool PaginaModelo::derivadasValidas = false;

PaginaModelo::PaginaModelo(QWidget* parent) :
    QWidget(parent), m_equacaoAlterada(true), m_ui(new Ui::PaginaModelo)
{
    m_ui->setupUi(this);

    // TODO: invisível até completar as correlações
    m_ui->pushButtonCorrelacoes->setVisible(false);

    QFont monoFonte {QFontDatabase::systemFont(QFontDatabase::FixedFont)};
    monoFonte.setBold(true);
    monoFonte.setPointSize(12);

    m_ui->lineEditModelo->setFont(monoFonte);

    // conecta o sinal para habilitar o botão Próximo
    connect(m_ui->lineEditModelo, &QLineEdit::editingFinished, this,
            &PaginaModelo::habilitarBotaoProximoEDerivadas);

    // conecta o sinal clicked do QPushButton ao sinal botaoProximoApertado
    // e ao botaoAnteriorApertado
    connect(m_ui->pushButtonAnterior, &QPushButton::clicked, this,
            &PaginaModelo::botaoAnteriorApertado);
    connect(m_ui->pushButtonProximo, &QPushButton::clicked, this,
            &PaginaModelo::botaoProximoApertado);

    // conecta o sinal clicked do QPushButton ao sinal botaoDerivadasApertado
    connect(m_ui->pushButtonDerivadas, &QPushButton::clicked, this,
            &PaginaModelo::botaoDerivadasApertado);

    // coleta os dados dos widgets filhos
    connect(this, &PaginaModelo::botaoProximoApertado, this, &PaginaModelo::coletarDadosResultado);
    connect(this, &PaginaModelo::botaoProximoApertado, this, &PaginaModelo::coletarDadosTipoAouB);

    connect(this, &PaginaModelo::botaoDerivadasApertado, this,
            &PaginaModelo::coletarDadosResultado);
    connect(this, &PaginaModelo::botaoDerivadasApertado, this, &PaginaModelo::coletarDadosTipoAouB);
}

PaginaModelo::~PaginaModelo()
{
    delete m_ui;
}

/***** dados do tipo resultado *****/

QString PaginaModelo::nomeGrandezaResultado() const
{
    return m_nomeGrandezaResultado;
}

QString PaginaModelo::definicaoGrandezaResultado() const
{
    return m_definicaoGrandezaResultado;
}

QString PaginaModelo::unidadeGrandezaResultado() const
{
    return m_unidadeGrandezaResultado;
}

/***** dados do tipo A o B *****/

// comuns a A e B

QVector<QString> PaginaModelo::nomeGrandezasAouB() const
{
    return m_nomeGrandezasAouB;
}

QVector<QString> PaginaModelo::definicaoGrandezasAouB() const
{
    return m_definicaoGrandezasAouB;
}

QVector<QString> PaginaModelo::unidadeGrandezasAouB() const
{
    return m_unidadeGrandezasAouB;
}

QVector<QString> PaginaModelo::tipoGrandezaAouB() const
{
    return m_tipoGrandezaAouB;
}

// tipo A

QVector<QVector<double>> PaginaModelo::dadosExperimentaisAouB() const
{
    return m_dadosExperimentaisAouB;
}

QVector<QString> PaginaModelo::avaliacaoIncertezaAouB() const
{
    return m_avaliacaoIncertezaAouB;
}

QVector<double> PaginaModelo::DPEAgrupadoAouB() const
{
    return m_DPEAgrupadoAouB;
}

QVector<unsigned int> PaginaModelo::numObsRIAouB() const
{
    return m_numObsRIAouB;
}

// tipo B

QVector<QString> PaginaModelo::distribuicao() const
{
    return m_distribuicao;
}

QVector<double> PaginaModelo::valorEstimado() const
{
    return m_valorEstimado;
}

QVector<double> PaginaModelo::incertezaExpandidaT() const
{
    return m_incertezaExpandidaT;
}

QVector<unsigned int> PaginaModelo::grausLiberdade() const
{
    return m_grausLiberdade;
}

QVector<double> PaginaModelo::nivelConfianca() const
{
    return m_nivelConfianca;
}

QVector<double> PaginaModelo::URTriMeiaLarguraLimites() const
{
    return m_URTriMeiaLarguraLimites;
}

QVector<double> PaginaModelo::incertezaExpandidaNormal() const
{
    return m_incertezaExpandidaNormal;
}

QVector<double> PaginaModelo::fatorAbrangencia() const
{
    return m_fatorAbrangencia;
}

QVector<double> PaginaModelo::incertezaPadrao() const
{
    return m_incertezaPadrao;
}

QVector<double> PaginaModelo::traMeiaLarguraLimites1() const
{
    return m_traMeiaLarguraLimites1;
}

QVector<double> PaginaModelo::traMeiaLarguraLimites2() const
{
    return m_traMeiaLarguraLimites2;
}

ModeloMatematico PaginaModelo::modeloMatematico() const
{
    return m_modeloMatematico;
}

// numero de grandezas excluindo a grandeza do resultado
int PaginaModelo::numeroGrandezas() const
{
    return m_numeroGrandezas;
}

/***** slots *****/

void PaginaModelo::habilitarBotaoProximoEDerivadas()
{
    if (validarModelo(lerModelo()))
    {
        m_ui->pushButtonProximo->setEnabled(true);
        m_ui->pushButtonDerivadas->setEnabled(true);

        m_ui->listWidgetGrandezas->setEnabled(true);

        if (m_equacaoAlterada)
        {
            criarListaPilhaGrandezas(m_grandezaResultado, m_modeloMatematico);
        }
    }
    else
    {
        m_ui->pushButtonProximo->setEnabled(false);
        m_ui->pushButtonDerivadas->setEnabled(false);

        m_ui->lineEditModelo->setFocus();

        m_ui->listWidgetGrandezas->setEnabled(false);
    }
}

void PaginaModelo::mudarGrandezaPilha(QListWidgetItem* atual, QListWidgetItem* anterior)
{
    if (!atual)
    {
        atual = anterior;
    }

    m_ui->stackedWidgetGrandezas->setCurrentIndex(m_ui->listWidgetGrandezas->row(atual));
}

void PaginaModelo::coletarDadosResultado()
{
    // dados do tipo resultado

    m_nomeGrandezaResultado = qobject_cast<TipoResultado*>(m_ui->stackedWidgetGrandezas->widget(0))
                                  ->nomeGrandezaResultado();
    m_definicaoGrandezaResultado = qobject_cast<TipoResultado*>(m_ui->stackedWidgetGrandezas->widget(0))
                                       ->definicaoGrandezaResultado();
    m_unidadeGrandezaResultado = qobject_cast<TipoResultado*>(m_ui->stackedWidgetGrandezas->widget(0))
                                     ->unidadeGrandezaResultado();

    if (m_unidadeGrandezaResultado.isEmpty() && JanelaPrincipal::apenasDerivadas)
    {
        QMessageBox::critical(this, tr("Unidade Inválida"),
                              tr("O campo Unidade da grandeza <b><font color=red>%1</font></b> "
                                 "deve possuir um valor válido")
                                  .arg(m_nomeGrandezaResultado));

        m_ui->pushButtonProximo->setEnabled(false);

        derivadasValidas = false;

        m_ui->listWidgetGrandezas->setCurrentRow(0);
        m_ui->stackedWidgetGrandezas->setCurrentIndex(0);
    }
    else
    {
        m_ui->pushButtonProximo->setEnabled(true);

        derivadasValidas = true;
    }
}

void PaginaModelo::coletarDadosTipoAouB()
{
    // comuns a A e B
    m_nomeGrandezasAouB.clear();
    m_definicaoGrandezasAouB.clear();
    m_unidadeGrandezasAouB.clear();
    m_tipoGrandezaAouB.clear();

    // tipo A
    m_dadosExperimentaisAouB.clear();
    m_avaliacaoIncertezaAouB.clear();
    m_DPEAgrupadoAouB.clear();
    m_numObsRIAouB.clear();

    // tipo B
    m_distribuicao.clear();
    m_valorEstimado.clear();
    m_incertezaExpandidaT.clear();
    m_grausLiberdade.clear();
    m_nivelConfianca.clear();
    m_URTriMeiaLarguraLimites.clear();
    m_incertezaExpandidaNormal.clear();
    m_fatorAbrangencia.clear();
    m_incertezaPadrao.clear();
    m_traMeiaLarguraLimites1.clear();
    m_traMeiaLarguraLimites2.clear();

    // dados do tipo A ou B
    if (m_ui->listWidgetGrandezas->count() == m_ui->stackedWidgetGrandezas->count())
    {
        int numGrandezaAouB = m_ui->stackedWidgetGrandezas->count();

        // variáveis para gerar lista com grandezas com unidades com valores inválidos
        QString nome;
        QStringList unidadesInvalidas;
        int primeira = -2;

        // widget 0 sempre dados do resultado
        for (int i = 1; i < numGrandezaAouB; ++i)
        {
            // comuns a A e B
            nome = qobject_cast<TipoAouB*>(m_ui->stackedWidgetGrandezas->widget(i))
                       ->nomeGrandezaTipoAouB();
            m_nomeGrandezasAouB.push_back(nome);

            m_definicaoGrandezasAouB.push_back(
                qobject_cast<TipoAouB*>(m_ui->stackedWidgetGrandezas->widget(i))
                    ->definicaoGrandezaTipoAouB());

            // verificar se um valor para a unidade existe
            auto unidade = qobject_cast<TipoAouB*>(m_ui->stackedWidgetGrandezas->widget(i))
                               ->unidadeGrandezaTipoAouB();
            if (unidade.isEmpty())
            {
                if (primeira == -2)
                {
                    primeira = i;
                }

                unidadesInvalidas.push_back(nome);
            }
            else
            {
                m_unidadeGrandezasAouB.push_back(unidade);
            }

            m_tipoGrandezaAouB.push_back(
                qobject_cast<TipoAouB*>(m_ui->stackedWidgetGrandezas->widget(i))
                    ->tipoGrandezaTipoAouB());

            // tipo A
            m_dadosExperimentaisAouB.push_back(
                qobject_cast<TipoAouB*>(m_ui->stackedWidgetGrandezas->widget(i))
                    ->dadosExperimentais());
            m_avaliacaoIncertezaAouB.push_back(
                qobject_cast<TipoAouB*>(m_ui->stackedWidgetGrandezas->widget(i))
                    ->avaliacaoIncerteza());
            m_DPEAgrupadoAouB.push_back(
                qobject_cast<TipoAouB*>(m_ui->stackedWidgetGrandezas->widget(i))
                    ->DPEAgrupado()
                    .toDouble());
            m_numObsRIAouB.push_back(
                qobject_cast<TipoAouB*>(m_ui->stackedWidgetGrandezas->widget(i))
                    ->numObsRI()
                    .toUInt());

            // tipo B
            m_distribuicao.push_back(
                qobject_cast<TipoAouB*>(m_ui->stackedWidgetGrandezas->widget(i))->distribuicao());
            m_valorEstimado.push_back(
                qobject_cast<TipoAouB*>(m_ui->stackedWidgetGrandezas->widget(i))
                    ->valorEstimado()
                    .toDouble());
            m_incertezaExpandidaT.push_back(
                qobject_cast<TipoAouB*>(m_ui->stackedWidgetGrandezas->widget(i))
                    ->incertezaExpandidaT()
                    .toDouble());
            m_grausLiberdade.push_back(
                qobject_cast<TipoAouB*>(m_ui->stackedWidgetGrandezas->widget(i))
                    ->grausLiberdade()
                    .toUInt());
            m_nivelConfianca.push_back(
                qobject_cast<TipoAouB*>(m_ui->stackedWidgetGrandezas->widget(i))
                    ->nivelConfianca()
                    .toDouble());
            m_URTriMeiaLarguraLimites.push_back(
                qobject_cast<TipoAouB*>(m_ui->stackedWidgetGrandezas->widget(i))
                    ->URTriMeiaLarguraLimites()
                    .toDouble());
            m_incertezaExpandidaNormal.push_back(
                qobject_cast<TipoAouB*>(m_ui->stackedWidgetGrandezas->widget(i))
                    ->incertezaExpandidaNormal()
                    .toDouble());
            m_fatorAbrangencia.push_back(
                qobject_cast<TipoAouB*>(m_ui->stackedWidgetGrandezas->widget(i))
                    ->fatorAbrangencia()
                    .toDouble());
            m_incertezaPadrao.push_back(
                qobject_cast<TipoAouB*>(m_ui->stackedWidgetGrandezas->widget(i))
                    ->incertezaPadrao()
                    .toDouble());
            m_traMeiaLarguraLimites1.push_back(
                qobject_cast<TipoAouB*>(m_ui->stackedWidgetGrandezas->widget(i))
                    ->traMeiaLarguraLimites1()
                    .toDouble());
            m_traMeiaLarguraLimites2.push_back(
                qobject_cast<TipoAouB*>(m_ui->stackedWidgetGrandezas->widget(i))
                    ->traMeiaLarguraLimites2()
                    .toDouble());
        }

        if (primeira != -2 && JanelaPrincipal::apenasDerivadas)
        {
            if (unidadesInvalidas.size() == 1)
            {
                QMessageBox::critical(this, tr("Unidade Inválida"),
                                      tr("O campo Unidade da grandeza <b><font "
                                         "color=red>%1</font></b> deve possuir um valor válido")
                                          .arg(unidadesInvalidas.join("")));
            }
            else
            {
                QMessageBox::critical(this, tr("Unidadse Inválidas"),
                                      tr("O campo Unidade das grandezas <b><font "
                                         "color=red>%1</font></b> devem possuir valores válidos")
                                          .arg(unidadesInvalidas.join(", ")));
            }

            if (!m_unidadeGrandezaResultado.isEmpty())
            {
                m_ui->listWidgetGrandezas->setCurrentRow(primeira);
                m_ui->stackedWidgetGrandezas->setCurrentIndex(primeira);
            }

            m_ui->pushButtonProximo->setEnabled(false);

            derivadasValidas = false;
        }
        else
        {
            m_ui->pushButtonProximo->setEnabled(true);

            derivadasValidas = true;
        }
    }
}

/***** private *****/

QStringList PaginaModelo::lerModelo()
{
    return m_ui->lineEditModelo->text().simplified().replace(" ", "").split(
        "=", QString::SkipEmptyParts);
}

bool PaginaModelo::validarModelo(QStringList equacao)
{
    QMessageBox mensagemCritica;
    mensagemCritica.setIcon(QMessageBox::Critical);
    mensagemCritica.setWindowTitle(tr("Erro"));

    // validação do modelo matemático
    // grandeza que representa o resultado
    if (equacao.size() != 2)
    {
        if (!equacao.isEmpty())
        {
            mensagemCritica.setText(tr("Modelo Inválido.\n\n"
                                       "A equação deve conter um sinal de igualdade.\n\n"
                                       "Por favor consulte a ajuda para mais informações (F1)."));
            mensagemCritica.exec();
        }

        return false;
    }

    m_grandezaResultado = equacao.value(0);
    m_grandezaOutras    = equacao.value(1);

    if (!m_grandezaResultado.contains(QRegularExpression("^\\w+$")))
    {
        mensagemCritica.setText(tr("Modelo Inválido.\n\n"
                                   "A equação pode conter somente uma variável à esquerda.\n\n"
                                   "Por favor consulte a ajuda para mais informações (F1)."));
        mensagemCritica.exec();

        return false;
    }

    if (m_grandezaOutras.contains(
            QRegularExpression(QString("\\b(%1)\\b").arg(m_grandezaResultado))))
    {
        mensagemCritica.setText(
            tr("<p>Modelo Inválido.<br><br>"
               "A variável <b><font color=red>%1</font></b> não pode aparecer nos "
               "dois lados da equação.<br><br>"
               "Por favor consulte a ajuda para mais informações (F1).</p>")
                .arg(m_grandezaResultado));
        mensagemCritica.exec();

        return false;
    }

    // equação contendo as outras grandeza
    try
    {
        ModeloMatematico modelo(m_grandezaOutras.toStdString());

        auto modeloStrings = modelo.simbolosStrings();

        std::vector<std::string> equacaoStrings;
        equacaoStrings.emplace_back(m_grandezaResultado.toStdString());
        equacaoStrings.insert(equacaoStrings.end(), modeloStrings.begin(), modeloStrings.end());

        if (m_equacaoStrings == equacaoStrings)
        {
            m_equacaoAlterada = false;
        }
        else
        {
            m_equacaoAlterada = true;
        }

        m_modeloMatematico = modelo;

        m_equacaoStrings = equacaoStrings;

        return true;
    }
    catch (const GiNaC::parse_error&)
    {
        mensagemCritica.setText(tr("<p>Modelo Inválido.<br><br>"
                                   "A expressão <b><font color=red>%1</font></b> contém termos "
                                   "inválidos.<br><br>"
                                   "Por favor consulte a ajuda para mais informações (F1).</p>")
                                    .arg(m_grandezaOutras));
        mensagemCritica.exec();

        return false;
    }

    return false;
}

void PaginaModelo::criarListaPilhaGrandezas(const QString& resultado,
                                            const ModeloMatematico& modelo)
{
    // limpa a lista, retirando todos os widgets
    m_ui->listWidgetGrandezas->clear();

    // limpa a pilha, retirando todos os widgets
    for (int i = m_ui->stackedWidgetGrandezas->count() - 1; i >= 0; --i)
    {
        QWidget* widget = m_ui->stackedWidgetGrandezas->widget(i);

        m_ui->stackedWidgetGrandezas->removeWidget(widget);
    }

    // primeiro item sempre a grandeza do resultado
    auto rotuloResultado = new QLabel(resultado);
    rotuloResultado->setAlignment(Qt::AlignCenter);
    rotuloResultado->setFrameStyle(QFrame::Panel | QFrame::Raised);
    m_ui->listWidgetGrandezas->setItemWidget(new QListWidgetItem(m_ui->listWidgetGrandezas),
                                             rotuloResultado);

    // primeiro widget sempre a grandeza do resultado
    m_ui->stackedWidgetGrandezas->addWidget(new TipoResultado(resultado, this));

    // itens que contem cada uma das outras grandezas
    for (const auto& simbolo : modelo.simbolosStrings())
    {
        auto rotuloGrandeza = new QLabel(QString::fromStdString(simbolo));
        rotuloGrandeza->setAlignment(Qt::AlignCenter);
        rotuloGrandeza->setFrameStyle(QFrame::Panel | QFrame::Raised);

        m_ui->listWidgetGrandezas->setItemWidget(new QListWidgetItem(m_ui->listWidgetGrandezas),
                                                 rotuloGrandeza);

        m_ui->stackedWidgetGrandezas->addWidget(
            new TipoAouB(QString::fromStdString(simbolo), this));
    }

    if (m_ui->listWidgetGrandezas->count() == m_ui->stackedWidgetGrandezas->count())
    {
        m_numeroGrandezas = m_ui->listWidgetGrandezas->count() - 1;
    }

    m_ui->listWidgetGrandezas->setCurrentRow(0);

    connect(m_ui->listWidgetGrandezas, &QListWidget::currentItemChanged, this,
            &PaginaModelo::mudarGrandezaPilha);
}
