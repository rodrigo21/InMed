#include "correlacao.hpp"

#include "grandezatipoa.hpp"

#include <iterator>
#include <numeric>

Correlacao::Correlacao(GrandezaTipoA grTipoA_q, GrandezaTipoA grTipoA_r) :
    m_desvio_q(
        grTipoA_q.desvio()),
    m_desvio_r(
        grTipoA_r.desvio())
{
    m_covariancia = std::inner_product(begin(m_desvio_q), end(m_desvio_q), begin(m_desvio_r), 0.0)
                    / (m_desvio_q.size() * (m_desvio_q.size() - 1));

    m_coeficienteCorrelacao = m_covariancia / ((grTipoA_q.incertezaPadrao()) * (grTipoA_r.incertezaPadrao()));
}

double Correlacao::covariancia() const
{
    return m_covariancia;
}

double Correlacao::coeficienteDecorrelacao() const
{
    return m_coeficienteCorrelacao;
}
