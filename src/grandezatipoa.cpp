#include "grandezatipoa.hpp"

#include "grandeza.hpp"
#include "incertezatipoa.hpp"

#include <ginac/ginac.h>

#include <algorithm>
#include <iterator>
#include <numeric>

GrandezaTipoA::GrandezaTipoA(const GiNaC::symbol& simboloGrandeza,
                             const std::string& definicao,
                             const std::string& unidade,
                             bool temCorrelacao,
                             const std::vector<double>& dadosExperimentais) :
    GrandezaTipoA(
        simboloGrandeza,
        definicao,
        unidade,
        temCorrelacao,
        dadosExperimentais,
        0.0,
        0)
{
    m_grausDeLiberdade = dadosExperimentais.size() - 1;
}

GrandezaTipoA::GrandezaTipoA(const GiNaC::symbol& simboloGrandeza,
                             const std::string& definicao,
                             const std::string& unidade,
                             bool temCorrelacao,
                             std::vector<double>
                                 dadosExperimentais,
                             double desvioPadraoAgrupado,
                             int numeroObservacoesAgrupadas) :
    Grandeza(simboloGrandeza,
             definicao,
             unidade),
    m_dadosExperimentais(std::move(dadosExperimentais)),
    m_desvio(m_dadosExperimentais.size()),
    m_numeroDadosObservados(m_dadosExperimentais.size())
{
    m_distribuicao = Distribuicao::Normal;

    m_valorEstimado = std::accumulate(begin(m_dadosExperimentais), end(m_dadosExperimentais), 0.0)
                      / m_numeroDadosObservados;

    m_grausDeLiberdade = numeroObservacoesAgrupadas - 1;

    // Cálculo do desvio
    std::transform(begin(m_dadosExperimentais), end(m_dadosExperimentais), begin(m_desvio),
                   [this](double j) { return j - m_valorEstimado; });

    IncertezaTipoA m_incerteza(m_desvio, desvioPadraoAgrupado, numeroObservacoesAgrupadas);

    m_incertezaPadrao = m_incerteza.incertezaPadraoA();

    m_possuiCorrelacao = temCorrelacao;
}

double GrandezaTipoA::incertezaPadrao()
{
    return m_incertezaPadrao;
}

bool GrandezaTipoA::possuiCorrelacao()
{
    return m_possuiCorrelacao;
}

std::vector<double> GrandezaTipoA::dadosExperimentais() const
{
    return m_dadosExperimentais;
}

std::vector<double> GrandezaTipoA::desvio() const
{
    return m_desvio;
}

int GrandezaTipoA::numeroDadosObservados() const
{
    return m_numeroDadosObservados;
}
