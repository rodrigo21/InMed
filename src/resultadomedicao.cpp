#include "resultadomedicao.hpp"

#include "grandeza.hpp"

#include <utility>

ResultadoMedicao::ResultadoMedicao(QObject* parent) :
    QObject(parent), m_valorResultado(0.0)
{
}

void ResultadoMedicao::setDadosMedicao(const ModeloMatematico& modelo,
                                       const GrandezaResultado& grandezaResultado,
                                       std::vector<std::shared_ptr<Grandeza>>
                                           grandezas)
{
    m_modelo = modelo;

    m_grandezaResultado = grandezaResultado;

    m_grandezas = std::move(grandezas);

    calcularValoresEstimados();

    calcularContribuicoesIncerteza();

    calcularValorResultado();
}

GiNaC::exmap ResultadoMedicao::mapaValorEstimadoGrandezas() const
{
    return m_mapaValorEstimadoGrandezas;
}

std::vector<ContribuicaoIncerteza> ResultadoMedicao::contribuicoes() const
{
    return m_contribuicoes;
}

double ResultadoMedicao::valorResultado() const
{
    return m_valorResultado;
}

void ResultadoMedicao::atualizar()
{
    calcularValoresEstimados();

    calcularContribuicoesIncerteza();

    calcularValorResultado();
}

void ResultadoMedicao::calcularValoresEstimados()
{
    m_mapaValorEstimadoGrandezas.clear();

    for (const auto& grandeza : m_grandezas)
    {
        m_mapaValorEstimadoGrandezas[grandeza->simboloGrandeza()] = grandeza->valorEstimado();
    }
}

void ResultadoMedicao::calcularValorResultado()
{
    m_valorResultado = m_grandezaResultado.valorEstimado(m_modelo, m_mapaValorEstimadoGrandezas);
}

void ResultadoMedicao::calcularContribuicoesIncerteza()
{
    m_contribuicoes.clear();

    for (const auto& grandeza : m_grandezas)
    {
        ContribuicaoIncerteza contribuicao(grandeza, m_modelo, m_mapaValorEstimadoGrandezas);

        m_contribuicoes.push_back(contribuicao);
    }
}
