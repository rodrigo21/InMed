#include "dadosmedicao.hpp"

#include "grandeza.hpp"
#include "grandezatipoa.hpp"
#include "grandezatipob.hpp"

#include <ginac/ginac.h>

#include <cmath>
#include <string>
#include <utility>

DadosMedicao::DadosMedicao(QObject* parent) :
    QObject(parent), m_numeroGrandezas(0), m_grandezasCorretas(true)
{
}

// dados da PaginaTitulo (0)

void DadosMedicao::setTitulo(const QString& titulo)
{
    m_titulo = titulo;
}

void DadosMedicao::setAutor(const QString& autor)
{
    m_autor = autor;
}

void DadosMedicao::setData(const QString& data)
{
    m_data = data;
}

void DadosMedicao::setReferencia(const QString& referencia)
{
    m_referencia = referencia;
}

void DadosMedicao::setVersao(const QString& versao)
{
    m_versao = versao;
}

void DadosMedicao::setDescricaoMedicao(const QString& descricao)
{
    m_descricaoMedicao = descricao;
}

// dados da PaginaModelo (1)

// dados do tipo resultado

void DadosMedicao::setNomeGrandezaResultado(const QString& nomeGrandezaResultado)
{
    m_nomeGrandezaResultado = nomeGrandezaResultado;
}

void DadosMedicao::setDefinicaoGrandezaResultado(const QString& definicaoGrandezaResultado)
{
    m_definicaoGrandezaResultado = definicaoGrandezaResultado;
}

void DadosMedicao::setUnidadeGrandezaResultado(const QString& unidadeGrandezaResultado)
{
    m_unidadeGrandezaResultado = unidadeGrandezaResultado;
}

// dados do tipo A ou B

// comuns a A e B

void DadosMedicao::setNomeGrandezasAouB(const QVector<QString>& nomeGrandezasAouB)
{
    m_nomeGrandezasAouB = nomeGrandezasAouB;
}

void DadosMedicao::setDefinicaoGrandezasAouB(const QVector<QString>& definicaoGrandezasAouB)
{
    m_definicaoGrandezasAouB = definicaoGrandezasAouB;
}

void DadosMedicao::setUnidadeGrandezasAouB(const QVector<QString>& unidadeGrandezasAouB)
{
    m_unidadeGrandezasAouB = unidadeGrandezasAouB;
}

void DadosMedicao::setTipoGrandezaAouB(const QVector<QString>& tipoGrandezaAouB)
{
    m_tipoGrandezaAouB = tipoGrandezaAouB;
}

void DadosMedicao::setCorrecaoGrandezaAouB(const QVector<QString>& correcaoGrandezaAouB)
{
    m_correcaoAouB = correcaoGrandezaAouB;
}

// tipo A

void DadosMedicao::setDadosExperimentaisAouB(const QVector<QVector<double>>& dadosExperimentaisAouB)
{
    m_dadosExperimentaisAouB = dadosExperimentaisAouB;
}

void DadosMedicao::setAvaliacaoIncertezaAouB(const QVector<QString>& avaliacaoIncertezaAouB)
{
    m_avaliacaoIncertezaAouB = avaliacaoIncertezaAouB;
}

void DadosMedicao::setDPEAgrupadoAouB(const QVector<double>& DPEAgrupadoAouB)
{
    m_DPEAgrupadoAouB = DPEAgrupadoAouB;
}

void DadosMedicao::setNumObsRIAouB(const QVector<unsigned int>& numObsRIAouB)
{
    m_numObsRIAouB = numObsRIAouB;
}

// tipo B

void DadosMedicao::setDistribuicao(const QVector<QString>& distribuicao)
{
    m_distribuicao = distribuicao;
}

void DadosMedicao::setValorEstimado(const QVector<double>& valorEstimado)
{
    m_valorEstimado = valorEstimado;
}

void DadosMedicao::setIncertezaExpandidaT(const QVector<double>& incertezaExpandidaT)
{
    m_incertezaExpandidaT = incertezaExpandidaT;
}

void DadosMedicao::setGrausLiberdade(const QVector<unsigned int>& grausLiberdade)
{
    m_grausLiberdade = grausLiberdade;
}

void DadosMedicao::setNivelConfianca(const QVector<double>& nivelConfianca)
{
    m_nivelConfianca = nivelConfianca;
}

void DadosMedicao::setURTriMeiaLarguraLimites(const QVector<double>& URTriMeiaLarguraLimites)
{
    m_URTriMeiaLarguraLimites = URTriMeiaLarguraLimites;
}

void DadosMedicao::setIncertezaExpandidaNormal(const QVector<double>& incertezaExpandidaNormal)
{
    m_incertezaExpandidaNormal = incertezaExpandidaNormal;
}

void DadosMedicao::setFatorAbrangencia(const QVector<double>& fatorAbrangencia)
{
    m_fatorAbrangencia = fatorAbrangencia;
}

void DadosMedicao::setIncertezaPadrao(const QVector<double>& incertezaPadrao)
{
    m_incertezaPadrao = incertezaPadrao;
}

void DadosMedicao::setTraMeiaLarguraLimites1(const QVector<double>& traMeiaLarguraLimites1)
{
    m_traMeiaLarguraLimites1 = traMeiaLarguraLimites1;
}

void DadosMedicao::setTraMeiaLarguraLimites2(const QVector<double>& traMeiaLarguraLimites2)
{
    m_traMeiaLarguraLimites2 = traMeiaLarguraLimites2;
}

void DadosMedicao::setNumeroGrandezas(int numeroGrandezas)
{
    m_numeroGrandezas = numeroGrandezas;
}

ModeloMatematico DadosMedicao::modeloMatematico() const
{
    return m_modeloMatematico;
}

void DadosMedicao::setModeloMatematico(const ModeloMatematico& modeloMatematico)
{
    m_modeloMatematico = modeloMatematico;
}

// criação das grandezas

GrandezaResultado DadosMedicao::grandezaResultado()
{
    return std::move(GrandezaResultado(GiNaC::symbol(m_nomeGrandezaResultado.toStdString()),
                                       m_definicaoGrandezaResultado.toStdString(),
                                       m_unidadeGrandezaResultado.toStdString()));
}

std::vector<std::shared_ptr<Grandeza>> DadosMedicao::grandezas()
{
    m_grandezas.clear();

    if (m_unidadeGrandezasAouB.size() == m_numeroGrandezas)
    {
        QStringList grandezasInvalidasDadosExp;
        QStringList grandezasErroValoresDPEA;
        QStringList grandezasErroValoresDistribuicao;

        for (int i = 0; i < m_numeroGrandezas; ++i)
        {
            // comuns a A e B

            auto simbolo   = modeloMatematico().simbolos()[i];
            auto definicao = m_definicaoGrandezasAouB[i].toStdString();
            auto unidade   = m_unidadeGrandezasAouB[i].toStdString();
            auto tipo      = m_tipoGrandezaAouB[i].toStdString();
            auto correcao  = m_correcaoAouB[i].toStdString();

            // tipo A

            auto dadosExperimentais = m_dadosExperimentaisAouB[i].toStdVector();
            auto avaliacaoIncerteza = m_avaliacaoIncertezaAouB[i].toStdString();
            auto DPEAgrupado        = m_DPEAgrupadoAouB[i];
            auto numObsRI           = m_numObsRIAouB[i];

            // tipo B

            // comuns para B
            auto distribuicao  = m_distribuicao[i].toStdString();
            auto valorEstimado = m_valorEstimado[i];
            // distribuição-t
            auto incertezaExpandidaT = m_incertezaExpandidaT[i];
            auto grausLiberdade      = m_grausLiberdade[i];
            auto nivelConfianca      = m_nivelConfianca[i];
            // comum para em forma de U, retangular, triangular
            auto URTriMeiaLarguraLimites = m_URTriMeiaLarguraLimites[i];
            // normal
            auto incertezaExpandidaNormal = m_incertezaExpandidaNormal[i];
            auto fatorAbrangencia         = m_fatorAbrangencia[i];
            // outras
            auto incertezaPadrao = m_incertezaPadrao[i];
            // trapezoidal
            auto traMeiaLarguraLimites1 = m_traMeiaLarguraLimites1[i];
            auto traMeiaLarguraLimites2 = m_traMeiaLarguraLimites2[i];

            // tipo A

            if (tipo == "Tipo A")
            {
                if (dadosExperimentais.size() > 1)
                {
                    bool possuiCorrelacao = false;

                    if (avaliacaoIncerteza == "Experimental")
                    {
                        m_grandezas.emplace_back(std::make_shared<GrandezaTipoA>(
                            simbolo, definicao, unidade, possuiCorrelacao,
                            dadosExperimentais));

                        m_grandezasCorretas = true;
                    }
                    else
                    {
                        if (DPEAgrupado == 0 || numObsRI == 0)
                        {
                            grandezasErroValoresDPEA.push_back(m_nomeGrandezasAouB[i]);

                            m_grandezasCorretas = false;
                        }
                        else
                        {
                            m_grandezas.emplace_back(std::make_shared<GrandezaTipoA>(
                                simbolo, definicao, unidade,
                                possuiCorrelacao, dadosExperimentais,
                                DPEAgrupado, numObsRI));

                            m_grandezasCorretas = true;
                        }
                    }
                }
                else
                {
                    m_grandezasCorretas = false;

                    grandezasInvalidasDadosExp.push_back(m_nomeGrandezasAouB[i]);
                }
            }

            // tipo B

            if (tipo == "Tipo B")
            {
                // TODO: Bug valorEstimado 0
                if (!std::isnan(valorEstimado))
                {
                    // distribuição-t
                    if (distribuicao == "Distribuição-t")
                    {
                        if (incertezaExpandidaT == 0 || grausLiberdade == 0 || nivelConfianca == 0)
                        {
                            grandezasErroValoresDistribuicao.push_back(m_nomeGrandezasAouB[i]);

                            m_grandezasCorretas = false;
                        }
                        else
                        {
                            m_grandezas.emplace_back(std::make_shared<GrandezaTipoB>(
                                simbolo, definicao, unidade, valorEstimado,
                                grausLiberdade,
                                Distribuicao::T, incertezaExpandidaT,
                                nivelConfianca));

                            m_grandezasCorretas = true;
                        }
                    }

                    // comum para em forma de U, retangular, triangular
                    if (distribuicao == "Em forma de U" || distribuicao == "Retangular" || distribuicao == "Triangular")
                    {
                        if (URTriMeiaLarguraLimites == 0)
                        {
                            grandezasErroValoresDistribuicao.push_back(m_nomeGrandezasAouB[i]);

                            m_grandezasCorretas = false;
                        }
                        else
                        {
                            if (distribuicao == "Em forma de U")
                            {
                                m_grandezas.emplace_back(std::make_shared<GrandezaTipoB>(
                                    simbolo, definicao, unidade,
                                    valorEstimado, grausLiberdade,
                                    Distribuicao::FormaU,
                                    URTriMeiaLarguraLimites, 0));
                            }

                            if (distribuicao == "Retangular")
                            {
                                m_grandezas.emplace_back(std::make_shared<GrandezaTipoB>(
                                    simbolo, definicao, unidade,
                                    valorEstimado, grausLiberdade,
                                    Distribuicao::Retangular,
                                    URTriMeiaLarguraLimites, 0));
                            }

                            if (distribuicao == "Triangular")
                            {
                                m_grandezas.emplace_back(std::make_shared<GrandezaTipoB>(
                                    simbolo, definicao, unidade,
                                    valorEstimado, grausLiberdade,
                                    Distribuicao::Triangular,
                                    URTriMeiaLarguraLimites, 0));
                            }

                            m_grandezasCorretas = true;
                        }
                    }

                    // normal
                    if (distribuicao == "Normal")
                    {
                        if (incertezaExpandidaNormal == 0)
                        {
                            grandezasErroValoresDistribuicao.push_back(m_nomeGrandezasAouB[i]);

                            m_grandezasCorretas = false;
                        }
                        else
                        {
                            if (fatorAbrangencia == 0)
                            {
                                fatorAbrangencia = 2;
                            }

                            m_grandezas.emplace_back(std::make_shared<GrandezaTipoB>(
                                simbolo, definicao, unidade, valorEstimado,
                                grausLiberdade,
                                Distribuicao::Normal,
                                incertezaExpandidaNormal,
                                fatorAbrangencia));

                            m_grandezasCorretas = true;
                        }
                    }

                    // outras
                    if (distribuicao == "Outras")
                    {
                        if (incertezaPadrao == 0)
                        {
                            grandezasErroValoresDistribuicao.push_back(m_nomeGrandezasAouB[i]);

                            m_grandezasCorretas = false;
                        }
                        else
                        {
                            m_grandezas.emplace_back(std::make_shared<GrandezaTipoB>(
                                simbolo, definicao, unidade, valorEstimado,
                                grausLiberdade,
                                Distribuicao::Outras, incertezaPadrao, 0));

                            m_grandezasCorretas = true;
                        }
                    }

                    // trapezoidal
                    if (distribuicao == "Trapezoidal")
                    {
                        if (traMeiaLarguraLimites1 == 0 || traMeiaLarguraLimites2 == 0)
                        {
                            grandezasErroValoresDistribuicao.push_back(m_nomeGrandezasAouB[i]);

                            m_grandezasCorretas = false;
                        }
                        else
                        {
                            m_grandezas.emplace_back(std::make_shared<GrandezaTipoB>(
                                simbolo, definicao, unidade, valorEstimado,
                                grausLiberdade,
                                Distribuicao::Trapezoidal,
                                traMeiaLarguraLimites1,
                                traMeiaLarguraLimites2));

                            m_grandezasCorretas = true;
                        }
                    }
                }
                else
                {
                    grandezasErroValoresDistribuicao.push_back(m_nomeGrandezasAouB[i]);

                    m_grandezasCorretas = false;
                }
            }
        }

        if (!grandezasInvalidasDadosExp.empty())
        {
            emit invalidosDadosExperimentais(grandezasInvalidasDadosExp);
        }
        else if (!grandezasErroValoresDPEA.empty())
        {
            emit erroValoresDPEA(grandezasErroValoresDPEA);
        }
        else if (!grandezasErroValoresDistribuicao.empty())
        {
            emit erroValoresDistribuicao(grandezasErroValoresDistribuicao);
        }
    }
    else
    {
        m_grandezasCorretas = false;
    }

    return m_grandezas;
}

bool DadosMedicao::grandezasCorretas() const
{
    return m_grandezasCorretas;
}
