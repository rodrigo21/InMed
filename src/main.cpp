// InMed - main.cpp

#include "inmedconfig.hpp"
#include "janelaprincipal.hpp"
#include "menucli.hpp"

#include <QApplication>
#include <QCommandLineParser>
#include <QDesktopWidget>
#include <QRect>

#include <exception>
#include <iostream>

int main(int argc, char* argv[])
{
    QApplication inmed(argc, argv);
    QApplication::setApplicationDisplayName("InMed");
    QApplication::setApplicationVersion(QString::fromStdString(InMed_VERSION_SHORT));

    QCommandLineParser analisador;
    analisador.setApplicationDescription(
        QApplication::translate("main", "Programa para análise de incertezas de medição"));
    analisador.addHelpOption();
    analisador.addVersionOption();
    analisador.addOption(
        {{"c", "cli"},
         QApplication::translate("main", "Inicia o programa no modo linha de comando (cli)")});
    analisador.process(inmed);

    bool usarCLI = analisador.isSet("cli");

    if (usarCLI)
    {
        try
        {
            menucli();
        }
        catch (std::exception& e)
        {
            std::cerr << e.what() << '\n';
        }

        return 0;
    }

    JanelaPrincipal janela;

    const QRect geometriaDisponivel = QApplication::desktop()->availableGeometry(&janela);
    janela.resize(geometriaDisponivel.width() / 2, (geometriaDisponivel.height() * 2) / 3);
    janela.move((geometriaDisponivel.width() - janela.width()) / 2,
                (geometriaDisponivel.height() - janela.height()) / 2);

    janela.show();

    return inmed.exec();
}
