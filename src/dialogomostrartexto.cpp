#include "dialogomostrartexto.hpp"
#include "ui_dialogomostrartexto.h"

#include <QByteArray>
#include <QDialogButtonBox>
#include <QFile>
#include <QFont>
#include <QFontDatabase>
#include <QIODevice>
#include <QMessageBox>
#include <QTextEdit>

DialogoMostrarTexto::DialogoMostrarTexto(const QString& arquivo,
                                         const QString& titulo,
                                         QWidget* parent) :
    QDialog(parent), m_ui(new Ui::DialogoMostrarTexto)
{
    m_ui->setupUi(this);

    setWindowTitle(titulo);

    carregarArquivoTexto(arquivo);

    connect(m_ui->buttonBox, &QDialogButtonBox::accepted, this, &DialogoMostrarTexto::accept);
}

void DialogoMostrarTexto::carregarArquivoTexto(const QString& arquivoNome)
{
    QFile arquivo(arquivoNome);
    if (!arquivo.exists())
    {
        QString mensagem(tr("Arquivo não encontrado: %1"));
        mensagem = mensagem.arg(arquivoNome);

        QMessageBox caixaMensagem(QMessageBox::Critical, tr("InMed"), mensagem, QMessageBox::Ok,
                                  this);
        caixaMensagem.exec();

        return;
    }

    arquivo.open(QIODevice::ReadOnly | QIODevice::Text);
    if (!arquivo.isReadable())
    {
        QString mensagem(tr("Não foi possível ler o arquivo %1"));
        mensagem = mensagem.arg(arquivoNome);

        QMessageBox caixaMensagem(QMessageBox::Critical, tr("InMed"), mensagem, QMessageBox::Ok,
                                  this);
        caixaMensagem.exec();

        arquivo.close();

        return;
    }

    auto arquivoDados = arquivo.readAll();
    arquivo.close();

    QFont monoFonte {QFontDatabase::systemFont(QFontDatabase::FixedFont)};

    m_ui->textEditTexto->setPlainText(arquivoDados);
    m_ui->textEditTexto->setReadOnly(true);
    m_ui->textEditTexto->setFont(monoFonte);
}

DialogoMostrarTexto::~DialogoMostrarTexto()
{
    delete m_ui;
}
