#include "funcoesextras.hpp"

#include "contribuicaoincerteza.hpp"
#include "incertezacombinada.hpp"

#include <QFontMetrics>
#include <QLineEdit>
#include <QPlainTextEdit>

#include <boost/math/distributions/complement.hpp>
#include <boost/math/distributions/students_t.hpp>

#include <cmath>
#include <map>
#include <vector>

// cálculo das incertezas

double kStudentsT(unsigned int grausDeLiberdade, double fatorP)
{
    boost::math::students_t distribuicao(grausDeLiberdade);

    return boost::math::quantile(boost::math::complement(distribuicao, (1 - (fatorP / 100)) / 2));
}

unsigned int grausLiberdadeEfetivo(std::vector<ContribuicaoIncerteza> contribuicoes,
                                   const IncertezaCombinada& incertezaCombinada)
{
    double veffDenon = 0.0;
    for (const auto& j : contribuicoes)
    {
        if (j.grausDeLiberdade() < 1)
        {
            continue;
        }
        else
        {
            veffDenon += ((std::pow(j.contribuicaoIncerteza(), 4)) / j.grausDeLiberdade());
        }
    }

    double veffNum = std::pow(incertezaCombinada.incertezaCombinada(), 4);

    return std::trunc(veffNum / veffDenon);
}

double fatorDeAbrangencia(double nivelDaConfianca)
{
    std::map<double, double> kp = {{68.27, 1}, {90, 1.645}, {95, 1.960}, {95.45, 2}, {99, 2.576}, {99.73, 3}};

    if (kp.find(nivelDaConfianca) != kp.end())
    {
        return kp[nivelDaConfianca];
    }

    return 0;
}

void ajustarPlainTextEdit(QPlainTextEdit* editor,
                          int numLinhas,
                          bool tabMudaFoco,
                          bool isAlturaFixa,
                          QSizePolicy::Policy horizontal,
                          QSizePolicy::Policy vertical)
{
    QFontMetrics fm(editor->font());

    int alturaLinha = fm.lineSpacing();

    if (isAlturaFixa)
    {
        editor->setFixedHeight(numLinhas * alturaLinha);
    }

    editor->setTabChangesFocus(tabMudaFoco);
    editor->setSizePolicy(horizontal, vertical);
}

void ajustarLineEdit(QLineEdit* editor,
                     int pixelsAdicionalLargura,
                     int pixelsAdicionalAltura,
                     bool isApenasLeitura,
                     bool isTamanhoFixo,
                     Qt::FocusPolicy foco,
                     QSizePolicy::Policy horizontal,
                     QSizePolicy::Policy vertical)
{
    editor->setReadOnly(isApenasLeitura);

    QFontMetrics fm(editor->font());

    int pixelsLargura = fm.horizontalAdvance(editor->text());
    int pixelsAltura  = fm.height();

    editor->setFocusPolicy(foco);

    if (isTamanhoFixo)
    {
        editor->setFixedSize(pixelsLargura + pixelsAdicionalLargura,
                             pixelsAltura + pixelsAdicionalAltura);
    }
    else
    {
        editor->setSizePolicy(horizontal, vertical);
    }
}

QString distribuicaoQString(Distribuicao distribuicao)
{
    if (distribuicao == Distribuicao::FormaU)
    {
        return "Em forma de U";
    }

    if (distribuicao == Distribuicao::Normal)
    {
        return "Normal";
    }

    if (distribuicao == Distribuicao::Outras)
    {
        return "Outras";
    }

    if (distribuicao == Distribuicao::Retangular)
    {
        return "Retangular";
    }

    if (distribuicao == Distribuicao::T)
    {
        return "Distribuição-t";
    }

    if (distribuicao == Distribuicao::Trapezoidal)
    {
        return "Trapezoidal";
    }

    if (distribuicao == Distribuicao::Triangular)
    {
        return "Triangular";
    }

    return "";
}

Conformidade avaliacaoConformidade(double y, double U, double limiteInferior, double limiteSuperior)
{
    // ISO 14253-1: 5.2
    // comprovar a conformidade com as especificações
    if ((limiteInferior < y - U) && (y + U < limiteSuperior))
    {
        return Conformidade::Conforme;
    }
    // ISO 14253-1: 5.3
    // comprovar a não conformidade com as especificações
    else if ((y + U < limiteInferior) || (limiteSuperior < y - U))
    {
        return Conformidade::NaoConforme;
    }
    // ISO 14253-1: 5.4
    // nem conformidade nem não conformidade é provado
    else /*if (((y - U < limiteInferior) && (limiteInferior < y + U))
         || ((y - U < limiteSuperior) && (limiteSuperior < y + U)))*/
    {
        return Conformidade::NaoDeterminada;
    }
}
