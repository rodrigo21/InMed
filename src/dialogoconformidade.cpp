#include "dialogoconformidade.hpp"
#include "ui_dialogoconformidade.h"

#include "funcoesextras.hpp"

#include <QMessageBox>
#include <QPushButton>
#include <QSizePolicy>

DialogoConformidade::DialogoConformidade(double resultadoMedicao,
                                         double incertezaExpandida,
                                         QString unidade,
                                         QWidget* parent) :
    QDialog(parent), m_resultadoMedicao(resultadoMedicao), m_incertezaExpandida(incertezaExpandida), m_unidade(unidade), m_ui(new Ui::DialogoConformidade)
{
    m_ui->setupUi(this);

    QSizePolicy sp_retain = m_ui->graficoAvaliacao->sizePolicy();
    sp_retain.setRetainSizeWhenHidden(true);
    m_ui->graficoAvaliacao->setSizePolicy(sp_retain);
    m_ui->graficoAvaliacao->hide();

    m_ui->labelUnidadeValorNominal->setText(unidade);
    m_ui->labelUnidadeToleranciaInferior->setText(unidade);
    m_ui->labelUnidadeToleranciaSuperior->setText(unidade);

    connect(m_ui->pushButtonAvaliar, &QPushButton::clicked, this,
            &DialogoConformidade::avaliarConformidade);
}

DialogoConformidade::~DialogoConformidade()
{
    delete m_ui;
}

void DialogoConformidade::avaliarConformidade()
{
    bool okVN;
    bool okInf;
    bool okSup;
    double valorNominal       = m_ui->lineEditValorNominal->text().toDouble(&okVN);
    double toleranciaInferior = m_ui->lineEditToleranciaInferior->text().toDouble(&okInf);
    double toleranciaSuperior = m_ui->lineEditToleranciaSuperior->text().toDouble(&okSup);

    if (okInf && okSup)
    {
        double limiteInferior = valorNominal - toleranciaInferior;
        double limiteSuperior = valorNominal + toleranciaSuperior;

        if (limiteInferior < limiteSuperior)
        {
            auto conformidade = avaliacaoConformidade(m_resultadoMedicao, m_incertezaExpandida,
                                                      limiteInferior, limiteSuperior);

            m_ui->graficoAvaliacao->avaliarConformidade(m_resultadoMedicao, m_incertezaExpandida,
                                                        m_unidade, valorNominal, limiteInferior,
                                                        limiteSuperior, conformidade);

            m_ui->graficoAvaliacao->show();
        }
        else
        {
            m_ui->graficoAvaliacao->hide();

            QMessageBox::critical(
                this, tr("Valores Inválidos"),
                tr("O valor do limite inferior deve ser menor que o valor do limite superior"));
        }
    }
    else
    {
        m_ui->graficoAvaliacao->hide();

        QMessageBox::critical(this, tr("Valores Inválidos"),
                              tr("O valor do limite inferior ou superior é inválido"));
    }
}
