#include "modelomatematico.hpp"

#include <ginac/ginac.h>

#include <utility>

ModeloMatematico::ModeloMatematico(std::string entrada)
{
    if (entrada != "Invalid_Model")
    {
        GiNaC::parser leitor;
        m_modelo = leitor(std::move(entrada));

        const GiNaC::symtab grandezas = leitor.get_syms();

        for (const auto& grandeza : grandezas)
        {
            m_simbolos.emplace_back(GiNaC::ex_to<GiNaC::symbol>(grandeza.second));
        }

        for (const auto& simbolo : m_simbolos)
        {
            m_vectorSimbolosStrings.emplace_back(simbolo.get_name());
        }
    }
}

GiNaC::ex ModeloMatematico::modelo() const
{
    return m_modelo;
}

std::vector<GiNaC::symbol> ModeloMatematico::simbolos() const
{
    return m_simbolos;
}

std::vector<std::string> ModeloMatematico::simbolosStrings() const
{
    return m_vectorSimbolosStrings;
}
