#include "dialogosobre.hpp"
#include "ui_dialogosobre.h"

#include <QDialogButtonBox>
#include <QLabel>

DialogoSobre::DialogoSobre(const QString& titulo, const QString& versao, QWidget* parent) :
    QDialog(parent), m_ui(new Ui::DialogoSobre)
{
    m_ui->setupUi(this);

    setWindowTitle(titulo);

    m_ui->labelVersao->setText(m_ui->labelVersao->text().arg(versao));

    QString url = "<a href=\"http://gitlab.com/rodrigo21/InMed/\">https://gitlab.com/rodrigo21/InMed</a>";
    m_ui->labelSite->setText(m_ui->labelSite->text().arg(url));

    connect(m_ui->buttonBox, &QDialogButtonBox::accepted, this, &DialogoSobre::accept);
}

DialogoSobre::~DialogoSobre()
{
    delete m_ui;
}
