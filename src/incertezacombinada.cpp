#include "incertezacombinada.hpp"

#include "contribuicaoincerteza.hpp"
#include "correlacao.hpp"
#include "grandeza.hpp"
#include "grandezatipoa.hpp"

#include <algorithm>
#include <cmath>
#include <memory>

IncertezaCombinada::IncertezaCombinada(std::vector<ContribuicaoIncerteza> contribuicoes) :
    m_contribuicaoes(contribuicoes)
{
    // primeira parte da equação 16 (ISO GUM)
    double varianciaCombinadaNaoCorrelacionada = 0.0;
    for (const auto& i : contribuicoes)
    {
        varianciaCombinadaNaoCorrelacionada += (i.contribuicaoIncerteza() * i.contribuicaoIncerteza());
    }

    // segunda parte da equação 16 (ISO GUM)
    double varianciaCombinadaCorrelacionada = 0.0;
    for (unsigned int i = 0; i < contribuicoes.size() - 1; ++i)
    {
        for (unsigned int j = i + 1; j < contribuicoes.size(); ++j)
        {
            if (contribuicoes[i].grandeza()->possuiCorrelacao() && contribuicoes[j].grandeza()->possuiCorrelacao())
            {
                varianciaCombinadaCorrelacionada += (Correlacao(
                                                         *(std::static_pointer_cast<GrandezaTipoA>(contribuicoes[i].grandeza())),
                                                         *(std::static_pointer_cast<GrandezaTipoA>(contribuicoes[j].grandeza())))
                                                         .coeficienteDecorrelacao())
                                                    * contribuicoes[i].contribuicaoIncerteza()
                                                    * contribuicoes[j].contribuicaoIncerteza();
            }
        }
    }

    m_incertezaCombinada = std::sqrt(varianciaCombinadaNaoCorrelacionada + 2 * varianciaCombinadaCorrelacionada);
}

double IncertezaCombinada::incertezaCombinada() const
{
    return m_incertezaCombinada;
}

std::vector<ContribuicaoIncerteza> IncertezaCombinada::contribuicaoes() const
{
    return m_contribuicaoes;
}
