#include "arearenderizacao.hpp"

#include <QBrush>
#include <QFont>
#include <QFontMetrics>
#include <QPainter>
#include <QPainterPath>
#include <QPalette>
#include <QPen>
#include <QRectF>

class QPaintEvent;

AreaRenderizacao::AreaRenderizacao(QWidget* parent) :
    QWidget(parent), m_y(0.0), m_U(0.0), m_unidade(""), m_limiteInferior(0.0), m_limiteSuperior(0.0), m_conformidade(Conformidade::NaoDeterminada)
{
    setBackgroundRole(QPalette::Base);
    setAutoFillBackground(true);
}

void AreaRenderizacao::avaliarConformidade(double y,
                                           double U,
                                           QString unidade,
                                           double valorNominal,
                                           double limiteInferior,
                                           double limiteSuperior,
                                           Conformidade conformidade)
{
    m_y              = y;
    m_U              = U;
    m_unidade        = unidade;
    m_valorNominal   = valorNominal;
    m_limiteInferior = limiteInferior;
    m_limiteSuperior = limiteSuperior;
    m_conformidade   = conformidade;

    update();
}

void AreaRenderizacao::paintEvent(QPaintEvent* /*event*/)
{
    QPainter pintor(this);
    pintor.setRenderHint(QPainter::Antialiasing, true);

    QPen caneta(Qt::black, 2, Qt::SolidLine);
    pintor.setPen(caneta);

    // eixo principal
    // comprimento = 520px
    pintor.drawLine(60, 240, 580, 240);
    desenharTriangulo(580, 235, 10, 10, DirecaoTriangulo::Direita, pintor, Qt::black);

    QRectF limites;
    double escala = 0.0;
    double x      = 0.0;
    YPosicao yPos;

    if (m_conformidade == Conformidade::Conforme)
    {
        limites = QRectF(120, 230, 400, 20);

        escala = limites.width() / (m_limiteSuperior - m_limiteInferior);

        // valor de y
        x = ((limites.width() * (m_y - m_limiteInferior))
             + (limites.left() * (m_limiteSuperior - m_limiteInferior)))
            / (m_limiteSuperior - m_limiteInferior);
        yPos = YPosicao::EL;
    }
    else if (m_conformidade == Conformidade::NaoConforme)
    {
        limites = QRectF(220, 230, 200, 20);

        // valor de y
        if (m_y < m_limiteInferior)
        {
            x    = 120;
            yPos = YPosicao::EsqLI;
        }
        else
        {
            x    = 520;
            yPos = YPosicao::DirLS;
        }
    }
    else // Não Determinado
    {
        limites = QRectF(170, 230, 300, 20);

        double larguraAlcanceIncerteza = 100;

        // valor de y
        if (m_y < m_limiteInferior)
        {
            x    = limites.left() - (larguraAlcanceIncerteza / 4);
            yPos = YPosicao::EsqLI;
        }
        else if (m_y == m_limiteInferior)
        {
            x    = limites.left();
            yPos = YPosicao::LimInf;
        }
        else if (m_y > m_limiteInferior && m_y <= (m_limiteInferior + ((m_limiteSuperior - m_limiteInferior) / 2)))
        {
            x    = limites.left() + (larguraAlcanceIncerteza / 4);
            yPos = YPosicao::DirLI;
        }
        else if (m_y < m_limiteSuperior && m_y >= (m_limiteSuperior - ((m_limiteSuperior - m_limiteInferior) / 2)))
        {
            x    = limites.right() - (larguraAlcanceIncerteza / 4);
            yPos = YPosicao::EsqLS;
        }
        else if (m_y == m_limiteSuperior)
        {
            x    = limites.right();
            yPos = YPosicao::LimSup;
        }
        else
        {
            x    = limites.right() + (larguraAlcanceIncerteza / 4);
            yPos = YPosicao::DirLS;
        }
    }

    // limite inferior
    // -8 pixels para centralizar fonte
    pintor.drawLine(limites.topLeft(), limites.bottomLeft());
    pintor.drawText(limites.left() - 8, 310,
                    QString::number(m_limiteInferior, 'f', 2) + " " + m_unidade);

    // limite superior
    // -8 pixels para centralizar fonte
    pintor.drawLine(limites.topRight(), limites.bottomRight());
    pintor.drawText(limites.right() - 8, 310,
                    QString::number(m_limiteSuperior, 'f', 2) + " " + m_unidade);

    pintor.save();

    // legendas
    desenharLegendas(pintor);

    pintor.restore();

    pintor.save();

    // zona de especificação
    desenharZonaEspecificacao(limites.left(), limites.right(), pintor, caneta);

    pintor.restore();

    // zona de conformidade
    desenharZonaConformidade(pintor, caneta, m_U * escala, yPos);

    // incertezas
    caneta.setBrush(Qt::black);
    caneta.setWidth(1);
    pintor.setPen(caneta);
    desenharIcertezas(limites, pintor, m_U * escala, yPos);

    // y
    // linha
    caneta.setBrush(Qt::darkBlue);
    caneta.setWidth(3);
    pintor.setPen(caneta);
    pintor.drawLine(x, 225, x, 255);
    // texto
    caneta.setBrush(Qt::darkBlue);
    caneta.setWidth(1);
    pintor.setPen(caneta);
    QString textoY = "y = " + QString::number(m_y, 'f', 2) + " " + m_unidade;
    pintor.drawText(x - (textoY.size() * 4), 175, textoY);
    // seta
    pintor.drawLine(x, 185, x, 215);
    desenharTriangulo(x - 3, 214, 6, 6, DirecaoTriangulo::Embaixo, pintor, Qt::darkBlue);

    // valor nominal
    x = 320; // meio da linha
    // linha
    caneta.setBrush(Qt::darkRed);
    caneta.setWidth(2);
    pintor.setPen(caneta);
    pintor.drawLine(x, 230, x, 250);
    // texto
    caneta.setBrush(Qt::darkRed);
    caneta.setWidth(1);
    pintor.setPen(caneta);
    QString textoVN = "VN = " + QString::number(m_valorNominal, 'f', 2) + " " + m_unidade;
    pintor.drawText(x - (textoVN.size() * 4), 330, textoVN);
    // seta
    pintor.drawLine(x, 255, x, 310);
    desenharTriangulo(x - 3, 254, 6, 6, DirecaoTriangulo::EmCima, pintor, Qt::darkRed);
}

void AreaRenderizacao::desenharTriangulo(double x,
                                         double y,
                                         double largura,
                                         double altura,
                                         DirecaoTriangulo direcao,
                                         QPainter& pintorTri,
                                         Qt::GlobalColor cor)
{
    QRectF retangulo = QRectF(x, y, largura, altura);

    QPainterPath caminho;
    if (direcao == DirecaoTriangulo::EmCima)
    {
        caminho.moveTo(retangulo.left() + (retangulo.width() / 2), retangulo.top());
        caminho.lineTo(retangulo.bottomLeft());
        caminho.lineTo(retangulo.bottomRight());
        caminho.lineTo(retangulo.left() + (retangulo.width() / 2), retangulo.top());
    }

    if (direcao == DirecaoTriangulo::Embaixo)
    {
        caminho.moveTo(retangulo.left() + (retangulo.width() / 2), retangulo.bottom());
        caminho.lineTo(retangulo.topLeft());
        caminho.lineTo(retangulo.topRight());
        caminho.lineTo(retangulo.left() + (retangulo.width() / 2), retangulo.bottom());
    }

    if (direcao == DirecaoTriangulo::Direita)
    {
        caminho.moveTo(retangulo.right(), retangulo.top() + (retangulo.height() / 2));
        caminho.lineTo(retangulo.topLeft());
        caminho.lineTo(retangulo.bottomLeft());
        caminho.moveTo(retangulo.right(), retangulo.top() + (retangulo.height() / 2));
    }

    if (direcao == DirecaoTriangulo::Esquerda)
    {
        caminho.moveTo(retangulo.left(), retangulo.top() + (retangulo.height() / 2));
        caminho.lineTo(retangulo.topRight());
        caminho.lineTo(retangulo.bottomRight());
        caminho.moveTo(retangulo.left(), retangulo.top() + (retangulo.height() / 2));
    }

    pintorTri.fillPath(caminho, QBrush(cor));
}

void AreaRenderizacao::desenharZonaEspecificacao(double limiteInferior,
                                                 double limiteSuperior,
                                                 QPainter& pintorZE,
                                                 QPen& canetaZE)
{
    canetaZE.setBrush(Qt::black);
    canetaZE.setWidth(1);
    pintorZE.setPen(canetaZE);

    pintorZE.drawLine(limiteInferior, 265, limiteSuperior, 265);

    pintorZE.drawLine(limiteInferior, 255, limiteInferior, 275);
    desenharTriangulo(limiteInferior, 262, 6, 6, DirecaoTriangulo::Esquerda, pintorZE, Qt::black);

    pintorZE.drawLine(limiteSuperior, 255, limiteSuperior, 275);
    desenharTriangulo(limiteSuperior - 6, 262, 6, 6, DirecaoTriangulo::Direita, pintorZE,
                      Qt::black);

    desenharTriangulo(limiteInferior - 3, 278, 6, 6, DirecaoTriangulo::EmCima, pintorZE, Qt::black);
    pintorZE.drawLine(limiteInferior, 280, limiteInferior, 295);

    desenharTriangulo(limiteSuperior - 3, 278, 6, 6, DirecaoTriangulo::EmCima, pintorZE, Qt::black);
    pintorZE.drawLine(limiteSuperior, 280, limiteSuperior, 295);

    QFont fonte;
    fonte.setPointSize(12);

    canetaZE.setBrush(Qt::darkCyan);

    pintorZE.setPen(canetaZE);
    pintorZE.setFont(fonte);
    pintorZE.drawText(285, 285, "ZE");
}

void AreaRenderizacao::desenharZonaConformidade(QPainter& pintorZC,
                                                QPen& canetaZC,
                                                double incerteza,
                                                YPosicao yPos)
{
    canetaZC.setBrush(Qt::black);
    canetaZC.setWidth(1);
    pintorZC.setPen(canetaZC);

    pintorZC.setBrush(Qt::BDiagPattern);

    if (m_conformidade == Conformidade::Conforme)
    {
        QRectF retangulo(120 + incerteza, 200, 400 - 2 * incerteza, 40);
        pintorZC.drawRect(retangulo);

        QFont fonte;
        fonte.setPointSize(12);

        canetaZC.setBrush(Qt::darkGreen);

        pintorZC.setPen(canetaZC);
        pintorZC.setFont(fonte);
        pintorZC.drawText(retangulo.center().x() + 6, retangulo.center().y() + 6, "ZC");
    }
    else if (m_conformidade == Conformidade::NaoConforme)
    {
        if (m_y < m_limiteInferior)
        {
            QRectF retangulo(60, 200, 120, 40);
            pintorZC.drawRect(retangulo);

            QFont fonte;
            fonte.setPointSize(12);

            canetaZC.setBrush(Qt::darkGreen);

            pintorZC.setPen(canetaZC);
            pintorZC.setFont(fonte);
            pintorZC.drawText(retangulo.left() + 15, retangulo.center().y() + 6, "ZNC");
        }
        else
        {
            QRectF retangulo(460, 200, 120, 40);
            pintorZC.drawRect(retangulo);

            QFont fonte;
            fonte.setPointSize(12);

            canetaZC.setBrush(Qt::darkGreen);

            pintorZC.setPen(canetaZC);
            pintorZC.setFont(fonte);
            pintorZC.drawText(retangulo.right() - 45, retangulo.center().y() + 6, "ZNC");
        }
    }
    else
    {
        if (yPos == YPosicao::EsqLI || yPos == YPosicao::DirLI || yPos == YPosicao::LimInf)
        {
            QRectF retangulo(120, 200, 100, 40);
            pintorZC.drawRect(retangulo);

            QFont fonte;
            fonte.setPointSize(12);

            canetaZC.setBrush(Qt::darkGreen);

            pintorZC.setPen(canetaZC);
            pintorZC.setFont(fonte);

            if (yPos == YPosicao::EsqLI)
            {
                pintorZC.drawText(retangulo.left() + 65, retangulo.center().y() + 6, "AI");
            }
            else
            {
                pintorZC.drawText(retangulo.left() + 15, retangulo.center().y() + 6, "AI");
            }
        }
        else if (yPos == YPosicao::EsqLS || yPos == YPosicao::DirLS || yPos == YPosicao::LimSup)
        {
            QRectF retangulo(420, 200, 100, 40);
            pintorZC.drawRect(retangulo);

            QFont fonte;
            fonte.setPointSize(12);

            canetaZC.setBrush(Qt::darkGreen);

            pintorZC.setPen(canetaZC);
            pintorZC.setFont(fonte);

            if (yPos == YPosicao::EsqLS)
            {
                pintorZC.drawText(retangulo.left() + 65, retangulo.center().y() + 6, "AI");
            }
            else
            {
                pintorZC.drawText(retangulo.left() + 15, retangulo.center().y() + 6, "AI");
            }
        }
    }
}

void AreaRenderizacao::desenharIcertezas(QRectF limites,
                                         QPainter& pintorInc,
                                         double incertezaLagura,
                                         YPosicao yPos)
{
    // lagura do texto da incerteza-padrão expandida
    QString inceretezaTexto("U = " + QString::number(m_U, 'f', 2) + " " + m_unidade);
    QFontMetrics fm = pintorInc.fontMetrics();
    int laguraTexto = fm.horizontalAdvance(inceretezaTexto);

    int y;
    if (laguraTexto + 6 < incertezaLagura)
    {
        y = 210;
    }
    else
    {
        y = 195;
    }

    if (m_conformidade == Conformidade::Conforme)
    {
        // limite inferior
        pintorInc.drawLine(limites.left(), 205, limites.left(), 225);
        pintorInc.drawLine(limites.left(), 215, limites.left() + incertezaLagura, 215);
        pintorInc.drawText(limites.left() + 6, y,
                           "U = " + QString::number(m_U, 'f', 2) + " " + m_unidade);
        desenharTriangulo(limites.left(), 212, 6, 6, DirecaoTriangulo::Esquerda, pintorInc,
                          Qt::black);
        desenharTriangulo(limites.left() - 6 + incertezaLagura, 212, 6, 6,
                          DirecaoTriangulo::Direita, pintorInc, Qt::black);

        // limite superior
        pintorInc.drawLine(limites.right(), 205, limites.right(), 225);
        pintorInc.drawLine(limites.right() - incertezaLagura, 215, limites.right(), 215);
        pintorInc.drawText(limites.right() + 6 - incertezaLagura, y,
                           "U = " + QString::number(m_U, 'f', 2) + " " + m_unidade);
        desenharTriangulo(limites.right() - incertezaLagura, 212, 6, 6, DirecaoTriangulo::Esquerda,
                          pintorInc, Qt::black);
        desenharTriangulo(limites.right() - 6, 212, 6, 6, DirecaoTriangulo::Direita, pintorInc,
                          Qt::black);
    }
    else if (m_conformidade == Conformidade::NaoConforme)
    {
        if (m_y < m_limiteInferior)
        {
            // limite inferior
            pintorInc.drawLine(limites.left(), 205, limites.left(), 225);
            pintorInc.drawLine(180, 215, limites.left(), 215);
            pintorInc.drawText(186, y, "U = " + QString::number(m_U, 'f', 2) + " " + m_unidade);
            desenharTriangulo(180, 212, 6, 6, DirecaoTriangulo::Esquerda, pintorInc, Qt::black);
            desenharTriangulo(limites.left() - 6, 212, 6, 6, DirecaoTriangulo::Direita, pintorInc,
                              Qt::black);
        }
        else
        {
            // limite superior
            pintorInc.drawLine(limites.right(), 205, limites.right(), 225);
            pintorInc.drawLine(460, 215, limites.right(), 215);
            pintorInc.drawText(limites.right() + 6, y,
                               "U = " + QString::number(m_U, 'f', 2) + " " + m_unidade);
            desenharTriangulo(limites.right(), 212, 6, 6, DirecaoTriangulo::Esquerda, pintorInc,
                              Qt::black);
            desenharTriangulo(454, 212, 6, 6, DirecaoTriangulo::Direita, pintorInc, Qt::black);
        }
    }
    else
    {
        if (yPos == YPosicao::EsqLI || yPos == YPosicao::DirLI || yPos == YPosicao::LimInf)
        {
            // limite inferior
            pintorInc.drawLine(limites.left(), limites.top() - 20, limites.left(),
                               limites.top() - 40);
            pintorInc.drawLine(limites.left() - 50, limites.top() - 20, limites.left() - 50,
                               limites.top() - 40);
            pintorInc.drawLine(limites.left() + 50, limites.top() - 20, limites.left() + 50,
                               limites.top() - 40);
            pintorInc.drawLine(limites.left() - 50, limites.top() - 30, limites.left() + 50,
                               limites.top() - 30);
            pintorInc.drawText(limites.left() + 12, y, "U");
            pintorInc.drawText(limites.left() - 38, y, "U");
            desenharTriangulo(limites.left() - 50, limites.top() - 33, 6, 6,
                              DirecaoTriangulo::Esquerda, pintorInc, Qt::black);
            desenharTriangulo(limites.left() - 6, limites.top() - 33, 6, 6,
                              DirecaoTriangulo::Direita, pintorInc, Qt::black);
            desenharTriangulo(limites.left(), limites.top() - 33, 6, 6, DirecaoTriangulo::Esquerda,
                              pintorInc, Qt::black);
            desenharTriangulo(limites.left() + 44, limites.top() - 33, 6, 6,
                              DirecaoTriangulo::Direita, pintorInc, Qt::black);
        }
        else if (yPos == YPosicao::EsqLS || yPos == YPosicao::DirLS || yPos == YPosicao::LimSup)
        {
            // limite superior
            pintorInc.drawLine(limites.right(), limites.top() - 20, limites.right(),
                               limites.top() - 40);
            pintorInc.drawLine(limites.right() - 50, limites.top() - 20, limites.right() - 50,
                               limites.top() - 40);
            pintorInc.drawLine(limites.right() + 50, limites.top() - 20, limites.right() + 50,
                               limites.top() - 40);
            pintorInc.drawLine(limites.right() - 50, limites.top() - 30, limites.right() + 50,
                               limites.top() - 30);
            pintorInc.drawText(limites.right() + 12, y, "U");
            pintorInc.drawText(limites.right() - 38, y, "U");
            desenharTriangulo(limites.right() - 50, limites.top() - 33, 6, 6,
                              DirecaoTriangulo::Esquerda, pintorInc, Qt::black);
            desenharTriangulo(limites.right() - 6, limites.top() - 33, 6, 6,
                              DirecaoTriangulo::Direita, pintorInc, Qt::black);
            desenharTriangulo(limites.right(), limites.top() - 33, 6, 6, DirecaoTriangulo::Esquerda,
                              pintorInc, Qt::black);
            desenharTriangulo(limites.right() + 44, limites.top() - 33, 6, 6,
                              DirecaoTriangulo::Direita, pintorInc, Qt::black);
        }

        pintorInc.drawText(20, 350, "U = " + QString::number(m_U, 'f', 2) + " " + m_unidade);
    }
}

void AreaRenderizacao::desenharLegendas(QPainter& pintorLeg)
{
    QString conformidadeString;
    QString conformidadeZona;
    Qt::GlobalColor cor;
    int larguraLegenda = 0;

    if (m_conformidade == Conformidade::Conforme)
    {
        conformidadeString = "Conforme";
        conformidadeZona   = "ZC: Zona de Conformidade\n";
        cor                = Qt::green;
        larguraLegenda     = 150;
    }
    else if (m_conformidade == Conformidade::NaoDeterminada)
    {
        conformidadeString = "Conformidade não comprovada";
        conformidadeZona   = "AI: Alcance de Incerteza\n";
        cor                = Qt::darkGray;
        larguraLegenda     = 140;
    }
    else
    {
        conformidadeString = "Não Conforme";
        conformidadeZona   = "ZNC: Zona de Não Conformidade\n";
        cor                = Qt::red;
        larguraLegenda     = 180;
    }

    QFont fonte;
    QPen caneta;

    // Legendas

    fonte.setPointSize(8);

    pintorLeg.setFont(fonte);
    pintorLeg.drawText(QRectF(3, 430, 170, 50), Qt::AlignLeft | Qt::AlignTop,
                       "Legenda:\n" + conformidadeZona + "ZE: Zona de Especificação");
    pintorLeg.setPen(caneta);
    pintorLeg.drawRect(0, 430, larguraLegenda, 50);

    // Conformidade

    fonte.setPointSize(18);

    caneta.setBrush(cor);
    pintorLeg.setPen(caneta);
    pintorLeg.setFont(fonte);

    QFontMetrics fm = pintorLeg.fontMetrics();
    int laguraTexto = fm.horizontalAdvance(conformidadeString);

    int posicaoConformidade = (640 - laguraTexto) / 2;

    pintorLeg.drawText(posicaoConformidade, 400, conformidadeString);
}
