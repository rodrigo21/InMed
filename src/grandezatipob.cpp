#include "grandezatipob.hpp"

#include "incertezatipob.hpp"

#include <ginac/ginac.h>

GrandezaTipoB::GrandezaTipoB(const GiNaC::symbol& simboloGrandeza,
                             const std::string& definicao,
                             const std::string& unidade,
                             double valorEstimado,
                             unsigned int grausDeLiberdade,
                             Distribuicao distribuicao,
                             double dado1,
                             double dado2) :
    Grandeza(simboloGrandeza, definicao, unidade)
{
    IncertezaTipoB incerteza(grausDeLiberdade, distribuicao, dado1, dado2);

    m_distribuicao = distribuicao;

    m_valorEstimado = valorEstimado;

    m_grausDeLiberdade = grausDeLiberdade;

    m_incertezaPadrao = incerteza.incertezaPadraoB();

    m_possuiCorrelacao = false;
}

double GrandezaTipoB::incertezaPadrao()
{
    return m_incertezaPadrao;
}

bool GrandezaTipoB::possuiCorrelacao()
{
    return m_possuiCorrelacao;
}
