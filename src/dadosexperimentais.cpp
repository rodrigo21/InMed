#include "dadosexperimentais.hpp"
#include "ui_dadosexperimentais.h"

#include <QApplication>
#include <QDesktopWidget>
#include <QDialogButtonBox>
#include <QMessageBox>
#include <QPair>
#include <QPushButton>
#include <QRect>
#include <QStringList>
#include <QTableWidget>
#include <QTableWidgetItem>

DadosExperimentais::DadosExperimentais(const QString& nomeGrandeza,
                                       QVector<double>& dadosAnteriores,
                                       QWidget* parent) :
    QDialog(parent), m_dados(dadosAnteriores), m_ui(new Ui::DadosExperimentais)
{
    m_ui->setupUi(this);

    m_ui->groupBoxDados->setTitle(nomeGrandeza);

    if (!m_dados.isEmpty())
    {
        m_ui->tableWidgetDados->setColumnCount(1);
        if (m_dados.size() == 1)
        {
            m_ui->tableWidgetDados->setRowCount(1);
        }

        recriarTabela();
    }
    else
    {
        m_ui->tableWidgetDados->setColumnCount(1);
        m_ui->tableWidgetDados->setRowCount(2);

        resize(0, (m_ui->tableWidgetDados->rowHeight(0) * 2 + (height() / 2)) + 10);
    }

    m_ui->tableWidgetDados->setHorizontalHeaderLabels({"Valor Observado"});

    m_ui->tableWidgetDados->resizeColumnsToContents();
    m_ui->tableWidgetDados->resizeRowsToContents();

    connect(m_ui->pushButtonAdicionar, &QPushButton::clicked, this,
            &DadosExperimentais::adicionarDado);
    connect(m_ui->pushButtonRemover, &QPushButton::clicked, this, &DadosExperimentais::removerDado);

    connect(m_ui->tableWidgetDados, &QTableWidget::currentCellChanged, this,
            &DadosExperimentais::ajustarTabela);

    connect(m_ui->buttonBox, &QDialogButtonBox::accepted, this, &DadosExperimentais::coletarDados);
}

QVector<double> DadosExperimentais::dados() const
{
    return m_dados;
}

DadosExperimentais::~DadosExperimentais()
{
    delete m_ui;
}

void DadosExperimentais::adicionarDado()
{
    auto linha = m_ui->tableWidgetDados->rowCount();

    m_ui->tableWidgetDados->insertRow(linha);

    m_ui->tableWidgetDados->setItem(linha, 0, new QTableWidgetItem);

    m_ui->tableWidgetDados->resizeColumnsToContents();
    m_ui->tableWidgetDados->resizeRowsToContents();

    const QRect geometriaDisponivel = QApplication::desktop()->availableGeometry(m_ui->tableWidgetDados);

    auto alturaMaxima = geometriaDisponivel.height() / 2;
    auto alturaAtual  = m_ui->tableWidgetDados->rowHeight(linha) + height();

    if (alturaAtual < alturaMaxima)
    {
        resize(m_ui->tableWidgetDados->columnWidth(linha), alturaAtual);
    }
}

void DadosExperimentais::removerDado()
{
    auto linhaRemover = m_ui->tableWidgetDados->rowCount() - 1;

    if (linhaRemover == 0)
    {
        auto itemRemover = m_ui->tableWidgetDados->item(linhaRemover, 0);
        if (itemRemover != nullptr)
        {
            itemRemover->setText("");
        }
    }

    if (linhaRemover > 0)
    {
        m_ui->tableWidgetDados->removeRow(linhaRemover);
    }
}

void DadosExperimentais::ajustarTabela(int /*atualLinha*/,
                                       int /*atualColuna*/,
                                       int /*anteriorLinha*/,
                                       int /*anteriorColuna*/)
{
    m_ui->tableWidgetDados->resizeColumnsToContents();
    m_ui->tableWidgetDados->resizeRowsToContents();
}

void DadosExperimentais::coletarDados()
{
    m_dados.clear();

    QVector<QPair<int, QString>> valoresInvalidos;

    for (int i = 0; i < m_ui->tableWidgetDados->rowCount(); ++i)
    {
        auto item = m_ui->tableWidgetDados->item(i, 0);
        if (item != nullptr)
        {
            auto texto = item->text();
            if (!texto.isEmpty())
            {
                bool ok;
                double valorNumerico = texto.toDouble(&ok);
                if (ok)
                {
                    m_dados.push_back(valorNumerico);
                }
                else
                {
                    valoresInvalidos.push_back({i + 1, texto});
                }
            }
        }
    }

    QMessageBox mensagemAviso;
    mensagemAviso.setIcon(QMessageBox::Warning);
    mensagemAviso.setWindowTitle(tr("Aviso"));

    if (!valoresInvalidos.isEmpty())
    {
        if (valoresInvalidos.size() == 1)
        {
            mensagemAviso.setText(
                tr("<p>Valor Inválido.<br><br>"
                   "A célula <b><font color=red>%1</font></b> contém um valor inválido "
                   "(<b><font color=red>%2</font></b>). Este valor será ignorado.<br><br>"
                   "Por favor consulte a ajuda para mais informações (F1).</p>")
                    .arg(valoresInvalidos[0].first)
                    .arg(valoresInvalidos[0].second));
        }
        else
        {
            QStringList numeroCelulasInvalidas;
            QStringList valorCelulasInvalidas;

            QPair<int, QString> celula;
            foreach (celula, valoresInvalidos)
            {
                numeroCelulasInvalidas.push_back(QString::number(celula.first));

                valorCelulasInvalidas.push_back(celula.second);
            }

            mensagemAviso.setText(
                tr("<p>Valores Inválidos.<br><br>"
                   "A células <b><font color=red>%1</font></b> contêm valores inválidos "
                   "(<b><font color=red>%2</font></b>). Estes valores serão ignorados.<br><br>"
                   "Por favor consulte a ajuda para mais informações (F1).</p>")
                    .arg(numeroCelulasInvalidas.join(", "))
                    .arg(valorCelulasInvalidas.join(", ")));
        }

        mensagemAviso.exec();
    }

    if (m_dados.size() < 2 && valoresInvalidos.isEmpty())
    {
        mensagemAviso.setText(tr("Pelo menos 2 valores válidos são necessários"));
        mensagemAviso.exec();
    }
}

void DadosExperimentais::recriarTabela()
{
    for (int linha = 0; linha < m_dados.size(); ++linha)
    {
        m_ui->tableWidgetDados->insertRow(linha);

        auto item = new QTableWidgetItem;
        item->setText(QString::number(m_dados.at(linha)));

        m_ui->tableWidgetDados->setItem(linha, 0, item);
    }

    int numeroColunas = m_dados.size();
    if (m_dados.size() == 1)
    {
        numeroColunas = 2;
    }

    const QRect geometriaDisponivel = QApplication::desktop()->availableGeometry(m_ui->tableWidgetDados);

    auto alturaMaxima = geometriaDisponivel.height() / 2;
    auto alturaAtual  = m_ui->tableWidgetDados->rowHeight(0) * numeroColunas + (height() / 2);

    if (alturaAtual < alturaMaxima)
    {
        resize(m_ui->tableWidgetDados->columnWidth(0), alturaAtual + 10);
    }
    else
    {
        resize(m_ui->tableWidgetDados->columnWidth(0), alturaMaxima + 10);
    }
}
