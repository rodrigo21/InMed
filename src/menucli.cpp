#include "menucli.hpp"

#include "contribuicaoincerteza.hpp"
#include "funcoesextras.hpp"
#include "grandeza.hpp"
#include "grandezaresultado.hpp"
#include "grandezatipoa.hpp"
#include "grandezatipob.hpp"
#include "incertezacombinada.hpp"
#include "modelomatematico.hpp"

#include <ginac/ginac.h>

#include <iomanip>
#include <iostream>
#include <limits>
#include <memory>
#include <string>
#include <utility>
#include <vector>

GrandezaResultado entradaGrandezaResultado()
{
    std::string simbolo;
    std::cout << "\nEntre com a grandeza do resultado: ";
    std::getline(std::cin, simbolo);

    std::string definicao("");
    std::string unidade("");

    std::cout << "\nDefinição da grandeza do resultado \"" << simbolo << "\": ";
    std::getline(std::cin, definicao);

    std::cout << "Unidade da grandeza do resultado\"" << simbolo << "\": ";
    std::getline(std::cin, unidade);

    return std::move(GrandezaResultado(GiNaC::symbol(simbolo), definicao, unidade));
}

ModeloMatematico lerModelo()
{
    std::string entrada;
    std::cout << "\nEntre com o modelo matemático: ";
    std::getline(std::cin, entrada);

    return ModeloMatematico(entrada);
}

std::vector<std::shared_ptr<Grandeza>> entradaGrandezas(const ModeloMatematico& modelo)
{
    std::vector<std::shared_ptr<Grandeza>> grandezas;

    for (const auto& simbolo : modelo.simbolos())
    {
        std::string definicao("");
        std::cout << "\nDefinição da grandeza \"" << simbolo.get_name() << "\": ";
        std::getline(std::cin, definicao);

        std::string unidade("");
        std::cout << "Unidade da grandeza \"" << simbolo.get_name() << "\": ";
        std::getline(std::cin, unidade);
        std::cout << '\n';

        do
        {
            std::string tipoAvaiacao("");
            std::cout << "Entre com o tipo de avaliação (A ou B) para a grandeza \""
                      << simbolo.get_name() << "\": ";
            std::getline(std::cin, tipoAvaiacao);

            // Tipo A
            if (tipoAvaiacao == "a" || tipoAvaiacao == "A")
            {
                std::vector<double> dadosExperimentais;
                double dado = 0;
                std::string temCorrelacao("");
                bool possuiCorrelacao = false;
                std::string possuiDesvioPadraoAgrupado("");

                std::cout << "\nA grandeza possui correlação (S/N): ";
                std::cin >> temCorrelacao;
                std::cin.ignore();
                if (temCorrelacao == "s" || temCorrelacao == "S")
                {
                    possuiCorrelacao = true;
                }

                int i = 1;
                std::cout << "\nEntre com o dado " << i
                          << " (após o último dado digite Ctrl+D no Linux ou Ctrl+Z no Windows): ";
                while (std::cin >> dado)
                {
                    std::cin.ignore();

                    dadosExperimentais.emplace_back(dado);
                    std::cout << "Entre com o dado " << i + 1 << ": ";

                    i++;
                }

                std::cin.clear();

                std::cout << "\n\nOs dados possuem um desvio-padrão experimental agrupado (S/N): ";
                std::cin >> possuiDesvioPadraoAgrupado;
                std::cin.ignore();
                if (possuiDesvioPadraoAgrupado == "s" || possuiDesvioPadraoAgrupado == "S")
                {
                    double desvioPadraoAgrupado    = 0.0;
                    int numeroObservacoesAgrupadas = 0;

                    std::cout << "\nEntre com o desvio-padrão experimental agrupado (" << unidade
                              << "): ";
                    std::cin >> desvioPadraoAgrupado;
                    std::cin.ignore();

                    std::cout << "Entre com o número de observações repetidas e independentes: ";
                    std::cin >> numeroObservacoesAgrupadas;
                    std::cin.ignore();

                    grandezas.emplace_back(std::make_shared<GrandezaTipoA>(
                        simbolo, definicao, unidade, possuiCorrelacao,
                        dadosExperimentais,
                        desvioPadraoAgrupado, numeroObservacoesAgrupadas));
                }
                else
                {
                    grandezas.emplace_back(std::make_shared<GrandezaTipoA>(
                        simbolo, definicao, unidade, possuiCorrelacao,
                        dadosExperimentais));
                }

                break;
            }
            // Tipo B
            else if (tipoAvaiacao == "b" || tipoAvaiacao == "B")
            {
                int numeroDistribuicao        = 0;
                Distribuicao distribuicao     = Distribuicao::Invalida;
                double valorEstimado          = 0.0;
                unsigned int grausDeLiberdade = 0.0;
                double dado1                  = 0.0;
                double dado2                  = 0.0;

                std::cout << "\nEntre com o valor estimado para a grandeza \"" << simbolo.get_name()
                          << "\": ";
                std::cin >> valorEstimado;
                std::cin.ignore();

                std::cout << "Entre com o número de graus de liberdade para a grandeza \""
                          << simbolo.get_name() << "\" (Use 0 para um valor desconhecido): ";
                std::cin >> grausDeLiberdade;
                std::cin.ignore();

                std::cout
                    << "\nEntre com o tipo de distribuição de probabilidade para a grandeza \""
                    << simbolo.get_name() << "\": ";
                do
                {
                    std::cout << '\n'
                              << "(1) Em forma de U\n"
                              << "(2) Normal\n"
                              << "(3) Outras\n"
                              << "(4) Retangular\n"
                              << "(5) T\n"
                              << "(6) Trapezoidal\n"
                              << "(7) Triangular\n"
                              << " -> Distribuição: ";
                    std::cin >> numeroDistribuicao;

                    // Verifica se o valor entrado é um numero
                    if (std::cin.fail())
                    {
                        std::cin.clear();
                        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                    }
                    else
                    {
                        switch (numeroDistribuicao)
                        {
                            case 1:
                            {
                                distribuicao = Distribuicao::FormaU;

                                std::cout
                                    << "\nEntre com o valor da meia-largura dos limites (a) para a "
                                       "grandeza \""
                                    << simbolo.get_name() << "\": ";

                                // dado1 -> limite para o valor estimado (a)
                                std::cin >> dado1;
                                std::cin.ignore();

                                break;
                            }

                            case 2:
                            {
                                std::cout
                                    << "\nEntre com o valor da incerteza expandida (U) para a "
                                       "grandeza \""
                                    << simbolo.get_name() << "\": ";

                                // dado1 -> incerteza expandida (U)
                                std::cin >> dado1;
                                std::cin.ignore();

                                // dado2 -> fator de abrangência (k)
                                std::cout << "Entre com o valor do fator de abrangência (k) para a "
                                             "grandeza \""
                                          << simbolo.get_name() << "\": ";
                                std::cin >> dado2;
                                std::cin.ignore();

                                if (dado2 == 0.0)
                                {
                                    distribuicao = Distribuicao::Invalida;
                                }
                                else
                                {
                                    distribuicao = Distribuicao::Normal;
                                }

                                break;
                            }

                            case 3:
                            {
                                distribuicao = Distribuicao::Outras;

                                std::cout
                                    << "\nEntre com o valor da incerteza padrão para a grandeza \""
                                    << simbolo.get_name() << "\": ";

                                // dado1 -> valor da incerteza
                                std::cin >> dado1;
                                std::cin.ignore();

                                break;
                            }

                            case 4:
                            {
                                distribuicao = Distribuicao::Retangular;

                                std::cout
                                    << "\nEntre com o valor da meia-largura dos limites (a) para a "
                                       "grandeza \""
                                    << simbolo.get_name() << "\": ";

                                // dado1 -> meia-largura dos limites para o valor estimado (a)
                                std::cin >> dado1;
                                std::cin.ignore();

                                break;
                            }

                            case 5:
                            {
                                if (grausDeLiberdade == 0)
                                {
                                    distribuicao = Distribuicao::Invalida;

                                    std::cout << "\nO número de graus de liberdade não pose ser 0 "
                                                 "para essa distribuição\n";
                                }
                                else
                                {
                                    distribuicao = Distribuicao::T;

                                    // dado1 -> incerteza expandida (U)
                                    std::cout
                                        << "\nEntre com o valor da incerteza expandida (U) para a "
                                           "grandeza \""
                                        << simbolo.get_name() << "\": ";
                                    std::cin >> dado1;
                                    std::cin.ignore();

                                    // dado2 -> nível da confiança
                                    std::cout
                                        << "Entre com o valor do nível de confiança (%) para a "
                                           "grandeza \""
                                        << simbolo.get_name() << "\": ";
                                    std::cin >> dado2;
                                    std::cin.ignore();
                                }

                                break;
                            }

                            case 6:
                            {
                                distribuicao = Distribuicao::Trapezoidal;

                                std::cout
                                    << "\nEntre com o valor da 1ª meia-largura dos limites (a) "
                                       "para a "
                                       "grandeza \""
                                    << simbolo.get_name() << "\": ";

                                // dado1 -> 1ª meia-largura dos limites para o valor estimado (a)
                                std::cin >> dado1;
                                std::cin.ignore();

                                // dado2 -> 2ª meia-largura dos limites para o valor estimado (b)
                                std::cout << "Entre com o valor da 2ª meia-largura dos limites (b) "
                                             "para a "
                                             "grandeza \""
                                          << simbolo.get_name() << "\": ";
                                std::cin >> dado2;
                                std::cin.ignore();
                                break;
                            }

                            case 7:
                            {
                                distribuicao = Distribuicao::Triangular;

                                std::cout
                                    << "\nEntre com o valor da meia-largura dos limites (a) para a "
                                       "grandeza \""
                                    << simbolo.get_name() << "\": ";

                                // dado1 -> meia-largura dos limites para o valor estimado (a)
                                std::cin >> dado1;
                                std::cin.ignore();

                                break;
                            }

                            default:
                                distribuicao = Distribuicao::Invalida;
                                break;
                        }
                    }

                    if (distribuicao != Distribuicao::Invalida)
                    {
                        break;
                    }
                    else
                    {
                        std::cout << "\nOpção inválida. Entre com uma opção válida:\n";

                        continue;
                    }
                } while (true);

                grandezas.emplace_back(
                    std::make_shared<GrandezaTipoB>(simbolo, definicao, unidade, valorEstimado,
                                                    grausDeLiberdade, distribuicao, dado1, dado2));

                break;
            }

            std::cout << "\nOpção inválida. ";
        } while (true);
    }

    return grandezas;
}

void menucli()
{
    auto grandezaResultado = entradaGrandezaResultado();

    auto modelo    = lerModelo();
    auto grandezas = entradaGrandezas(modelo);

    GiNaC::exmap mapaValorEstimado;
    for (const auto& grandeza : grandezas)
    {
        mapaValorEstimado[grandeza->simboloGrandeza()] = grandeza->valorEstimado();
    }

    std::cout << "\nEntre com valor do nível da confiança (%) para a incerteza combinada do "
                 "resultado."
                 "\nValores válidos: 68.27, 90, 95, 95.45, 99, 99.73. (padrão: 95.45%): ";
    double nivelDaConfianca = 0.0;
    std::cin >> nivelDaConfianca;
    std::cin.ignore();

    std::string calcularK = "";
    bool calculeK         = false;
    std::cout << "\nCalcular fator de abrangência baseado nos graus de liberdade efetivos (S/N): ";
    std::cin >> calcularK;
    std::cin.ignore();
    if (calcularK == "s" || calcularK == "S")
    {
        calculeK = true;
    }

    std::cout << "\n\n\n-----------------------------------Balanço de "
                 "Incertezas-----------------------------------";

    std::vector<ContribuicaoIncerteza> contribuicoes;
    for (const auto& grandeza : grandezas)
    {
        std::cout << "\n\nValor estimado para " << grandeza->nomeGrandeza() << ": "
                  << grandeza->valorEstimado() << " " << grandeza->unidade();

        std::cout << "\nIncerteza Padrão para " << grandeza->nomeGrandeza() << ": "
                  << grandeza->incertezaPadrao() << " " << grandeza->unidade();

        ContribuicaoIncerteza contribuicao(grandeza, modelo, mapaValorEstimado);
        contribuicoes.emplace_back(contribuicao);

        std::cout << "\nDerivada parcial ∂" << grandezaResultado.simboloGrandeza() << "/∂"
                  << contribuicao.grandeza()->simboloGrandeza() << " = "
                  << contribuicao.coeficienteSensibilidade();

        std::cout << "\nValor do coeficiente de sensibilidade para "
                  << contribuicao.grandeza()->simboloGrandeza() << ": "
                  << contribuicao.valorCoeficienteSensibilidade();

        std::cout << "\nContribuição de incerteza para "
                  << contribuicao.grandeza()->simboloGrandeza() << ": "
                  << contribuicao.contribuicaoIncerteza() << " " << grandezaResultado.unidade();
    }

    IncertezaCombinada incertezaCombinada(contribuicoes);

    std::cout << "\n\n\n----------------------------Índice de contribuição de incerteza-"
                 "----------------------------\n\n";

    for (const auto& i : incertezaCombinada.contribuicaoes())
    {
        std::cout << std::setprecision(6) << i.grandeza()->nomeGrandeza() << ": "
                  << ((i.contribuicaoIncerteza() * i.contribuicaoIncerteza())
                      / (incertezaCombinada.incertezaCombinada()
                         * incertezaCombinada.incertezaCombinada()))
                         * 100
                  << "%\n";
    }

    std::cout << "\n\n----------------------------------------Resultados---------------------------"
                 "--------------\n";

    std::cout << std::setprecision(6) << std::fixed;
    std::cout << "\nIncerteza Combinada = " << incertezaCombinada.incertezaCombinada() << " "
              << grandezaResultado.unidade() << '\n';

    auto valorResultado = grandezaResultado.valorEstimado(modelo, mapaValorEstimado);

    std::cout << "\nValor estimado " << grandezaResultado.simboloGrandeza() << " = "
              << valorResultado << " " << grandezaResultado.unidade()
              << ", com uma incerteza-padrão combinada uc = "
              << incertezaCombinada.incertezaCombinada() << " " << grandezaResultado.unidade()
              << "\n";

    auto veff = grausLiberdadeEfetivo(contribuicoes, incertezaCombinada);

    double k;
    if (veff != 0 && calculeK)
    {
        k = kStudentsT(veff, nivelDaConfianca);
        std::cout << "\nGraus de liberdade efetivo = " << veff << '\n';
    }
    else
    {
        double fda = fatorDeAbrangencia(nivelDaConfianca);

        if (fda != 0)
        {
            k = fda;
        }
        else
        {
            std::cout << "\nValor do nível da confiança inválido, assumindo k = 2";

            k                = 2;
            nivelDaConfianca = 95.45;
        }
    }

    std::cout << "\nFator de abrangência para um nível da confiança de " << std::setprecision(0)
              << nivelDaConfianca << "%: k(" << nivelDaConfianca << ") = " << std::setprecision(2)
              << k << "\n";

    std::cout << std::setprecision(6) << "\nValor estimado (" << grandezaResultado.simboloGrandeza()
              << " = " << valorResultado << " ± " << (incertezaCombinada.incertezaCombinada() * k)
              << ") " << grandezaResultado.unidade();

    std::cout << "\n\n--------------------------------------------------------------------------"
                 "-------------"
                 "----\n";
}
