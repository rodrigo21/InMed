#include "grandezaresultado.hpp"

#include "modelomatematico.hpp"

#include <ginac/ginac.h>

#include <iostream>
#include <string>

GrandezaResultado::GrandezaResultado(const GiNaC::symbol& simboloGrandeza,
                                     const std::string& definicao,
                                     const std::string& unidade) :
    Grandeza(simboloGrandeza,
             definicao,
             unidade)
{
    m_grausDeLiberdade = 0;

    m_valorEstimado = 0;

    m_incertezaPadrao = 0;

    m_possuiCorrelacao = false;

    m_distribuicao = Distribuicao::Invalida;
}

double GrandezaResultado::valorEstimado(const ModeloMatematico& modelo,
                                        GiNaC::exmap mapaValorEstimado)
{
    try
    {
        m_valorEstimado = GiNaC::ex_to<GiNaC::numeric>(modelo.modelo().subs(mapaValorEstimado).evalf())
                              .to_double();

        return m_valorEstimado;
    }
    catch (...)
    {
        std::cout << "Erro: Valores inválidos para a grandeza do resultado." << std::endl;

        return 0;
    }
}

double GrandezaResultado::incertezaPadrao()
{
    return m_incertezaPadrao;
}

bool GrandezaResultado::possuiCorrelacao()
{
    return m_possuiCorrelacao;
}
