#include "incertezatipob.hpp"

#include "funcoesextras.hpp"
#include "grandeza.hpp"

#include <cmath>

IncertezaTipoB::IncertezaTipoB(unsigned int grausDeLiberdade,
                               Distribuicao distribuicao,
                               double dado1,
                               double dado2)
{
    double k = 0.0;

    switch (distribuicao)
    {
        /*
           << "(1) FormaU\n"
           << "(2) Normal\n"
           << "(3) Outras\n"
           << "(4) Retangular\n"
           << "(5) T\n"
           << "(6) Trapezoidal\n"
           << "(7) Triangular\n;"
         */
        case Distribuicao::FormaU:

            // dado1 -> meia-largura dos limites para o valor estimado (a)
            // dado2 -> 0 (não utilizado)
            m_incertezaPadrao = dado1 / std::sqrt(2);
            break;

        case Distribuicao::Normal:

            // dado1 -> incerteza expandida (U)
            // dado2 -> fator de abrangência (k)
            // garantir dado2 diferente de 0
            if (dado2 != 0)
            {
                m_incertezaPadrao = dado1 / dado2;
            }

            break;

        case Distribuicao::Outras:

            // outros tipos de incertezas
            // dado1 -> valor da incerteza
            // dado2 -> 0 (não utilizado)
            m_incertezaPadrao = dado1;
            break;

        case Distribuicao::Retangular:

            // dado1 -> meia-largura dos limites para o valor estimado (a)
            // dado2 -> 0 (não utilizado)
            m_incertezaPadrao = dado1 / std::sqrt(3);
            break;

        case Distribuicao::T:

            // dado1 -> incerteza expandida (U)
            // dado2 -> nível da confiança
            k                 = kStudentsT(grausDeLiberdade, dado2);
            m_incertezaPadrao = dado1 / k;
            break;

        case Distribuicao::Trapezoidal:

            // dado1 -> 1ª meia-largura dos limites para o valor estimado (a)
            // dado2 -> 2ª meia-largura dos limites para o valor estimado (b)
            m_incertezaPadrao = dado1 * (std::sqrt((1 + (dado2 * dado2)) / 6));
            break;

        case Distribuicao::Triangular:

            // dado1 -> meia-largura dos limites para o valor estimado (a)
            // dado2 -> 0 (não utilizado)
            m_incertezaPadrao = dado1 / std::sqrt(6);
            break;

        default:
            break;
    }
}

double IncertezaTipoB::incertezaPadraoB() const
{
    return m_incertezaPadrao;
}
