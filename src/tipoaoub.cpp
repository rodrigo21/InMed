#include "tipoaoub.hpp"

#include "dadosexperimentais.hpp"
#include "funcoesextras.hpp"

#include <QComboBox>
#include <QFormLayout>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPlainTextEdit>
#include <QPushButton>
#include <QSizePolicy>
#include <QSpacerItem>
#include <QVBoxLayout>

TipoAouB::TipoAouB(const QString& objetoNome, QWidget* parent) :
    QWidget(parent)
{
    // nome do widget
    setObjectName(objetoNome);

    /*********** widgets comuns a A e B ***********/

    auto widgetDefinicaoNomeUnidade = new QWidget(this);

    // espaço em branco para alinhar os widgets
    QString espaco(" ");

    // descrição

    auto labelDefinicao = new QLabel(QString("%1Definição:").arg(espaco.repeated(30)), widgetDefinicaoNomeUnidade);

    m_plaintexteditDefinicao = new QPlainTextEdit(widgetDefinicaoNomeUnidade);
    ajustarPlainTextEdit(m_plaintexteditDefinicao, 4, true, true, QSizePolicy::Expanding,
                         QSizePolicy::Minimum);

    // unidade

    auto labelUnidade = new QLabel("Unidade:", widgetDefinicaoNomeUnidade);

    m_lineeditUnidade = new QLineEdit(widgetDefinicaoNomeUnidade);
    ajustarLineEdit(m_lineeditUnidade, 0, 0, false, false, Qt::StrongFocus, QSizePolicy::Maximum,
                    QSizePolicy::Maximum);

    // tipo (A ou B)

    auto labelTipo = new QLabel("Tipo:", widgetDefinicaoNomeUnidade);

    m_comboboxTipo = new QComboBox(widgetDefinicaoNomeUnidade);
    m_comboboxTipo->addItems({"Tipo A", "Tipo B"});
    m_comboboxTipo->setCurrentIndex(0);

    // form layout com os widgets comuns a A e B
    auto formlayoutDefinicaoNomeUnidade = new QFormLayout(widgetDefinicaoNomeUnidade);
    formlayoutDefinicaoNomeUnidade->addRow(labelDefinicao, m_plaintexteditDefinicao);
    formlayoutDefinicaoNomeUnidade->addRow(labelUnidade, m_lineeditUnidade);
    formlayoutDefinicaoNomeUnidade->addRow(labelTipo, m_comboboxTipo);

    /*********** tipo A ***********/

    m_widgetDadosA = new QWidget(this);

    // dados experimentais

    auto labelDadosExperimentais = new QLabel(QString("%1Dados Experimentais:").arg(espaco.repeated(12)), m_widgetDadosA);

    auto pushbuttonDadosExperimentais = new QPushButton("Adicionar Dados", m_widgetDadosA);

    // avaliação da incerteza

    auto labelAvaliacaoIncerteza = new QLabel("Avaliação da Incerteza:", m_widgetDadosA);

    m_comboboxAvaliacaoIncerteza = new QComboBox(m_widgetDadosA);
    m_comboboxAvaliacaoIncerteza->addItems({"Experimental", "Desvio-padrão Experimental Agrupado"});
    m_comboboxAvaliacaoIncerteza->setCurrentIndex(0);

    // desvio padrão experimental agrupado, se 'Desvio Padrão Agrupado' for selecionado

    m_labelDPEAgrupado = new QLabel(QString("%1Desvio-padrão\nExperimental Agrupado:").arg(espaco.repeated(15)),
                                    m_widgetDadosA);

    m_lineeditDPEAgrupado = new QLineEdit(m_widgetDadosA);

    m_labelDPEAgrupadoUnidade = new QLabel(m_widgetDadosA);

    // número de observações repetidas e independentes, se "Desvio Padrão Agrupado" for selecionado

    m_labelNumObsRI = new QLabel(
        QString("%1Número de Observações\nRepetidas e Independentes:").arg(espaco.repeated(5)),
        m_widgetDadosA);

    m_lineeditNumObsRI = new QLineEdit(m_widgetDadosA);

    /***** grid layout com os widgets para o tipo A *****/
    auto gridlayoutDadosA = new QGridLayout(m_widgetDadosA);

    gridlayoutDadosA->addWidget(labelDadosExperimentais, 0, 0, Qt::AlignRight | Qt::AlignVCenter);
    gridlayoutDadosA->addWidget(pushbuttonDadosExperimentais, 0, 1,
                                Qt::AlignLeft | Qt::AlignVCenter);

    gridlayoutDadosA->addWidget(labelAvaliacaoIncerteza, 1, 0, Qt::AlignRight | Qt::AlignVCenter);
    gridlayoutDadosA->addWidget(m_comboboxAvaliacaoIncerteza, 1, 1, 1, 2,
                                Qt::AlignLeft | Qt::AlignVCenter);

    gridlayoutDadosA->addWidget(m_labelDPEAgrupado, 2, 0, Qt::AlignRight | Qt::AlignVCenter);
    gridlayoutDadosA->addWidget(m_lineeditDPEAgrupado, 2, 1, Qt::AlignLeft | Qt::AlignVCenter);
    gridlayoutDadosA->addWidget(m_labelDPEAgrupadoUnidade, 2, 2, Qt::AlignLeft | Qt::AlignVCenter);

    gridlayoutDadosA->addWidget(m_labelNumObsRI, 3, 0, Qt::AlignRight | Qt::AlignVCenter);
    gridlayoutDadosA->addWidget(m_lineeditNumObsRI, 3, 1, Qt::AlignLeft | Qt::AlignVCenter);

    gridlayoutDadosA->addItem(
        new QSpacerItem(40, 20, QSizePolicy::MinimumExpanding, QSizePolicy::Maximum), 2, 3);

    /*********** tipo B ***********/

    m_widgetDadosB = new QWidget(this);

    // distribuição

    auto labelDistribuicao = new QLabel(QString("%1Distribuição:").arg(espaco.repeated(26)), m_widgetDadosB);

    m_comboboxDistribuicao = new QComboBox(m_widgetDadosB);
    m_comboboxDistribuicao->addItems({"Distribuição-t", "Em forma de U", "Normal", "Outras",
                                      "Retangular", "Trapezoidal", "Triangular"});
    m_comboboxDistribuicao->setCurrentIndex(4);

    // valor estimado

    auto labelValorEstimado = new QLabel("Valor Estimado:", m_widgetDadosB);

    m_lineeditValorEstimado = new QLineEdit(m_widgetDadosB);

    auto labelValorEstimadoUnidade = new QLabel(m_widgetDadosB);

    // TODO: graus de liberdade para todos os tipos
    // graus de liberdade

    m_labelGrausLiberdade = new QLabel("Graus de Liberdade:", m_widgetDadosB);

    m_lineeditGrausLiberdade = new QLineEdit(m_widgetDadosB);

    /***** se 'Distribuição-t' for selecionado *****/

    // incerteza expandida (U),

    m_labelIncertezaExpandidaT = new QLabel("Incerteza Expandida (U):", m_widgetDadosB);

    m_lineeditIncertezaExpandidaT = new QLineEdit(m_widgetDadosB);

    m_labelIncertezaExpandidaUnidadeT = new QLabel(m_widgetDadosB);

    // nível de confiança (%)

    m_labelNivelConfianca = new QLabel("Nível de Confiança:", m_widgetDadosB);

    m_lineeditNivelConfianca = new QLineEdit(m_widgetDadosB);

    m_labelNivelConfiancaPorcentagem = new QLabel("%", m_widgetDadosB);

    /***** se 'Em forma de U', 'Retangular', 'Triangular' for selecionado *****/

    // meia-largura dos limites

    m_labelURTriMeiaLarguraLimites = new QLabel("Meia-largura dos Limites:", m_widgetDadosB);

    m_lineeditURTriMeiaLarguraLimites = new QLineEdit(m_widgetDadosB);

    m_labelURTriMeiaLarguraLimitesUnidade = new QLabel(m_widgetDadosB);

    /***** se 'Normal' for selecionado *****/

    // incerteza expandida (U)

    m_labelIncertezaExpandidaNormal = new QLabel("Incerteza Expandida (U):", m_widgetDadosB);

    m_lineeditIncertezaExpandidaNormal = new QLineEdit(m_widgetDadosB);

    m_labelIncertezaExpandidaUnidadeNormal = new QLabel(m_widgetDadosB);

    // fator de abrangência (k)

    m_labelFatorAbrangencia = new QLabel("Fator de Abrangência:", m_widgetDadosB);

    m_lineeditFatorAbrangencia = new QLineEdit(m_widgetDadosB);

    /***** se 'Outras' for selecionado *****/

    // incerteza padrão

    m_labelIncertezaPadrao = new QLabel("Incerteza Padrão:", m_widgetDadosB);

    m_lineeditIncertezaPadrao = new QLineEdit(m_widgetDadosB);

    m_labelIncertezaPadraoUnidade = new QLabel(m_widgetDadosB);

    /***** se 'Trapezoidal' for selecionado *****/

    // 1ª meia-largura dos limites

    m_labelTraMeiaLarguraLimites1 = new QLabel("1ª Meia-largura dos Limites:", m_widgetDadosB);

    m_lineeditTraMeiaLarguraLimites1 = new QLineEdit(m_widgetDadosB);

    m_labelTraMeiaLarguraLimitesUnidade1 = new QLabel(m_widgetDadosB);

    // 2ª meia-largura dos limites

    m_labelTraMeiaLarguraLimites2 = new QLabel("2ª Meia-largura dos Limites:", m_widgetDadosB);

    m_lineEditTraMeiaLarguraLimites2 = new QLineEdit(m_widgetDadosB);

    m_labelTraMeiaLarguraLimitesUnidade2 = new QLabel(m_widgetDadosB);

    /***** grid layout com os widgets para o tipo B *****/

    auto gridlayoutDadosB = new QGridLayout(m_widgetDadosB);

    gridlayoutDadosB->addWidget(labelDistribuicao, 0, 0, Qt::AlignRight | Qt::AlignVCenter);
    gridlayoutDadosB->addWidget(m_comboboxDistribuicao, 0, 1, Qt::AlignLeft | Qt::AlignVCenter);

    gridlayoutDadosB->addWidget(labelValorEstimado, 1, 0, Qt::AlignRight | Qt::AlignVCenter);
    gridlayoutDadosB->addWidget(m_lineeditValorEstimado, 1, 1, Qt::AlignLeft | Qt::AlignVCenter);
    gridlayoutDadosB->addWidget(labelValorEstimadoUnidade, 1, 2, Qt::AlignLeft | Qt::AlignVCenter);

    gridlayoutDadosB->addWidget(m_labelGrausLiberdade, 2, 0, Qt::AlignRight | Qt::AlignVCenter);
    gridlayoutDadosB->addWidget(m_lineeditGrausLiberdade, 2, 1, Qt::AlignLeft | Qt::AlignVCenter);

    // distribuição-t
    // incerteza expandida (U)
    gridlayoutDadosB->addWidget(m_labelIncertezaExpandidaT, 3, 0,
                                Qt::AlignRight | Qt::AlignVCenter);
    gridlayoutDadosB->addWidget(m_lineeditIncertezaExpandidaT, 3, 1,
                                Qt::AlignLeft | Qt::AlignVCenter);
    gridlayoutDadosB->addWidget(m_labelIncertezaExpandidaUnidadeT, 3, 2,
                                Qt::AlignLeft | Qt::AlignVCenter);
    // nível de confiança (%)
    gridlayoutDadosB->addWidget(m_labelNivelConfianca, 4, 0, Qt::AlignRight | Qt::AlignVCenter);
    gridlayoutDadosB->addWidget(m_lineeditNivelConfianca, 4, 1, Qt::AlignLeft | Qt::AlignVCenter);
    gridlayoutDadosB->addWidget(m_labelNivelConfiancaPorcentagem, 4, 2,
                                Qt::AlignLeft | Qt::AlignVCenter);

    // comum para em forma de U, retangular, triangular
    // meia-largura dos limites
    gridlayoutDadosB->addWidget(m_labelURTriMeiaLarguraLimites, 3, 0,
                                Qt::AlignRight | Qt::AlignVCenter);
    gridlayoutDadosB->addWidget(m_lineeditURTriMeiaLarguraLimites, 3, 1,
                                Qt::AlignLeft | Qt::AlignVCenter);
    gridlayoutDadosB->addWidget(m_labelURTriMeiaLarguraLimitesUnidade, 3, 2,
                                Qt::AlignLeft | Qt::AlignVCenter);

    // normal
    // incerteza expandida (U)
    gridlayoutDadosB->addWidget(m_labelIncertezaExpandidaNormal, 3, 0,
                                Qt::AlignRight | Qt::AlignVCenter);
    gridlayoutDadosB->addWidget(m_lineeditIncertezaExpandidaNormal, 3, 1,
                                Qt::AlignLeft | Qt::AlignVCenter);
    gridlayoutDadosB->addWidget(m_labelIncertezaExpandidaUnidadeNormal, 3, 2,
                                Qt::AlignLeft | Qt::AlignVCenter);
    // fator de abrangência (k)
    gridlayoutDadosB->addWidget(m_labelFatorAbrangencia, 4, 0, Qt::AlignRight | Qt::AlignVCenter);
    gridlayoutDadosB->addWidget(m_lineeditFatorAbrangencia, 4, 1, Qt::AlignLeft | Qt::AlignVCenter);

    // outras
    // incerteza padrão
    gridlayoutDadosB->addWidget(m_labelIncertezaPadrao, 3, 0, Qt::AlignRight | Qt::AlignVCenter);
    gridlayoutDadosB->addWidget(m_lineeditIncertezaPadrao, 3, 1, Qt::AlignLeft | Qt::AlignVCenter);
    gridlayoutDadosB->addWidget(m_labelIncertezaPadraoUnidade, 3, 2,
                                Qt::AlignLeft | Qt::AlignVCenter);

    // trapezoidal
    // 1ª meia-largura dos limites
    gridlayoutDadosB->addWidget(m_labelTraMeiaLarguraLimites1, 3, 0,
                                Qt::AlignRight | Qt::AlignVCenter);
    gridlayoutDadosB->addWidget(m_lineeditTraMeiaLarguraLimites1, 3, 1,
                                Qt::AlignLeft | Qt::AlignVCenter);
    gridlayoutDadosB->addWidget(m_labelTraMeiaLarguraLimitesUnidade1, 3, 2,
                                Qt::AlignLeft | Qt::AlignVCenter);
    // 2ª meia-largura dos limites
    gridlayoutDadosB->addWidget(m_labelTraMeiaLarguraLimites2, 4, 0,
                                Qt::AlignRight | Qt::AlignVCenter);
    gridlayoutDadosB->addWidget(m_lineEditTraMeiaLarguraLimites2, 4, 1,
                                Qt::AlignLeft | Qt::AlignVCenter);
    gridlayoutDadosB->addWidget(m_labelTraMeiaLarguraLimitesUnidade2, 4, 2,
                                Qt::AlignLeft | Qt::AlignVCenter);

    gridlayoutDadosB->addItem(new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum),
                              0, 2);

    /*********** Correção ***********/

    auto widgetCorrecao = new QWidget(this);

    auto labelCorrecao        = new QLabel("Correção:", widgetCorrecao);
    auto lineeditCorrecao     = new QLineEdit(widgetCorrecao);
    auto labelCorrecaoUnidade = new QLabel(widgetCorrecao);

    auto gridlayoutCorrecao       = new QHBoxLayout(widgetCorrecao);
    auto horizontalSpacerDireita  = new QSpacerItem(240, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
    auto horizontalSpacerEsquerda = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

    gridlayoutCorrecao->addItem(horizontalSpacerEsquerda);
    gridlayoutCorrecao->addWidget(labelCorrecao);
    gridlayoutCorrecao->addWidget(lineeditCorrecao);
    gridlayoutCorrecao->addWidget(labelCorrecaoUnidade);
    gridlayoutCorrecao->addItem(horizontalSpacerDireita);

    /*********** layout principal com todos os widgets principais ***********/

    auto verticallayoutPrincipal = new QVBoxLayout(this);
    verticallayoutPrincipal->setContentsMargins(0, 0, 0, 0);

    verticallayoutPrincipal->addWidget(widgetDefinicaoNomeUnidade);

    verticallayoutPrincipal->addWidget(m_widgetDadosA);

    verticallayoutPrincipal->addWidget(m_widgetDadosB);

    verticallayoutPrincipal->addWidget(widgetCorrecao);

    verticallayoutPrincipal->addItem(
        new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding));

    setLayout(verticallayoutPrincipal);

    /*********** funções (slots) que determinam os item iniciais nos combo boxes ***********/

    // inicia como 'Tipo A'
    tipoAlterado(m_comboboxTipo->currentText());

    // inicia com 'Experimental'
    avaliacaoIncertezaAlterada(m_comboboxAvaliacaoIncerteza->currentText());

    distribuicaoAlterada(m_comboboxDistribuicao->currentText());

    coletarDados();

    /*********** connections ***********/

    // mostra tipo A ou tipo B
    connect(m_comboboxTipo, &QComboBox::currentTextChanged, this, &TipoAouB::tipoAlterado);

    // muda dependendo do da avaliação de incerteza para o tipo A
    connect(m_comboboxAvaliacaoIncerteza, &QComboBox::currentTextChanged, this,
            &TipoAouB::avaliacaoIncertezaAlterada);

    // muda de acordo com o tipo de distribuição para o tipo B
    connect(m_comboboxDistribuicao, &QComboBox::currentTextChanged, this,
            &TipoAouB::distribuicaoAlterada);

    // adiciona os dados experimentais do tipo A
    connect(pushbuttonDadosExperimentais, &QPushButton::clicked, this,
            &TipoAouB::adicionarDadosExperimentais);

    // altera o label da unidade quando um valor é digitado no line edit
    connect(m_lineeditUnidade, &QLineEdit::textChanged, this, [=] {
        m_labelDPEAgrupadoUnidade->setText(m_lineeditUnidade->text());

        labelValorEstimadoUnidade->setText(m_lineeditUnidade->text());

        m_labelIncertezaExpandidaUnidadeT->setText(m_lineeditUnidade->text());

        m_labelURTriMeiaLarguraLimitesUnidade->setText(m_lineeditUnidade->text());

        m_labelIncertezaExpandidaUnidadeNormal->setText(m_lineeditUnidade->text());

        m_labelIncertezaPadraoUnidade->setText(m_lineeditUnidade->text());

        m_labelTraMeiaLarguraLimitesUnidade1->setText(m_lineeditUnidade->text());
        m_labelTraMeiaLarguraLimitesUnidade2->setText(m_lineeditUnidade->text());
    });

    // comuns a A e B
    connect(m_plaintexteditDefinicao, &QPlainTextEdit::textChanged, this, &TipoAouB::coletarDados);
    connect(m_lineeditUnidade, &QLineEdit::editingFinished, this, &TipoAouB::coletarDados);
    connect(m_comboboxTipo, &QComboBox::currentTextChanged, this, &TipoAouB::coletarDados);

    // Tipo A
    connect(m_comboboxAvaliacaoIncerteza, &QComboBox::currentTextChanged, this,
            &TipoAouB::coletarDados);
    connect(m_lineeditDPEAgrupado, &QLineEdit::editingFinished, this, &TipoAouB::coletarDados);
    connect(m_lineeditNumObsRI, &QLineEdit::editingFinished, this, &TipoAouB::coletarDados);

    // Tipo B
    connect(m_comboboxDistribuicao, &QComboBox::currentTextChanged, this, &TipoAouB::coletarDados);
    connect(m_lineeditValorEstimado, &QLineEdit::editingFinished, this, &TipoAouB::coletarDados);
    connect(m_lineeditIncertezaExpandidaT, &QLineEdit::editingFinished, this,
            &TipoAouB::coletarDados);
    connect(m_lineeditGrausLiberdade, &QLineEdit::editingFinished, this, &TipoAouB::coletarDados);
    connect(m_lineeditNivelConfianca, &QLineEdit::editingFinished, this, &TipoAouB::coletarDados);
    connect(m_lineeditURTriMeiaLarguraLimites, &QLineEdit::editingFinished, this,
            &TipoAouB::coletarDados);
    connect(m_lineeditIncertezaExpandidaNormal, &QLineEdit::editingFinished, this,
            &TipoAouB::coletarDados);
    connect(m_lineeditFatorAbrangencia, &QLineEdit::editingFinished, this, &TipoAouB::coletarDados);
    connect(m_lineeditIncertezaPadrao, &QLineEdit::editingFinished, this, &TipoAouB::coletarDados);
    connect(m_lineeditTraMeiaLarguraLimites1, &QLineEdit::editingFinished, this,
            &TipoAouB::coletarDados);
    connect(m_lineEditTraMeiaLarguraLimites2, &QLineEdit::editingFinished, this,
            &TipoAouB::coletarDados);
}

// dados do tipo A ou B

// comuns a A e B

QString TipoAouB::nomeGrandezaTipoAouB() const
{
    return m_nomeGrandezaTipoAouB;
}

QString TipoAouB::definicaoGrandezaTipoAouB() const
{
    return m_definicaoGrandezaTipoAouB;
}

QString TipoAouB::unidadeGrandezaTipoAouB() const
{
    return m_unidadeGrandezaTipoAouB;
}

QString TipoAouB::tipoGrandezaTipoAouB() const
{
    return m_tipoGrandezaTipoAouB;
}

// tipo A

QVector<double> TipoAouB::dadosExperimentais() const
{
    return m_dadosExperimentais;
}

QString TipoAouB::avaliacaoIncerteza() const
{
    return m_avaliacaoIncerteza;
}

QString TipoAouB::DPEAgrupado() const
{
    return m_DPEAgrupado;
}

QString TipoAouB::numObsRI() const
{
    return m_numObsRI;
}

// tipo B

QString TipoAouB::distribuicao() const
{
    return m_distribuicao;
}

QString TipoAouB::valorEstimado() const
{
    return m_valorEstimado;
}

QString TipoAouB::incertezaExpandidaT() const
{
    return m_incertezaExpandidaT;
}

QString TipoAouB::grausLiberdade() const
{
    return m_grausLiberdade;
}

QString TipoAouB::nivelConfianca() const
{
    return m_nivelConfianca;
}

QString TipoAouB::URTriMeiaLarguraLimites() const
{
    return m_URTriMeiaLarguraLimites;
}

QString TipoAouB::incertezaExpandidaNormal() const
{
    return m_incertezaExpandidaNormal;
}

QString TipoAouB::fatorAbrangencia() const
{
    return m_fatorAbrangencia;
}

QString TipoAouB::incertezaPadrao() const
{
    return m_incertezaPadrao;
}

QString TipoAouB::traMeiaLarguraLimites1() const
{
    return m_traMeiaLarguraLimites1;
}

QString TipoAouB::traMeiaLarguraLimites2() const
{
    return m_traMeiaLarguraLimites2;
}

// slots

void TipoAouB::tipoAlterado(const QString& tipo)
{
    m_widgetDadosA->setVisible(false);
    m_widgetDadosB->setVisible(false);

    if (tipo == "Tipo A")
    {
        m_widgetDadosA->setVisible(true);
    }

    if (tipo == "Tipo B")
    {
        m_widgetDadosB->setVisible(true);
    }
}

void TipoAouB::avaliacaoIncertezaAlterada(const QString& avaliacao)
{
    m_labelDPEAgrupado->setVisible(false);
    m_lineeditDPEAgrupado->setVisible(false);
    m_labelDPEAgrupadoUnidade->setVisible(false);

    m_labelNumObsRI->setVisible(false);
    m_lineeditNumObsRI->setVisible(false);

    if (avaliacao == "Desvio-padrão Experimental Agrupado")
    {
        m_labelDPEAgrupado->setVisible(true);
        m_lineeditDPEAgrupado->setVisible(true);
        m_labelDPEAgrupadoUnidade->setVisible(true);

        m_labelNumObsRI->setVisible(true);
        m_lineeditNumObsRI->setVisible(true);
    }
}

void TipoAouB::distribuicaoAlterada(const QString& distribuicao)
{
    // distribuição-t
    // incerteza expandida (U)
    m_labelIncertezaExpandidaT->setVisible(false);
    m_lineeditIncertezaExpandidaT->setVisible(false);
    m_labelIncertezaExpandidaUnidadeT->setVisible(false);

    // graus de liberdade
    // m_labelGrausLiberdade->setVisible(false);
    // m_lineeditGrausLiberdade->setVisible(false);
    m_lineeditGrausLiberdade->setPlaceholderText("∞");

    // nível de confiança (%)
    m_labelNivelConfianca->setVisible(false);
    m_lineeditNivelConfianca->setVisible(false);
    m_labelNivelConfiancaPorcentagem->setVisible(false);

    // comum para em forma de U, retangular, triangular
    // meia-largura dos limites
    m_labelURTriMeiaLarguraLimites->setVisible(false);
    m_lineeditURTriMeiaLarguraLimites->setVisible(false);
    m_labelURTriMeiaLarguraLimitesUnidade->setVisible(false);

    // normal
    // incerteza expandida (U)
    m_labelIncertezaExpandidaNormal->setVisible(false);
    m_lineeditIncertezaExpandidaNormal->setVisible(false);
    m_labelIncertezaExpandidaUnidadeNormal->setVisible(false);

    // fator de abrangência (k)
    m_labelFatorAbrangencia->setVisible(false);
    m_lineeditFatorAbrangencia->setVisible(false);

    // outras
    // incerteza padrão
    m_labelIncertezaPadrao->setVisible(false);
    m_lineeditIncertezaPadrao->setVisible(false);
    m_labelIncertezaPadraoUnidade->setVisible(false);

    // trapezoidal
    // 1ª meia-largura dos limites
    m_labelTraMeiaLarguraLimites1->setVisible(false);
    m_lineeditTraMeiaLarguraLimites1->setVisible(false);
    m_labelTraMeiaLarguraLimitesUnidade1->setVisible(false);

    // 2ª meia-largura dos limites
    m_labelTraMeiaLarguraLimites2->setVisible(false);
    m_lineEditTraMeiaLarguraLimites2->setVisible(false);
    m_labelTraMeiaLarguraLimitesUnidade2->setVisible(false);

    if (distribuicao == "Distribuição-t")
    {
        // incerteza expandida (U)
        m_labelIncertezaExpandidaT->setVisible(true);
        m_lineeditIncertezaExpandidaT->setVisible(true);
        m_labelIncertezaExpandidaUnidadeT->setVisible(true);

        // graus de liberdade
        // m_labelGrausLiberdade->setVisible(true);
        // m_lineeditGrausLiberdade->setVisible(true);

        // nível de confiança (%)
        m_labelNivelConfianca->setVisible(true);
        m_lineeditNivelConfianca->setVisible(true);
        m_labelNivelConfiancaPorcentagem->setVisible(true);
    }

    if (distribuicao == "Em forma de U" || distribuicao == "Retangular" || distribuicao == "Triangular")
    {
        // meia-largura dos limites
        m_labelURTriMeiaLarguraLimites->setVisible(true);
        m_lineeditURTriMeiaLarguraLimites->setVisible(true);
        m_labelURTriMeiaLarguraLimitesUnidade->setVisible(true);
    }

    if (distribuicao == "Normal")
    {
        // incerteza expandida (U)
        m_labelIncertezaExpandidaNormal->setVisible(true);
        m_lineeditIncertezaExpandidaNormal->setVisible(true);
        m_labelIncertezaExpandidaUnidadeNormal->setVisible(true);

        // fator de abrangência (k)
        m_labelFatorAbrangencia->setVisible(true);
        m_lineeditFatorAbrangencia->setVisible(true);
        m_lineeditFatorAbrangencia->setPlaceholderText("2");
    }

    if (distribuicao == "Outras")
    {
        // incerteza padrão
        m_labelIncertezaPadrao->setVisible(true);
        m_lineeditIncertezaPadrao->setVisible(true);
        m_labelIncertezaPadraoUnidade->setVisible(true);
    }

    if (distribuicao == "Trapezoidal")
    {
        // 1ª meia-largura dos limites
        m_labelTraMeiaLarguraLimites1->setVisible(true);
        m_lineeditTraMeiaLarguraLimites1->setVisible(true);
        m_labelTraMeiaLarguraLimitesUnidade1->setVisible(true);

        // 2ª meia-largura dos limites
        m_labelTraMeiaLarguraLimites2->setVisible(true);
        m_lineEditTraMeiaLarguraLimites2->setVisible(true);
        m_labelTraMeiaLarguraLimitesUnidade2->setVisible(true);
    }
}

void TipoAouB::adicionarDadosExperimentais()
{
    m_dialogDadosExperimentais = new DadosExperimentais(objectName(), m_dadosExperimentais, this);
    m_dialogDadosExperimentais->exec();

    m_dadosExperimentais = m_dialogDadosExperimentais->dados();
}

void TipoAouB::coletarDados()
{
    // dados do tipo A ou B

    // comuns a A e B
    m_nomeGrandezaTipoAouB      = objectName();
    m_definicaoGrandezaTipoAouB = m_plaintexteditDefinicao->toPlainText();
    m_unidadeGrandezaTipoAouB   = m_lineeditUnidade->text();
    m_tipoGrandezaTipoAouB      = m_comboboxTipo->currentText();

    // Tipo A
    m_avaliacaoIncerteza = m_comboboxAvaliacaoIncerteza->currentText();
    m_DPEAgrupado        = m_lineeditDPEAgrupado->text();
    m_numObsRI           = m_lineeditNumObsRI->text();

    // Tipo B
    m_distribuicao = m_comboboxDistribuicao->currentText();

    // TODO: Bug valorEstimado 0
    auto testeValorEstimado = m_lineeditValorEstimado->text();
    if (testeValorEstimado == '-')
    {
        m_valorEstimado = QString('0');
    }
    else
    {
        m_valorEstimado = m_lineeditValorEstimado->text();
    }

    m_incertezaExpandidaT      = m_lineeditIncertezaExpandidaT->text();
    m_grausLiberdade           = m_lineeditGrausLiberdade->text();
    m_nivelConfianca           = m_lineeditNivelConfianca->text();
    m_URTriMeiaLarguraLimites  = m_lineeditURTriMeiaLarguraLimites->text();
    m_incertezaExpandidaNormal = m_lineeditIncertezaExpandidaNormal->text();
    m_fatorAbrangencia         = m_lineeditFatorAbrangencia->text();
    m_incertezaPadrao          = m_lineeditIncertezaPadrao->text();
    m_traMeiaLarguraLimites1   = m_lineeditTraMeiaLarguraLimites1->text();
    m_traMeiaLarguraLimites2   = m_lineEditTraMeiaLarguraLimites2->text();
}
