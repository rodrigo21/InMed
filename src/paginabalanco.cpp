#include "paginabalanco.hpp"
#include "ui_paginabalanco.h"

#include "dialogoconformidade.hpp"
#include "funcoesextras.hpp"
#include "setorescontribuicao.hpp"

#include <QCheckBox>
#include <QComboBox>
#include <QDialog>
#include <QLineEdit>
#include <QPainter>
#include <QPushButton>
#include <QTableWidgetItem>
#include <QTimer>
#include <QVBoxLayout>
#include <QtCharts/QChartView>
#include <QtCharts/QPieSeries>

PaginaBalanco::PaginaBalanco(QWidget* parent) :
    QWidget(parent), m_ui(new Ui::PaginaBalanco)
{
    m_ui->setupUi(this);

    m_ui->comboBoxValorNivelConfianca->setCurrentIndex(3);

    m_ui->comboBoxValorNivelConfianca->insertItem(6, "Manual");

    // conecta o sinal clicked do QPushButton ao sinal botaoAnteriorApertado
    connect(m_ui->pushButtonAnterior, &QPushButton::clicked, this,
            &PaginaBalanco::botaoAnteriorApertado);

    // exibe as fontes de incertezas
    connect(m_ui->pushButtonFontesIncertezas, &QPushButton::clicked, this,
            &PaginaBalanco::mostrarFontesIncertezas);

    // exibe a avaliacao da conformidade
    connect(m_ui->pushButtonAvaliacaoConformidade, &QPushButton::clicked, this,
            &PaginaBalanco::mostrarAvaliacaoConformidade);
}

void PaginaBalanco::setDadosBalanco(const GrandezaResultado& grandezaResultado,
                                    std::vector<ContribuicaoIncerteza>
                                        contribuicoes,
                                    double valorResultado)
{
    m_grandezaResultado = grandezaResultado;

    m_contribuicoes = contribuicoes;

    m_valorResuldado = valorResultado;

    m_k = 0.0;

    m_incertezaCombinada      = IncertezaCombinada(m_contribuicoes);
    m_valorIncertezaCombinada = m_incertezaCombinada.incertezaCombinada();

    m_incertezaExpandida = 0.0;

    m_veff = grausLiberdadeEfetivo(m_contribuicoes, m_incertezaCombinada);

    connect(m_ui->comboBoxValorNivelConfianca, &QComboBox::currentTextChanged, this,
            &PaginaBalanco::ajustarValorResultado);

    connect(m_ui->lineEditValorFatorAbrangencia, &QLineEdit::textChanged, this,
            &PaginaBalanco::ajustarValorResultado);

    connect(m_ui->checkBoxCalcularVeff, &QCheckBox::stateChanged, this,
            &PaginaBalanco::habilitarManual);

    QTimer::singleShot(0, this, &PaginaBalanco::atualizar);
}

PaginaBalanco::~PaginaBalanco()
{
    delete m_ui;
}

void PaginaBalanco::ajustarValorResultado()
{
    m_ui->labelValorResultado->setText(QString::number(m_valorResuldado, 'f', 2) + " "
                                       + QString::fromStdString(m_grandezaResultado.unidade()));

    m_ui->lineEditValorFatorAbrangencia->setReadOnly(true);

    m_ui->checkBoxCalcularVeff->hide();
    m_ui->lineEditValorVeff->hide();

    if (m_ui->comboBoxValorNivelConfianca->currentText() == "Manual")
    {
        m_ui->lineEditValorFatorAbrangencia->setReadOnly(false);

        m_k = m_ui->lineEditValorFatorAbrangencia->text().toDouble();
    }
    else if (m_veff != 0)
    {
        m_ui->checkBoxCalcularVeff->show();

        if (m_ui->checkBoxCalcularVeff->isChecked())
        {
            m_ui->lineEditValorVeff->setText(QString::number(m_veff));
            m_ui->lineEditValorVeff->show();

            m_k = kStudentsT(m_veff, m_ui->comboBoxValorNivelConfianca->currentText()
                                         .simplified()
                                         .replace("%", "")
                                         .toDouble());
        }
        else
        {
            m_k = fatorDeAbrangencia(m_ui->comboBoxValorNivelConfianca->currentText()
                                         .simplified()
                                         .replace("%", "")
                                         .toDouble());
        }

        m_ui->lineEditValorFatorAbrangencia->setText(QString::number(m_k, 'f', 2));
    }
    else
    {
        m_k = fatorDeAbrangencia(m_ui->comboBoxValorNivelConfianca->currentText()
                                     .simplified()
                                     .replace("%", "")
                                     .toDouble());

        m_ui->lineEditValorFatorAbrangencia->setText(QString::number(m_k, 'f', 2));
    }

    m_incertezaExpandida = m_valorIncertezaCombinada * m_k;

    m_ui->labelValorIncertezaExpandida->setText(
        QString::number(m_incertezaExpandida, 'f', 2) + " "
        + QString::fromStdString(m_grandezaResultado.unidade()));
}

void PaginaBalanco::habilitarManual()
{
    if (m_ui->checkBoxCalcularVeff->isChecked())
    {
        m_ui->comboBoxValorNivelConfianca->removeItem(6);
    }
    else
    {
        if (m_ui->comboBoxValorNivelConfianca->itemText(6) != "Manual")
        {
            m_ui->comboBoxValorNivelConfianca->insertItem(6, "Manual");
        }
    }

    emit ajustarValorResultado();
}

void PaginaBalanco::mostrarFontesIncertezas()
{
    using namespace QtCharts;

    auto serieContribuicao = new QPieSeries();

    for (const auto& inc : m_incertezaCombinada.contribuicaoes())
    {
        serieContribuicao->append(
            new SetoresContribuicao(QString::fromStdString(inc.grandeza()->nomeGrandeza()),
                                    indiceContribuicao(inc.contribuicaoIncerteza())));
    }

    QDialog dialogoDiagrama;

    auto diagramaVisao = new QChartView(&dialogoDiagrama);
    diagramaVisao->chart()->setTitle(tr("Fontes de Incertezas"));
    diagramaVisao->chart()->addSeries(serieContribuicao);
    diagramaVisao->chart()->setAnimationOptions(QChart::AllAnimations);
    diagramaVisao->chart()->legend()->setVisible(true);
    diagramaVisao->chart()->legend()->setAlignment(Qt::AlignRight);
    diagramaVisao->setRenderHint(QPainter::Antialiasing);

    auto layout = new QVBoxLayout();
    layout->addWidget(diagramaVisao);

    dialogoDiagrama.setLayout(layout);
    dialogoDiagrama.setWindowTitle(tr("Fontes de Incertezas"));
    dialogoDiagrama.resize(800, 600);
    dialogoDiagrama.exec();
}

void PaginaBalanco::mostrarAvaliacaoConformidade()
{
    m_dialogoConformidade = new DialogoConformidade(m_valorResuldado, m_incertezaExpandida,
                                                    QString::fromStdString(m_grandezaResultado.unidade()), this);

    m_dialogoConformidade->exec();
}

void PaginaBalanco::atualizar()
{
    m_ui->groupBoxGrandezaResultado->setTitle(
        QString::fromStdString(m_grandezaResultado.nomeGrandeza()));

    m_ui->lineEditDefinicaoResultado->setText(
        QString::fromStdString(m_grandezaResultado.definicao()));

    criarTabelaGrandezas();
    criarTabelaGrandezaResultado();
    ajustarValorResultado();
}

void PaginaBalanco::criarTabelaGrandezas()
{
    m_ui->tableWidgetGrandezas->setRowCount(m_contribuicoes.size());
    m_ui->tableWidgetGrandezas->setColumnCount(7);
    m_ui->tableWidgetGrandezas->setHorizontalHeaderLabels(
        {"Grandeza", "Valor", "Incerteza Padrão", "Distribuição", "Coeficiente de Sensibilidade",
         "Contribuição de Incerteza", "Índice de Contribuição de Incerteza"});

    m_ui->tableWidgetGrandezas->verticalHeader()->setVisible(false);

    auto protoItem = new QTableWidgetItem;
    protoItem->setTextAlignment(Qt::AlignCenter);

    for (int i = 0; i < m_ui->tableWidgetGrandezas->rowCount(); ++i)
    {
        QTableWidgetItem* item;

        item = protoItem->clone();
        item->setText(QString::fromStdString(m_contribuicoes[i].grandeza()->nomeGrandeza()));
        m_ui->tableWidgetGrandezas->setItem(i, 0, item);

        item = protoItem->clone();
        item->setText(QString::number(m_contribuicoes[i].grandeza()->valorEstimado(), 'f', 2) + " "
                      + QString::fromStdString(m_contribuicoes[i].grandeza()->unidade()));
        m_ui->tableWidgetGrandezas->setItem(i, 1, item);

        item = protoItem->clone();
        item->setText(QString::number(m_contribuicoes[i].grandeza()->incertezaPadrao(), 'f', 2)
                      + " " + QString::fromStdString(m_contribuicoes[i].grandeza()->unidade()));
        m_ui->tableWidgetGrandezas->setItem(i, 2, item);

        item = protoItem->clone();
        item->setText(distribuicaoQString(m_contribuicoes[i].grandeza()->distribuicao()));
        m_ui->tableWidgetGrandezas->setItem(i, 3, item);

        item = protoItem->clone();
        item->setText(QString::number(m_contribuicoes[i].valorCoeficienteSensibilidade(), 'f', 2));
        m_ui->tableWidgetGrandezas->setItem(i, 4, item);

        item = protoItem->clone();
        item->setText(QString::number(m_contribuicoes[i].contribuicaoIncerteza(), 'f', 2) + " "
                      + QString::fromStdString(m_grandezaResultado.unidade()));
        m_ui->tableWidgetGrandezas->setItem(i, 5, item);

        item = protoItem->clone();
        item->setText(
            QString::number(indiceContribuicao(m_contribuicoes[i].contribuicaoIncerteza()), 'f', 2)
            + " %");
        m_ui->tableWidgetGrandezas->setItem(i, 6, item);
    }

    m_ui->tableWidgetGrandezas->resizeColumnsToContents();
    m_ui->tableWidgetGrandezas->resizeRowsToContents();
}

void PaginaBalanco::criarTabelaGrandezaResultado()
{
    m_ui->tableWidgetGrandezaResultado->setRowCount(1);
    m_ui->tableWidgetGrandezaResultado->setColumnCount(3);
    m_ui->tableWidgetGrandezaResultado->setHorizontalHeaderLabels(
        {"Grandeza", "Valor", "Incerteza Padrão"});

    m_ui->tableWidgetGrandezaResultado->verticalHeader()->setVisible(false);

    auto protoItem = new QTableWidgetItem;
    protoItem->setTextAlignment(Qt::AlignCenter);
    QTableWidgetItem* item;

    item = protoItem->clone();
    item->setText(QString::fromStdString(m_grandezaResultado.nomeGrandeza()));
    m_ui->tableWidgetGrandezaResultado->setItem(0, 0, item);

    item = protoItem->clone();
    item->setText(QString::number(m_valorResuldado, 'f', 2) + " "
                  + QString::fromStdString(m_grandezaResultado.unidade()));
    m_ui->tableWidgetGrandezaResultado->setItem(0, 1, item);

    item = protoItem->clone();
    item->setText(QString::number(m_valorIncertezaCombinada, 'f', 2) + " "
                  + QString::fromStdString(m_grandezaResultado.unidade()));
    m_ui->tableWidgetGrandezaResultado->setItem(0, 2, item);

    m_ui->tableWidgetGrandezaResultado->resizeColumnsToContents();
    m_ui->tableWidgetGrandezaResultado->resizeRowsToContents();
}

double PaginaBalanco::indiceContribuicao(double contribuicao)
{
    return ((contribuicao * contribuicao) / (m_valorIncertezaCombinada * m_valorIncertezaCombinada))
           * 100;
}
