#include "tiporesultado.hpp"

#include "funcoesextras.hpp"

#include <QFormLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPlainTextEdit>
#include <QSizePolicy>

TipoResultado::TipoResultado(const QString& objetoNome, QWidget* parent) :
    QWidget(parent)
{
    // nome do widget
    setObjectName(objetoNome);

    auto widgetDefinicaoNomeUnidade = new QWidget;

    // espaço em branco para alinhar os widgets
    QString espaco(" ");

    // descrição

    auto labelDefinicao = new QLabel(QString("%1Definição:").arg(espaco.repeated(30)), widgetDefinicaoNomeUnidade);

    m_plaintexteditDefinicao = new QPlainTextEdit(widgetDefinicaoNomeUnidade);
    ajustarPlainTextEdit(m_plaintexteditDefinicao, 4, true, true, QSizePolicy::Expanding,
                         QSizePolicy::Minimum);

    // unidade

    auto labelUnidade = new QLabel("Unidade:", widgetDefinicaoNomeUnidade);

    m_lineeditUnidade = new QLineEdit(widgetDefinicaoNomeUnidade);
    ajustarLineEdit(m_lineeditUnidade, 0, 0, false, false, Qt::StrongFocus, QSizePolicy::Maximum,
                    QSizePolicy::Maximum);

    // tipo (sempre como Resultado)

    auto labelTipo = new QLabel("Tipo:", widgetDefinicaoNomeUnidade);

    auto lineEditTipo = new QLineEdit("Resultado", widgetDefinicaoNomeUnidade);
    ajustarLineEdit(lineEditTipo, 0, 0, true, false, Qt::NoFocus, QSizePolicy::Maximum,
                    QSizePolicy::Maximum);

    // form layout com os widgets para o tipo Resultado
    auto formlayoutDefinicaoNomeUnidade = new QFormLayout;
    formlayoutDefinicaoNomeUnidade->addRow(labelDefinicao, m_plaintexteditDefinicao);
    formlayoutDefinicaoNomeUnidade->addRow(labelUnidade, m_lineeditUnidade);
    formlayoutDefinicaoNomeUnidade->addRow(labelTipo, lineEditTipo);

    setLayout(formlayoutDefinicaoNomeUnidade);

    coletarDados();

    connect(m_plaintexteditDefinicao, &QPlainTextEdit::textChanged, this,
            &TipoResultado::coletarDados);
    connect(m_lineeditUnidade, &QLineEdit::editingFinished, this, &TipoResultado::coletarDados);
}

QString TipoResultado::nomeGrandezaResultado() const
{
    return m_nomeGrandezaResultado;
}

QString TipoResultado::definicaoGrandezaResultado() const
{
    return m_definicaoGrandezaResultado;
}

QString TipoResultado::unidadeGrandezaResultado() const
{
    return m_unidadeGrandezaResultado;
}

void TipoResultado::coletarDados()
{
    // dados da grandeza resultado
    m_nomeGrandezaResultado      = objectName();
    m_definicaoGrandezaResultado = m_plaintexteditDefinicao->toPlainText();
    m_unidadeGrandezaResultado   = m_lineeditUnidade->text();
}
