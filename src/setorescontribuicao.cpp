#include "setorescontribuicao.hpp"

using namespace QtCharts;

SetoresContribuicao::SetoresContribuicao(QString grandeza, double valor) :
    m_grandezaNome(grandeza)
{
    setValue(valor);

    atualizarRotulos();

    connect(this, SIGNAL(percentageChanged()), this, SLOT(atualizarRotulos()));
    connect(this, SIGNAL(hovered(bool)), this, SLOT(mostrarDestacado(bool)));
}

void SetoresContribuicao::atualizarRotulos()
{
    QString rotulo = m_grandezaNome;
    rotulo += ": ";
    rotulo += QString::number(this->value(), 'f', 2);
    rotulo += " %";

    setLabel(rotulo);
}

void SetoresContribuicao::mostrarDestacado(bool mostrar)
{
    setLabelVisible(mostrar);
    setExploded(mostrar);
}
