#ifndef INCERTEZACOMBINADA_HPP
#define INCERTEZACOMBINADA_HPP

#include "contribuicaoincerteza.hpp"

#include <vector>

class IncertezaCombinada
{
public:
    explicit IncertezaCombinada(std::vector<ContribuicaoIncerteza> contribuicoes);

    double incertezaCombinada() const;

    std::vector<ContribuicaoIncerteza> contribuicaoes() const;

    IncertezaCombinada()  = default;
    ~IncertezaCombinada() = default;

private:
    double m_incertezaCombinada;

    std::vector<ContribuicaoIncerteza> m_contribuicaoes;
};

#endif // INCERTEZACOMBINADA_HPP
