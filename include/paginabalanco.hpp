#ifndef PAGINABALANCO_HPP
#define PAGINABALANCO_HPP

#include "contribuicaoincerteza.hpp"
#include "grandezaresultado.hpp"
#include "incertezacombinada.hpp"

#include <QString>
#include <QWidget>

#include <vector>

class DialogoConformidade;

namespace Ui
{
class PaginaBalanco;
}

class PaginaBalanco : public QWidget
{
    Q_OBJECT

public:
    explicit PaginaBalanco(QWidget* parent = 0);

    void setDadosBalanco(const GrandezaResultado& grandezaResultado,
                         std::vector<ContribuicaoIncerteza> contribuicoes, double valorResultado);

    ~PaginaBalanco();

signals:
    void botaoAnteriorApertado();

private slots:
    void ajustarValorResultado();
    void habilitarManual();

    void mostrarFontesIncertezas();

    void mostrarAvaliacaoConformidade();

private:
    void atualizar();

    void criarTabelaGrandezas();
    void criarTabelaGrandezaResultado();

    double indiceContribuicao(double contribuicao);

    GrandezaResultado m_grandezaResultado;
    std::vector<ContribuicaoIncerteza> m_contribuicoes;
    double m_valorResuldado;

    unsigned int m_veff;
    double m_k;

    IncertezaCombinada m_incertezaCombinada;
    double m_valorIncertezaCombinada;

    double m_incertezaExpandida;

    DialogoConformidade* m_dialogoConformidade;

    Ui::PaginaBalanco* m_ui;
};

#endif // PAGINABALANCO_HPP
