#ifndef PAGINATITULO_HPP
#define PAGINATITULO_HPP

#include <QString>
#include <QWidget>

namespace Ui
{
class PaginaTitulo;
}

class PaginaTitulo : public QWidget
{
    Q_OBJECT

public:
    explicit PaginaTitulo(QWidget* parent = 0);

    ~PaginaTitulo();

    QString titulo() const;

    QString dadosAutor() const;
    QString dadosData() const;
    QString dadosReferencia() const;
    QString dadosVersao() const;

    QString descricao() const;

signals:
    void botaoProximoApertado();

private slots:
    void habilitarBotaoProximo();

private:
    Ui::PaginaTitulo* m_ui;
};

#endif // PAGINATITULO_HPP
