#ifndef TIPOAOUB_HPP
#define TIPOAOUB_HPP

#include <QString>
#include <QVector>
#include <QWidget>

class DadosExperimentais;
class QComboBox;
class QLabel;
class QLineEdit;
class QPlainTextEdit;

class TipoAouB : public QWidget
{
    Q_OBJECT

public:
    TipoAouB(const QString& objetoNome, QWidget* parent = 0);

    // dados do tipo A ou B

    // comuns a A e B
    QString nomeGrandezaTipoAouB() const;
    QString definicaoGrandezaTipoAouB() const;
    QString unidadeGrandezaTipoAouB() const;
    QString tipoGrandezaTipoAouB() const;

    // tipo A

    QVector<double> dadosExperimentais() const;
    QString avaliacaoIncerteza() const;
    QString DPEAgrupado() const;
    QString numObsRI() const;

    // tipo B
    QString distribuicao() const;
    QString valorEstimado() const;
    QString incertezaExpandidaT() const;
    QString grausLiberdade() const;
    QString nivelConfianca() const;
    QString URTriMeiaLarguraLimites() const;
    QString incertezaExpandidaNormal() const;
    QString fatorAbrangencia() const;
    QString incertezaPadrao() const;
    QString traMeiaLarguraLimites1() const;
    QString traMeiaLarguraLimites2() const;

private slots:
    void tipoAlterado(const QString& tipo);

    void avaliacaoIncertezaAlterada(const QString& avaliacao);

    void distribuicaoAlterada(const QString& distribuicao);

    void adicionarDadosExperimentais();

    void coletarDados();

private:
    /*********** dados do tipo A ou B ***********/

    // comuns a A e B
    QString m_nomeGrandezaTipoAouB;
    QString m_definicaoGrandezaTipoAouB;
    QString m_unidadeGrandezaTipoAouB;
    QString m_tipoGrandezaTipoAouB;

    // tipo A
    QVector<double> m_dadosExperimentais; // vetor com os valores dos dados experimentais
    QString m_avaliacaoIncerteza;
    QString m_DPEAgrupado;
    QString m_numObsRI;

    // tipo B
    QString m_distribuicao;
    QString m_valorEstimado;
    QString m_incertezaExpandidaT;
    QString m_grausLiberdade;
    QString m_nivelConfianca;
    QString m_URTriMeiaLarguraLimites;
    QString m_incertezaExpandidaNormal;
    QString m_fatorAbrangencia;
    QString m_incertezaPadrao;
    QString m_traMeiaLarguraLimites1;
    QString m_traMeiaLarguraLimites2;

    /*********** widgets para a interface gráfica ***********/

    QWidget* m_widgetDadosA;
    QWidget* m_widgetDadosB;

    /***** widgets comuns a A e B *****/

    // descrição
    QPlainTextEdit* m_plaintexteditDefinicao;

    // unidade
    QLineEdit* m_lineeditUnidade;

    // tipo
    QComboBox* m_comboboxTipo;

    /***** tipo A *****/

    // tabela com os dados experimentais para o tipo A
    DadosExperimentais* m_dialogDadosExperimentais;

    // avaliação da incerteza
    QComboBox* m_comboboxAvaliacaoIncerteza;

    // desvio padrão experimental agrupado
    QLabel* m_labelDPEAgrupado;
    QLineEdit* m_lineeditDPEAgrupado;
    QLabel* m_labelDPEAgrupadoUnidade;

    // número de observações repetidas e independentes
    QLabel* m_labelNumObsRI;
    QLineEdit* m_lineeditNumObsRI;

    /***** tipo B *****/

    // distribuição
    QComboBox* m_comboboxDistribuicao;

    // valor estimado
    QLineEdit* m_lineeditValorEstimado;

    // distribuição-t
    // incerteza expandida (U)
    QLabel* m_labelIncertezaExpandidaT;
    QLineEdit* m_lineeditIncertezaExpandidaT;
    QLabel* m_labelIncertezaExpandidaUnidadeT;

    // graus de liberdade
    QLabel* m_labelGrausLiberdade;
    QLineEdit* m_lineeditGrausLiberdade;

    // nível de confiança (%)
    QLabel* m_labelNivelConfianca;
    QLineEdit* m_lineeditNivelConfianca;
    QLabel* m_labelNivelConfiancaPorcentagem;

    // comum para em forma de U, retangular, triangular
    // meia-largura dos limites
    QLabel* m_labelURTriMeiaLarguraLimites;
    QLineEdit* m_lineeditURTriMeiaLarguraLimites;
    QLabel* m_labelURTriMeiaLarguraLimitesUnidade;

    // normal
    // incerteza expandida (U)
    QLabel* m_labelIncertezaExpandidaNormal;
    QLineEdit* m_lineeditIncertezaExpandidaNormal;
    QLabel* m_labelIncertezaExpandidaUnidadeNormal;

    // fator de abrangência (k)
    QLabel* m_labelFatorAbrangencia;
    QLineEdit* m_lineeditFatorAbrangencia;

    // outras
    // incerteza padrão
    QLabel* m_labelIncertezaPadrao;
    QLineEdit* m_lineeditIncertezaPadrao;
    QLabel* m_labelIncertezaPadraoUnidade;

    // trapezoidal
    // 1ª meia-largura dos limites
    QLabel* m_labelTraMeiaLarguraLimites1;
    QLineEdit* m_lineeditTraMeiaLarguraLimites1;
    QLabel* m_labelTraMeiaLarguraLimitesUnidade1;

    // 2ª meia-largura dos limites
    QLabel* m_labelTraMeiaLarguraLimites2;
    QLineEdit* m_lineEditTraMeiaLarguraLimites2;
    QLabel* m_labelTraMeiaLarguraLimitesUnidade2;
};

#endif // TIPOAOUB_HPP
