#ifndef MENUCLI_HPP
#define MENUCLI_HPP

#include "grandezaresultado.hpp"
#include "modelomatematico.hpp"

#include <memory>
#include <vector>

class Grandeza;

ModeloMatematico lerModelo();

GrandezaResultado entradaGrandezaResultado();

std::vector<std::shared_ptr<Grandeza>> entradaGrandezas(const ModeloMatematico& modelo);

void menucli();

#endif // MENUCLI_HPP
