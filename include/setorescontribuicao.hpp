#ifndef SETORESCONTRIBUICAO_HPP
#define SETORESCONTRIBUICAO_HPP

#include <QString>

#include <QtCharts/QPieSlice>

class SetoresContribuicao : public QtCharts::QPieSlice
{
    Q_OBJECT

public:
    SetoresContribuicao(QString grandeza, double valor);

    virtual ~SetoresContribuicao() = default;

private slots:
    void atualizarRotulos();
    void mostrarDestacado(bool mostrar);

private:
    QString m_grandezaNome;
};

#endif // SETORESCONTRIBUICAO_HPP
