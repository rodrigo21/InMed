#ifndef RESULTADOMEDICAO_HPP
#define RESULTADOMEDICAO_HPP

#include "contribuicaoincerteza.hpp"
#include "grandezaresultado.hpp"
#include "modelomatematico.hpp"

#include <ginac/ginac.h>

#include <QObject>

#include <memory>
#include <vector>

class Grandeza;

class ResultadoMedicao : public QObject
{
    Q_OBJECT

public:
    explicit ResultadoMedicao(QObject* parent = 0);

    void setDadosMedicao(const ModeloMatematico& modelo,
                         const GrandezaResultado& grandezaResultado,
                         std::vector<std::shared_ptr<Grandeza>>
                             grandezas);

    // mapa com os valores estimados das grandezas
    GiNaC::exmap mapaValorEstimadoGrandezas() const;

    // vetor com as contribuições de incertezas das grandezas
    std::vector<ContribuicaoIncerteza> contribuicoes() const;

    double valorResultado() const;

private:
    void atualizar();

    void calcularValoresEstimados();

    void calcularValorResultado();

    void calcularContribuicoesIncerteza();

    ModeloMatematico m_modelo;
    GrandezaResultado m_grandezaResultado;
    std::vector<std::shared_ptr<Grandeza>> m_grandezas;

    // mapa com os valores estimados das grandezas
    GiNaC::exmap m_mapaValorEstimadoGrandezas;

    // vetor com as contribuições de incertezas das grandezas
    std::vector<ContribuicaoIncerteza> m_contribuicoes;

    double m_valorResultado;
};

#endif // RESULTADOMEDICAO_HPP
