#ifndef GRANDEZARESULTADO_HPP
#define GRANDEZARESULTADO_HPP

#include "grandeza.hpp"

#include <ginac/ginac.h>

#include <string>

class ModeloMatematico;

class GrandezaResultado : public Grandeza
{
public:
    GrandezaResultado(const GiNaC::symbol& simboloGrandeza = GiNaC::symbol(),
                      const std::string& definicao         = std::string(),
                      const std::string& unidade           = std::string());

    double valorEstimado(const ModeloMatematico& modelo, GiNaC::exmap mapaValorEstimado);

    virtual double incertezaPadrao() override;
    virtual bool possuiCorrelacao() override;

    ~GrandezaResultado() = default;
};

#endif // GRANDEZARESULTADO_HPP
