#ifndef DADOSMEDICAO_HPP
#define DADOSMEDICAO_HPP

#include "grandezaresultado.hpp"
#include "modelomatematico.hpp"

#include <QObject>
#include <QString>
#include <QVector>

#include <memory>
#include <vector>

class Grandeza;

class QStringList;

class DadosMedicao : public QObject
{
    Q_OBJECT

public:
    explicit DadosMedicao(QObject* parent = 0);

    // dados da PaginaTitulo (0)

    void setTitulo(const QString& titulo);
    void setAutor(const QString& autor);
    void setData(const QString& data);
    void setReferencia(const QString& referencia);
    void setVersao(const QString& versao);
    void setDescricaoMedicao(const QString& descricao);

    // dados da PaginaModelo (1)

    // dados do tipo resultado

    void setNomeGrandezaResultado(const QString& nomeGrandezaResultado);
    void setDefinicaoGrandezaResultado(const QString& definicaoGrandezaResultado);
    void setUnidadeGrandezaResultado(const QString& unidadeGrandezaResultado);

    // dados do tipo A ou B

    // comuns a A e B
    void setNomeGrandezasAouB(const QVector<QString>& nomeGrandezasAouB);
    void setDefinicaoGrandezasAouB(const QVector<QString>& definicaoGrandezasAouB);
    void setUnidadeGrandezasAouB(const QVector<QString>& unidadeGrandezasAouB);
    void setTipoGrandezaAouB(const QVector<QString>& tipoGrandezaAouB);
    void setCorrecaoGrandezaAouB(const QVector<QString>& correcaoGrandezaAouB);

    // tipo A
    void setDadosExperimentaisAouB(const QVector<QVector<double>>& dadosExperimentaisAouB);
    void setAvaliacaoIncertezaAouB(const QVector<QString>& avaliacaoIncertezaAouB);
    void setDPEAgrupadoAouB(const QVector<double>& DPEAgrupadoAouB);
    void setNumObsRIAouB(const QVector<unsigned int>& numObsRIAouB);

    // tipo B
    void setDistribuicao(const QVector<QString>& distribuicao);
    void setValorEstimado(const QVector<double>& valorEstimado);
    void setIncertezaExpandidaT(const QVector<double>& incertezaExpandidaT);
    void setGrausLiberdade(const QVector<unsigned int>& grausLiberdade);
    void setNivelConfianca(const QVector<double>& nivelConfianca);
    void setURTriMeiaLarguraLimites(const QVector<double>& URTriMeiaLarguraLimites);
    void setIncertezaExpandidaNormal(const QVector<double>& incertezaExpandidaNormal);
    void setFatorAbrangencia(const QVector<double>& fatorAbrangencia);
    void setIncertezaPadrao(const QVector<double>& incertezaPadrao);
    void setTraMeiaLarguraLimites1(const QVector<double>& traMeiaLarguraLimites1);
    void setTraMeiaLarguraLimites2(const QVector<double>& traMeiaLarguraLimites2);

    // modelo matemático (lodo direito da equação)
    ModeloMatematico modeloMatematico() const;
    void setModeloMatematico(const ModeloMatematico& modeloMatematico);

    // numero de grandezas excluindo a grandeza do resultado
    void setNumeroGrandezas(int numeroGrandezas);

    // dados da PaginaResultado

    // GrandezaResultado da medição
    GrandezaResultado grandezaResultado();
    // vector com todas as outras grandezas
    std::vector<std::shared_ptr<Grandeza>> grandezas();

    bool grandezasCorretas() const;

signals:
    void invalidosDadosExperimentais(const QStringList& grandezasInvalidas);
    void erroValoresDPEA(const QStringList& grandezasInvalidas);
    void erroValoresDistribuicao(const QStringList& grandezasInvalidas);

private:
    // dados da PaginaTitulo (0)

    QString m_titulo;
    QString m_autor;
    QString m_data;
    QString m_referencia;
    QString m_versao;
    QString m_descricaoMedicao;

    // dados da PaginaModelo (1)

    // dados do tipo resultado

    QString m_nomeGrandezaResultado;
    QString m_definicaoGrandezaResultado;
    QString m_unidadeGrandezaResultado;

    // dados do tipo A ou B

    // comuns a A e B
    QVector<QString> m_nomeGrandezasAouB;
    QVector<QString> m_definicaoGrandezasAouB;
    QVector<QString> m_unidadeGrandezasAouB;
    QVector<QString> m_tipoGrandezaAouB;
    QVector<QString> m_correcaoAouB;

    // tipo A
    QVector<QVector<double>> m_dadosExperimentaisAouB;
    QVector<QString> m_avaliacaoIncertezaAouB;
    QVector<double> m_DPEAgrupadoAouB;
    QVector<unsigned int> m_numObsRIAouB;

    // tipo B
    QVector<QString> m_distribuicao;
    QVector<double> m_valorEstimado;
    QVector<double> m_incertezaExpandidaT;
    QVector<unsigned int> m_grausLiberdade;
    QVector<double> m_nivelConfianca;
    QVector<double> m_URTriMeiaLarguraLimites;
    QVector<double> m_incertezaExpandidaNormal;
    QVector<double> m_fatorAbrangencia;
    QVector<double> m_incertezaPadrao;
    QVector<double> m_traMeiaLarguraLimites1;
    QVector<double> m_traMeiaLarguraLimites2;

    // modelo matemático (lodo direito da equação)
    ModeloMatematico m_modeloMatematico;

    // numero de grandezas excluindo a grandeza do resultado
    int m_numeroGrandezas;

    // dados da PaginaResultado
    GrandezaResultado m_grandezaResultado;
    std::vector<std::shared_ptr<Grandeza>> m_grandezas;

    bool m_grandezasCorretas;
};

#endif // DADOSMEDICAO_HPP
