#ifndef INCERTEZATIPOB_HPP
#define INCERTEZATIPOB_HPP

#include "grandeza.hpp"

class IncertezaTipoB
{
public:
    IncertezaTipoB(unsigned int grausDeLiberdade,
                   Distribuicao distribuicao,
                   double dado1,
                   double dado2 = 0.0);

    double incertezaPadraoB() const;

    ~IncertezaTipoB() = default;

private:
    double m_incertezaPadrao; // incerteza-padrão
};

#endif // INCERTEZATIPOB_HPP
