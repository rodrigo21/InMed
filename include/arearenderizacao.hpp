#ifndef AREARENDERIZACAO_HPP
#define AREARENDERIZACAO_HPP

#include "funcoesextras.hpp"

#include <QRectF>
#include <QString>
#include <QWidget>

class QPaintEvent;
class QPainter;
class QPen;

enum class DirecaoTriangulo
{
    EmCima,
    Embaixo,
    Direita,
    Esquerda
};

// posicão de y em relação aos limites para resultados não determinados
enum class YPosicao
{
    EsqLI,  // esquerda do limite inferior
    DirLI,  // direita do limite inferior
    LimInf, // no limite inferior
    EsqLS,  // esquerda do limite superior
    DirLS,  // esquerda do limite superior
    LimSup, // no limite superior
    EL      // entre limites
};

class AreaRenderizacao : public QWidget
{
    Q_OBJECT

public:
    explicit AreaRenderizacao(QWidget* parent = 0);

signals:
public slots:
    void avaliarConformidade(double y,
                             double U,
                             QString unidade,
                             double valorNominal,
                             double limiteInferior,
                             double limiteSuperior,
                             Conformidade conformidade);

protected:
    void paintEvent(QPaintEvent* event) override;

private:
    void desenharTriangulo(double x,
                           double y,
                           double largura,
                           double altura,
                           DirecaoTriangulo direcao,
                           QPainter& pintorTri,
                           Qt::GlobalColor cor);
    void desenharZonaEspecificacao(double limiteInferior,
                                   double limiteSuperior,
                                   QPainter& pintorZE,
                                   QPen& canetaZE);
    void desenharZonaConformidade(QPainter& pintorZC,
                                  QPen& canetaZC,
                                  double incerteza = 0.0,
                                  YPosicao yPos    = YPosicao::EL);
    void desenharIcertezas(QRectF limites,
                           QPainter& pintorInc,
                           double incertezaLagura = 0.0,
                           YPosicao yPos          = YPosicao::EL);
    void desenharLegendas(QPainter& pintorLeg);

    double m_y;        // resultado da medição
    double m_U;        // incerteza expandida
    QString m_unidade; // unidade do resultado
    double m_valorNominal;
    double m_limiteInferior;
    double m_limiteSuperior;
    Conformidade m_conformidade;
};

#endif // AREARENDERIZACAO_HPP
