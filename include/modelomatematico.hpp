#ifndef MODELOMATEMATICO_HPP
#define MODELOMATEMATICO_HPP

#include <ginac/ginac.h>

#include <string>
#include <vector>

class ModeloMatematico
{
public:
    explicit ModeloMatematico(std::string entrada = "Invalid_Model");

    ~ModeloMatematico() = default;

    GiNaC::ex modelo() const;

    std::vector<GiNaC::symbol> simbolos() const;

    std::vector<std::string> simbolosStrings() const;

private:
    GiNaC::ex m_modelo;

    std::vector<GiNaC::symbol> m_simbolos;

    std::vector<std::string> m_vectorSimbolosStrings;
};

#endif // MODELOMATEMATICO_HPP
