#ifndef GRANDEZA_HPP
#define GRANDEZA_HPP

#include <ginac/ginac.h>

#include <string>

enum class Distribuicao
{
    FormaU,
    Normal,
    Outras,
    Retangular,
    T,
    Trapezoidal,
    Triangular,

    Invalida
};

class Grandeza
{
public:
    Grandeza(const GiNaC::symbol& simboloGrandeza, std::string definicao, std::string unidade);

    GiNaC::symbol simboloGrandeza() const;
    std::string nomeGrandeza() const;
    std::string definicao() const;
    std::string unidade() const;

    int grausDeLiberdade() const;
    double valorEstimado() const;
    Distribuicao distribuicao() const;

    double correcao() const;

    virtual double incertezaPadrao() = 0;
    virtual bool possuiCorrelacao()  = 0;

    virtual ~Grandeza() = default;

protected:
    GiNaC::symbol m_simboloGrandeza;

    std::string m_nomeGrandeza;
    std::string m_definicao;
    std::string m_unidade;

    int m_grausDeLiberdade;   // número de graus de liberdade
    double m_valorEstimado;   // valor estimado da grandeza
    double m_incertezaPadrao; // incerteza-padrão calculada

    bool m_possuiCorrelacao;

    Distribuicao m_distribuicao; // tipo de distribuição

    double m_correcao;
};

#endif // GRANDEZA_HPP
