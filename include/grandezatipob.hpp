#ifndef GRANDEZATIPOB_HPP
#define GRANDEZATIPOB_HPP

#include "grandeza.hpp"

#include <ginac/ginac.h>

#include <string>

class GrandezaTipoB : public Grandeza
{
public:
    GrandezaTipoB(const GiNaC::symbol& simboloGrandeza,
                  const std::string& definicao,
                  const std::string& unidade,
                  double valorEstimado,
                  unsigned int grausDeLiberdade,
                  Distribuicao distribuicao,
                  double dado1,
                  double dado2 = 0.0);

    virtual double incertezaPadrao() override;
    virtual bool possuiCorrelacao() override;

    ~GrandezaTipoB() = default;
};

#endif // GRANDEZATIPOB_HPP
