#ifndef DERIVADASPARCIAIS_HPP
#define DERIVADASPARCIAIS_HPP

#include "grandezaresultado.hpp"
#include "modelomatematico.hpp"

#include <QDialog>

class QWidget;

namespace Ui
{
class DerivadasParciais;
}

class DerivadasParciais : public QDialog
{
    Q_OBJECT

public:
    DerivadasParciais(GrandezaResultado grandezaResultado,
                      ModeloMatematico modeloMatematico,
                      QWidget* parent = 0);

    ~DerivadasParciais();

private:
    Ui::DerivadasParciais* m_ui;
};

#endif // DERIVADASPARCIAIS_HPP
