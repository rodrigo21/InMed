#ifndef DIALOGOCONFORMIDADE_HPP
#define DIALOGOCONFORMIDADE_HPP

#include <QDialog>
#include <QString>

class QWidget;

namespace Ui
{
class DialogoConformidade;
}

class DialogoConformidade : public QDialog
{
    Q_OBJECT

public:
    DialogoConformidade(double resultadoMedicao,
                        double incertezaExpandida,
                        QString unidade,
                        QWidget* parent = 0);

    ~DialogoConformidade();

private slots:
    void avaliarConformidade();

private:
    double m_resultadoMedicao;
    double m_incertezaExpandida;
    QString m_unidade;

    Ui::DialogoConformidade* m_ui;
};

#endif // DIALOGOCONFORMIDADE_HPP
