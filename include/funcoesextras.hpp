#ifndef FUNCOESEXTRAS_HPP
#define FUNCOESEXTRAS_HPP

#include "grandeza.hpp"

#include <QSizePolicy>
#include <QString>

#include <vector>

class ContribuicaoIncerteza;
class IncertezaCombinada;

class QLineEdit;
class QPlainTextEdit;

// cálculo das incertezas

double kStudentsT(unsigned int grausDeLiberdade, double fatorP);

unsigned int grausLiberdadeEfetivo(std::vector<ContribuicaoIncerteza> contribuicoes,
                                   const IncertezaCombinada& incertezaCombinada);

double fatorDeAbrangencia(double nivelDaConfianca);

// Qt GUI

void ajustarPlainTextEdit(QPlainTextEdit* editor,
                          int numLinhas,
                          bool tabMudaFoco               = false,
                          bool isAlturaFixa              = false,
                          QSizePolicy::Policy horizontal = QSizePolicy::Preferred,
                          QSizePolicy::Policy vertical   = QSizePolicy::Preferred);

void ajustarLineEdit(QLineEdit* editor,
                     int pixelsAdicionalLargura,
                     int pixelsAdicionalAltura,
                     bool isApenasLeitura           = false,
                     bool isTamanhoFixo             = false,
                     Qt::FocusPolicy foco           = Qt::StrongFocus,
                     QSizePolicy::Policy horizontal = QSizePolicy::Preferred,
                     QSizePolicy::Policy vertical   = QSizePolicy::Preferred);

QString distribuicaoQString(Distribuicao distribuicao);

enum class Conformidade
{
    Conforme,
    NaoConforme,
    NaoDeterminada
};

Conformidade avaliacaoConformidade(double y, double U, double limiteInferior,
                                   double limiteSuperior);

#endif // FUNCOESEXTRAS_HPP
