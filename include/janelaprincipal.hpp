#ifndef JANELAPRINCIPAL_HPP
#define JANELAPRINCIPAL_HPP

#include "dadosmedicao.hpp"
#include "resultadomedicao.hpp"

#include <QMainWindow>

class DerivadasParciais;
class PaginaTitulo;
class PaginaModelo;
class PaginaBalanco;

class QWidget;

namespace Ui
{
class JanelaPrincipal;
}

class JanelaPrincipal : public QMainWindow
{
    Q_OBJECT

public:
    explicit JanelaPrincipal(QWidget* parent = 0);

    static bool apenasDerivadas;

    ~JanelaPrincipal();

private slots:
    // slots dos actions
    void mostrarAjuda();
    void mostrarLicenca();
    void sobreInMed();

    // mudar a página do QStackedWidget
    void anteriorPagina();
    void proximaPagina();

    // mostrar derivadas parciais
    void visualizarDerivadasParciais();

private:
    void definirConexoes();

    bool coletarDados(int proximaPagina);

    // paginas da janela
    PaginaTitulo* m_paginaTitulo;
    PaginaModelo* m_paginaModelo;
    PaginaBalanco* m_paginaBalanco;

    DadosMedicao m_dadosMedicao;

    ResultadoMedicao m_resultadoMedicao;

    // derivadas parciais
    DerivadasParciais* m_dialogDerivadasParciais;

    Ui::JanelaPrincipal* m_ui;
};

#endif // JANELAPRINCIPAL_HPP
