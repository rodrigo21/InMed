#ifndef CONTRIBUICAOINCERTEZA_HPP
#define CONTRIBUICAOINCERTEZA_HPP

#include "modelomatematico.hpp"

#include <ginac/ginac.h>

#include <memory>

class Grandeza;

class ContribuicaoIncerteza
{
public:
    ContribuicaoIncerteza(std::shared_ptr<Grandeza> grandeza,
                          const ModeloMatematico& modelo,
                          GiNaC::exmap mapaValorEstimado);

    GiNaC::ex coeficienteSensibilidade() const;
    double valorCoeficienteSensibilidade() const;
    double contribuicaoIncerteza() const;
    int grausDeLiberdade() const;

    std::shared_ptr<Grandeza> grandeza() const;

    ~ContribuicaoIncerteza() = default;

private:
    GiNaC::ex m_coeficienteSensibilidade;
    double m_valorCoeficienteSensibilidade;
    double m_contribuicaoIncerteza;
    int m_grausDeLiberdade;

    std::shared_ptr<Grandeza> m_grandeza;

    ModeloMatematico m_modelo;
    GiNaC::exmap m_mapaValorEstimado;
};

#endif // CONTRIBUICAOINCERTEZA_HPP
