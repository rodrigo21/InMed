#ifndef GRANDEZATIPOA_HPP
#define GRANDEZATIPOA_HPP

#include "grandeza.hpp"

#include <ginac/ginac.h>

#include <string>
#include <vector>

class GrandezaTipoA : public Grandeza
{
public:
    GrandezaTipoA(const GiNaC::symbol& simboloGrandeza,
                  const std::string& definicao,
                  const std::string& unidade,
                  bool temCorrelacao,
                  const std::vector<double>& dadosExperimentais);

    GrandezaTipoA(const GiNaC::symbol& simboloGrandeza,
                  const std::string& definicao,
                  const std::string& unidade,
                  bool temCorrelacao,
                  std::vector<double>
                      dadosExperimentais,
                  double desvioPadraoAgrupado,
                  int numeroObservacoesAgrupadas);

    virtual double incertezaPadrao() override;
    virtual bool possuiCorrelacao() override;

    std::vector<double> dadosExperimentais() const;
    std::vector<double> desvio() const;
    int numeroDadosObservados() const;

    ~GrandezaTipoA() = default;

private:
    const std::vector<double> m_dadosExperimentais; // conjunto de dados experimentais
    std::vector<double> m_desvio;                   // desvio em relação à média

    int m_numeroDadosObservados; // número de dados observados
};

#endif // GRANDEZATIPOA_HPP
