// InMed - incertezatipoa.hpp

#ifndef INCERTEZATIPOA_HPP
#define INCERTEZATIPOA_HPP

#include <vector>

class IncertezaTipoA
{
public:
    IncertezaTipoA(std::vector<double> desvio,
                   double desvioPadraoAgrupado,
                   int numeroObservacoesAgrupadas);

    double variancia() const;
    double desvioPadrao() const;
    double varianciaDaMedia() const;
    double incertezaPadraoA() const;

    ~IncertezaTipoA() = default;

private:
    double m_variancia;        // variância experimental das observações
    double m_desvioPadrao;     // desvio-padrão experimental
    double m_varianciaDaMedia; // variância experimental da média
    double m_incertezaPadrao;  // incerteza-padrão
};

#endif // INCERTEZATIPOA_HPP
