#ifndef DADOSEXPERIMENTAIS_HPP
#define DADOSEXPERIMENTAIS_HPP

#include <QDialog>
#include <QString>
#include <QVector>

class QWidget;

namespace Ui
{
class DadosExperimentais;
}

class DadosExperimentais : public QDialog
{
    Q_OBJECT

public:
    DadosExperimentais(const QString& nomeGrandeza,
                       QVector<double>& dadosAnteriores,
                       QWidget* parent = 0);

    QVector<double> dados() const;

    ~DadosExperimentais();

signals:
    void dadosExperimentaisAlterados();

private slots:
    void adicionarDado();

    void removerDado();

    void ajustarTabela(int /*atualLinha*/, int /*atualColuna*/, int /*anteriorLinha*/,
                       int /*anteriorColuna*/);

    void coletarDados();

private:
    void recriarTabela();

    QVector<double> m_dados;

    Ui::DadosExperimentais* m_ui;
};

#endif // DADOSEXPERIMENTAIS_HPP
