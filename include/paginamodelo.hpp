#ifndef PAGINAMODELO_HPP
#define PAGINAMODELO_HPP

#include "modelomatematico.hpp"

#include <QString>
#include <QStringList>
#include <QVector>
#include <QWidget>

#include <string>
#include <vector>

class QListWidgetItem;

namespace Ui
{
class PaginaModelo;
}

class PaginaModelo : public QWidget
{
    Q_OBJECT

public:
    explicit PaginaModelo(QWidget* parent = 0);

    ~PaginaModelo();

    // QString modelo() const;

    // dados do tipo resultado

    QString nomeGrandezaResultado() const;
    QString definicaoGrandezaResultado() const;
    QString unidadeGrandezaResultado() const;

    // dados do tipo A o B

    // comuns a A e B
    QVector<QString> nomeGrandezasAouB() const;
    QVector<QString> definicaoGrandezasAouB() const;
    QVector<QString> unidadeGrandezasAouB() const;
    QVector<QString> tipoGrandezaAouB() const;

    // tipo A
    QVector<QVector<double>> dadosExperimentaisAouB() const;
    QVector<QString> avaliacaoIncertezaAouB() const;
    QVector<double> DPEAgrupadoAouB() const;
    QVector<unsigned int> numObsRIAouB() const;

    // tipo B
    QVector<QString> distribuicao() const;
    QVector<double> valorEstimado() const;
    QVector<double> incertezaExpandidaT() const;
    QVector<unsigned int> grausLiberdade() const;
    QVector<double> nivelConfianca() const;
    QVector<double> URTriMeiaLarguraLimites() const;
    QVector<double> incertezaExpandidaNormal() const;
    QVector<double> fatorAbrangencia() const;
    QVector<double> incertezaPadrao() const;
    QVector<double> traMeiaLarguraLimites1() const;
    QVector<double> traMeiaLarguraLimites2() const;

    ModeloMatematico modeloMatematico() const;

    // numero de grandezas excluindo a grandeza do resultado
    int numeroGrandezas() const;

    static bool derivadasValidas;

signals:
    void botaoAnteriorApertado();
    void botaoProximoApertado();
    void botaoDerivadasApertado();

private slots:
    void habilitarBotaoProximoEDerivadas();

    void mudarGrandezaPilha(QListWidgetItem* atual, QListWidgetItem* anterior);

    // adquire os dados para a grandeza resultado
    void coletarDadosResultado();

    // adquire os dados para a grandeza tipo A ou B
    void coletarDadosTipoAouB();

private:
    // dados do tipo resultado

    QString m_nomeGrandezaResultado;
    QString m_definicaoGrandezaResultado;
    QString m_unidadeGrandezaResultado;

    // dados do tipo A o B

    // comuns a A e B
    QVector<QString> m_nomeGrandezasAouB;
    QVector<QString> m_definicaoGrandezasAouB;
    QVector<QString> m_unidadeGrandezasAouB;
    QVector<QString> m_tipoGrandezaAouB;

    // tipo A
    QVector<QVector<double>> m_dadosExperimentaisAouB;
    QVector<QString> m_avaliacaoIncertezaAouB;
    QVector<double> m_DPEAgrupadoAouB;
    QVector<unsigned int> m_numObsRIAouB;

    // tipo B
    QVector<QString> m_distribuicao;
    QVector<double> m_valorEstimado;
    QVector<double> m_incertezaExpandidaT;
    QVector<unsigned int> m_grausLiberdade;
    QVector<double> m_nivelConfianca;
    QVector<double> m_URTriMeiaLarguraLimites;
    QVector<double> m_incertezaExpandidaNormal;
    QVector<double> m_fatorAbrangencia;
    QVector<double> m_incertezaPadrao;
    QVector<double> m_traMeiaLarguraLimites1;
    QVector<double> m_traMeiaLarguraLimites2;

    // leitura e validação da equação digitado

    QStringList lerModelo();
    bool validarModelo(QStringList equacao);

    // cria um QLisWidget um QStackedWidget com todos os símbolos do resultados e do modelo

    void criarListaPilhaGrandezas(const QString& resultado, const ModeloMatematico& modelo);

    QString m_grandezaResultado;
    QString m_grandezaOutras;

    ModeloMatematico m_modeloMatematico;

    std::vector<std::string> m_equacaoStrings;

    bool m_equacaoAlterada;

    int m_numeroGrandezas;

    Ui::PaginaModelo* m_ui;
};

#endif // PAGINAMODELO_HPP
