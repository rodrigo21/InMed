#ifndef TIPORESULTADO_HPP
#define TIPORESULTADO_HPP

#include <QString>
#include <QWidget>

class QLineEdit;
class QPlainTextEdit;

class TipoResultado : public QWidget
{
    Q_OBJECT

public:
    TipoResultado(const QString& objetoNome, QWidget* parent = 0);

    QString nomeGrandezaResultado() const;
    QString definicaoGrandezaResultado() const;
    QString unidadeGrandezaResultado() const;

private slots:
    void coletarDados();

private:
    QString m_nomeGrandezaResultado;
    QString m_definicaoGrandezaResultado;
    QString m_unidadeGrandezaResultado;

    QPlainTextEdit* m_plaintexteditDefinicao;
    QLineEdit* m_lineeditUnidade;
};

#endif // TIPORESULTADO_HPP
