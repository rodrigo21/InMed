#ifndef DIALOGOSOBRE_HPP
#define DIALOGOSOBRE_HPP

#include <QDialog>
#include <QString>

class QWidget;

namespace Ui
{
class DialogoSobre;
}

class DialogoSobre : public QDialog
{
    Q_OBJECT

public:
    DialogoSobre(const QString& titulo, const QString& versao, QWidget* parent = 0);

    ~DialogoSobre();

private:
    Ui::DialogoSobre* m_ui;
};

#endif // DIALOGOSOBRE_HPP
