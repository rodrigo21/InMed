#ifndef CORRELACAO_HPP
#define CORRELACAO_HPP

#include <vector>

class GrandezaTipoA;

class Correlacao
{
public:
    Correlacao(GrandezaTipoA grTipoA_q, GrandezaTipoA grTipoA_r);

    double covariancia() const;
    double coeficienteDecorrelacao() const;

    ~Correlacao() = default;

private:
    std::vector<double> m_desvio_q;
    std::vector<double> m_desvio_r;

    double m_covariancia;
    double m_coeficienteCorrelacao;
};

#endif // CORRELACAO_HPP
