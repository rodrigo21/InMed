#ifndef DIALOGOMOSTRARTEXTO_HPP
#define DIALOGOMOSTRARTEXTO_HPP

#include <QDialog>
#include <QString>

class QWidget;

namespace Ui
{
class DialogoMostrarTexto;
}

class DialogoMostrarTexto : public QDialog
{
    Q_OBJECT

public:
    DialogoMostrarTexto(const QString& arquivo, const QString& titulo, QWidget* parent = 0);

    ~DialogoMostrarTexto();

private:
    void carregarArquivoTexto(const QString& arquivoNome);

    Ui::DialogoMostrarTexto* m_ui;
};

#endif // DIALOGOMOSTRARTEXTO_HPP
