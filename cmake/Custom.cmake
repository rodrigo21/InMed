#############################################################################
# platform options

if(CMAKE_CXX_COMPILER_ID MATCHES "GNU" AND CMAKE_SYSTEM_NAME MATCHES "Windows")
    if(CMAKE_SIZEOF_VOID_P EQUAL 8)
        set(InMed_Exec ${PROJECT_NAME}_win64)
    else()
        set(InMed_Exec ${PROJECT_NAME}_win32)
    endif()
endif()

#############################################################################
# version

include(GetGitRevisionDescription)
git_describe(InMed_VERSION --tags)

#parse the version information into pieces.
string(REGEX REPLACE "^v([0-9]+)\\..*" "\\1" InMed_VERSION_MAJOR "${InMed_VERSION}")
string(REGEX REPLACE "^v[0-9]+\\.([0-9]+).*" "\\1" InMed_VERSION_MINOR "${InMed_VERSION}")
string(REGEX REPLACE "^v[0-9]+\\.[0-9]+\\.([0-9]+).*" "\\1" InMed_VERSION_PATCH "${InMed_VERSION}")
string(REGEX REPLACE "^v[0-9]+\\.[0-9]+\\.[0-9]+(.*)" "\\1" InMed_VERSION_SHA1 "${InMed_VERSION}")

set(InMed_VERSION_SHORT "${InMed_VERSION_MAJOR}.${InMed_VERSION_MINOR}.${InMed_VERSION_PATCH}")

# configure a header file to pass some of the CMake settings to the source code
configure_file (
    "${CMAKE_CURRENT_SOURCE_DIR}/include/inmedconfig.hpp.in"
    "${CMAKE_CURRENT_BINARY_DIR}/inmedconfig.hpp")

# add the binary tree to the search path for include files so that we will find InMedConfig.hpp
include_directories("${CMAKE_CURRENT_BINARY_DIR}")

#############################################################################
# include-what-you-use

option(USE_IWYU "Use include-what-you-use during the build" FALSE)

function(INCLUDE_WHAT_YOU_USE name)
    if(USE_IWYU)
        if(USE_COTIRE)
            message(AUTHOR_WARNING "Please disable cotire to use include-what-you-use")
        else()
            find_program(IWYU_PATH NAMES include-what-you-use iwyu)
            if(NOT IWYU_PATH)
                message(FATAL_ERROR "Could not find the program include-what-you-use")
            else()
                set(IWYU_PATH_AND_OPTIONS ${IWYU_PATH}
                   #-Xiwyu --mapping_file=${CMAKE_SOURCE_DIR}/tools/include-what-you-use/boost-1.64-all-private.imp
                    -Xiwyu --mapping_file=${CMAKE_SOURCE_DIR}/tools/include-what-you-use/boost-1.64-all.imp
                    -Xiwyu --mapping_file=${CMAKE_SOURCE_DIR}/tools/include-what-you-use/boost-all-private.imp
                    -Xiwyu --mapping_file=${CMAKE_SOURCE_DIR}/tools/include-what-you-use/boost-all.imp
                    -Xiwyu --mapping_file=${CMAKE_SOURCE_DIR}/tools/include-what-you-use/clang-6.intrinsics.imp
                    -Xiwyu --mapping_file=${CMAKE_SOURCE_DIR}/tools/include-what-you-use/gcc-8.intrinsics.imp
                    -Xiwyu --mapping_file=${CMAKE_SOURCE_DIR}/tools/include-what-you-use/gcc.libc.imp
                    -Xiwyu --mapping_file=${CMAKE_SOURCE_DIR}/tools/include-what-you-use/gcc.stl.headers.imp
                    -Xiwyu --mapping_file=${CMAKE_SOURCE_DIR}/tools/include-what-you-use/gcc.symbols.imp
                    -Xiwyu --mapping_file=${CMAKE_SOURCE_DIR}/tools/include-what-you-use/iwyu.gcc.imp
                    -Xiwyu --mapping_file=${CMAKE_SOURCE_DIR}/tools/include-what-you-use/libcxx.imp
                    -Xiwyu --mapping_file=${CMAKE_SOURCE_DIR}/tools/include-what-you-use/qt4.imp
                    -Xiwyu --mapping_file=${CMAKE_SOURCE_DIR}/tools/include-what-you-use/qt5_4.imp
                    -Xiwyu --mapping_file=${CMAKE_SOURCE_DIR}/tools/include-what-you-use/stl.c.headers.imp
                    -Xiwyu --mapping_file=${CMAKE_SOURCE_DIR}/tools/include-what-you-use/third_party.imp)
            endif()
        endif()

        set_property(TARGET ${name} PROPERTY CXX_INCLUDE_WHAT_YOU_USE ${IWYU_PATH_AND_OPTIONS})
    endif()
endfunction()

#############################################################################
# link-what-you-use

option(USE_LWYU "Use link-what-you-use during the build" FALSE)

function(LINK_WHAT_YOU_USE name)
    set_property(TARGET ${name} PROPERTY LINK_WHAT_YOU_USE ${USE_LWYU})
endfunction()
